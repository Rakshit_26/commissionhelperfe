import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentRoutingModule } from './agent-routing.module';
import { AgentDashboardComponent } from './Components/agent-dashboard/agent-dashboard.component';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { countUpTimerConfigModel, timerTexts, CountupTimerService } from 'ngx-timer';
import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { AgentService } from 'app/shared/services/agent/agent.service';
import { TimeZoneList } from '../shared/timezone';
import { SocketService } from '../shared/services/socket/socket.service';
import { AgentCallDialogComponent } from './Components/agent-call-dialog/agent-call-dialog.component';
import { AgentDialerListComponent } from './Components/agent-dialer-list/agent-dialer-list.component';
import { AgentFeedbackDialogComponent } from './Components/agent-feedback-dialog/agent-feedback-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { MatCardModule } from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatStepperModule} from '@angular/material/stepper';
import { NgxTimerModule } from 'ngx-timer';
import { AgentHeaderComponent } from './Components/agent-header/agent-header.component';
import { AgentHomeComponent } from './Components/agent-home/agent-home.component';
import { AgentProfileComponent } from './Components/agent-profile/agent-profile.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from '../../environments/environment';
import { MaterialModule } from 'app/material/material';
const config: SocketIoConfig = {
  url: environment.URL, options: {
    query: { Authorization: localStorage.token }
  }
};

@NgModule({
  declarations: [AgentDashboardComponent,  AgentCallDialogComponent, AgentDialerListComponent, AgentFeedbackDialogComponent, AgentHeaderComponent, AgentHomeComponent, AgentProfileComponent],
  imports: [
    CommonModule,
    AgentRoutingModule,
    GridModule,
    ButtonsModule,
    ToastrModule,
    MatCardModule,
    MatFormFieldModule,
    MatStepperModule,
    NgxTimerModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,InputsModule,
   // SharedModule,
    SocketIoModule.forRoot(config)
  ],
  providers:[AgentService,TimeZoneList,SocketService]
})
export class AgentModule { }
