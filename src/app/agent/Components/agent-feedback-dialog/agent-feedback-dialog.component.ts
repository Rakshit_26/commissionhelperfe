import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-agent-feedback-dialog',
  templateUrl: './agent-feedback-dialog.component.html',
  styleUrls: ['./agent-feedback-dialog.component.scss']
})
export class AgentFeedbackDialogComponent implements OnInit {

  constructor(public dialogRef1: MatDialogRef<AgentFeedbackDialogComponent>) { }

  ngOnInit(): void {
  }
  cancel() {
    this.dialogRef1.close();
  }
}
