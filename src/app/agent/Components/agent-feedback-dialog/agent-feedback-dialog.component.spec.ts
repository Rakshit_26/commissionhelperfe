import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentFeedbackDialogComponent } from './agent-feedback-dialog.component';

describe('AgentFeedbackDialogComponent', () => {
  let component: AgentFeedbackDialogComponent;
  let fixture: ComponentFixture<AgentFeedbackDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentFeedbackDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentFeedbackDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
