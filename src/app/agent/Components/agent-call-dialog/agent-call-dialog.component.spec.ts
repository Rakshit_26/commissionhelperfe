import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentCallDialogComponent } from './agent-call-dialog.component';

describe('AgentCallDialogComponent', () => {
  let component: AgentCallDialogComponent;
  let fixture: ComponentFixture<AgentCallDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentCallDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentCallDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
