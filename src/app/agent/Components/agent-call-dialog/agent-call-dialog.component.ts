import { Component, OnInit, Inject, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AgentService } from '../../../shared/services/agent/agent.service';
import { countUpTimerConfigModel, timerTexts, CountupTimerService } from 'ngx-timer';
import { AgentFeedbackDialogComponent } from '../agent-feedback-dialog/agent-feedback-dialog.component';
import { SocketService } from '../../../shared/services/socket/socket.service';
declare const Twilio: any;
declare var $: any;

@Component({
  selector: 'app-agent-call-dialog',
  templateUrl: './agent-call-dialog.component.html',
  styleUrls: ['./agent-call-dialog.component.scss'],
  providers:[SocketService]
})
export class AgentCallDialogComponent implements OnInit {
  CallerName: any;
  CallerNumber: any;
  callerlist: any;
  callscript: any;
  testConfig: any;
  callconnected: any;
  Notes = '';
  callended: boolean = false;
  connection: any;
  
  constructor(
      private service: AgentService,
      private countservice: CountupTimerService,
      public dialog: MatDialog,
      public DialogMat: MatDialogRef<AgentCallDialogComponent>,
      private socketService: SocketService,
      @Inject(MAT_DIALOG_DATA) public data: any
      ) {
  
  
  }
  
  ngOnInit() {
  debugger
  this.socketService.callDialing(this.data);
  console.log('connection is null. Initiating the call');
  var params = { "To": "+917000040178", "IsRecord": true };
  this.connection = Twilio.Device.connect(params);
  
  setTimeout(() => {
  $("i").addClass("green");
  }, 2000);
  
  
  this.twiliocall();
  
  
  
  
  this.testConfig = new countUpTimerConfigModel();
  this.testConfig.timerClass = 'test_Timer_class';
  
  // timer text values
  this.testConfig.timerTexts = new timerTexts();
  this.testConfig.timerTexts.hourText = '';
  this.testConfig.timerTexts.minuteText = '';
  this.testConfig.timerTexts.secondsText = '';
  this.countservice.startTimer();
  
  // setTimeout(() => {
  // this.ok();
  // }, 2000);
  
  
  this.CallerName = this.data.ContactList.AccountName;
  this.CallerNumber = this.data.ContactList.Phone;
  this.callscript = this.data.callscript;
  
  
  }
  
  
  twiliocall() {
  
  
  Twilio.Device.ready(function () {
  debugger;
  this.logtext = 'Ready';
  $("i").addClass("green");
  
  });
  Twilio.Device.disconnect(function () {
  debugger
  this.onPhone = false;
  this.logtext = 'Call ended.';
  $("i").addClass("red");
  
  setTimeout(() => {
  $('#dialogBox').click();
  }, 500);
  
  
  $('#openfeedback').click();
  
  
  });
  }
  
  feedback(): void {
  const dialogRef1 = this.dialog.open(AgentFeedbackDialogComponent, {
  });
  
  }
  
  callTransfer(){
      this.socketService.callForward(this.data);
  }
  
  cancel() {
  
  }
  ok() {
  debugger
  this.DialogMat.close('can pass string');
  this.countservice.stopTimer();
  
  }

}