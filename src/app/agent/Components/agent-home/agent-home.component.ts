import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agent-home',
  templateUrl: './agent-home.component.html',
  styleUrls: ['./agent-home.component.scss']
})
export class AgentHomeComponent implements OnInit {
  userName: any
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    debugger;
    this.userName = localStorage.userName
  }

}
