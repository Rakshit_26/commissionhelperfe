import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from '../../../shared/services/socket/socket.service';
import { Subscription } from 'rxjs';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-agent-header',
  templateUrl: './agent-header.component.html',
  styleUrls: ['./agent-header.component.scss']
})
export class AgentHeaderComponent implements OnInit {
  private _docSub: Subscription;
  Status = 'Available';
  mobileQuery: MediaQueryList;
  userName: any
  // fillerNav = Array.from({length: 50}, (_, i) => `Nav Item ${i + 1}`);

  private _mobileQueryListener: () => void;






  constructor(private router: Router, private socketservice: SocketService, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this._docSub = this.socketservice.getStatus.subscribe(doc => {
      debugger;
      console.log(doc)
      this.Status = doc['Status'];
    })
    this.userName = localStorage.userName
  }
  changeStatus(event) {
    debugger;
    this.Status = event.value;
    this.socketservice.agentStatusChange(this.Status);

  }

  ngOnDestroy() {
    this._docSub.unsubscribe();
  }

  logout() {
    this.socketservice.disconnect();
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
