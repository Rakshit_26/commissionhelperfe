import { Component, OnInit } from '@angular/core';
import { AgentService } from '../../../shared/services/agent/agent.service';
import { MatTableDataSource } from '@angular/material/table';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AgentCallDialogComponent } from '../agent-call-dialog/agent-call-dialog.component';
import { AgentFeedbackDialogComponent } from '../agent-feedback-dialog/agent-feedback-dialog.component';
import { DialerService } from '../../../shared/services/dialer/dialer.service';
import { countUpTimerConfigModel, timerTexts, CountupTimerService } from 'ngx-timer';
declare const Twilio: any;
import { SocketService } from '../../../shared/services/socket/socket.service';

@Component({
  selector: 'app-agent-dialer-list',
  templateUrl: './agent-dialer-list.component.html',
  styleUrls: ['./agent-dialer-list.component.scss'],
  providers:[SocketService]
})
export class AgentDialerListComponent implements OnInit {
  pageSize: number = 10;
  client: Variable;
  connection: any;
  currentConnection: any;
  phoneNumber: any = '';
  logtext: '';
  callscript = '';
  Notes = '';

  dataSource: any;//MatTableDataSource<any>;
  pageLimit: number[] = [5, 10];
  initialPage = {
    "length": 10,
    "pageIndex": 0,
    "pageSize": 10,
    "previousPageIndex": 0
  };

  public displayedColumns = ['AccountName', 'ContactName', 'SalesForceId', 'Email', 'Phone', 'ListId', 'City', 'State'];
  constructor(
    private service: AgentService,
    public dialog: MatDialog,
    private countservice: CountupTimerService,
    private dialerservice: DialerService,
    private socketService: SocketService
  ) {
    this.dialerservice.generateTwilioToken().subscribe(res => {
      debugger;
      localStorage.setItem('twilio', res['token']);
      Twilio.Device.setup(res['token']);
    });

    // Configure event handlers for Twilio Device
    // Twilio.Device.disconnect(function () {
    // debugger
    // this.onPhone = false;
    // this.logtext = 'Call ended.';
    // this.dialog.close();
    // });

    // Twilio.Device.ready(function () {
    // this.logtext = 'Connected';

    // });



  }



  ngOnInit(): void {
    this.getdialerlist();
    // debugger;
    // this.socketService.getListOfContacts.subscribe(res => {
    //     debugger;
    //     this.dataSource = new MatTableDataSource(res);
    // })
    // this.socketService.getContactList();
  }



  getdialerlist() {
    debugger
    this.service.getDilalerlist().subscribe(res => {
      debugger
      console.log(res);
      this.dataSource = res['ContactList']
     // this.dataSource = new MatTableDataSource(res['ContactList']);


    }, err => {

    })

  }

  onclick(eve){
    debugger;
    if(eve.columnIndex == 4){
      this.call(eve.dataItem);
    }
    console.log(eve);
  }
  call(ContactList): void {
    debugger;

    this.service.getdialelistWithContactID(ContactList._id).subscribe(res => {
      this.callscript = res['DialerList']["CallScript"];
      if (this.connection == null) {
        debugger;

        const dialogRef = this.dialog.open(AgentCallDialogComponent, {
          data: { ContactList, callscript: this.callscript },
          width: '1000px',
        });

      }
      else {
        this.connection = null;
        Twilio.Device.disconnectAll();
      }
    })


  }






}
