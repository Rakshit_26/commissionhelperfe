import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentDialerListComponent } from './agent-dialer-list.component';

describe('AgentDialerListComponent', () => {
  let component: AgentDialerListComponent;
  let fixture: ComponentFixture<AgentDialerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentDialerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentDialerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
