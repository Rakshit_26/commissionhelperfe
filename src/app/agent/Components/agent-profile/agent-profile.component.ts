import { Component, OnInit } from '@angular/core';
import { AgentService } from '../../../shared/services/agent/agent.service';

@Component({
  selector: 'app-agent-profile',
  templateUrl: './agent-profile.component.html',
  styleUrls: ['./agent-profile.component.scss']
})
export class AgentProfileComponent implements OnInit {
  profileData: any = {};
  constructor(private service: AgentService) { }

  ngOnInit(): void {
    this.getProfileDetails();
  }

  getProfileDetails() {
    debugger;
    this.service.getprofile().subscribe(res => {

      console.log(res);
      this.profileData = res['user'];


    }, err => {
      console.log(err);
      this.service.ErrorSuccess('Cant Get user details!');

    })
  }
}
