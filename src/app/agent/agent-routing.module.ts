  import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentDashboardComponent } from './Components/agent-dashboard/agent-dashboard.component';
import { AgentCallDialogComponent } from './Components/agent-call-dialog/agent-call-dialog.component';
import { AgentDialerListComponent } from './Components/agent-dialer-list/agent-dialer-list.component';
import { AgentFeedbackDialogComponent } from './Components/agent-feedback-dialog/agent-feedback-dialog.component';
import { AgentHomeComponent } from './Components/agent-home/agent-home.component';



const routes: Routes = [
  {path:'home', component:AgentHomeComponent},
  {path: 'app-agent-dashboard', component: AgentDashboardComponent},
  {path: 'app-agent-call-dialog', component: AgentCallDialogComponent},
  {path: 'app-agent-dialer-list', component: AgentDialerListComponent},
  {path: 'app-agent-feedback-dialog', component: AgentFeedbackDialogComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule { }
