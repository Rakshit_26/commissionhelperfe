import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogtoLoginComponent } from './dialogto-login.component';

describe('DialogtoLoginComponent', () => {
  let component: DialogtoLoginComponent;
  let fixture: ComponentFixture<DialogtoLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogtoLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogtoLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
