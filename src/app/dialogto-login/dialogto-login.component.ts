import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialogto-login',
  templateUrl: './dialogto-login.component.html',
  styleUrls: ['./dialogto-login.component.css']
})
export class DialogtoLoginComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<DialogtoLoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  login(){
    this.router.navigate(['/login']);
  }

}
