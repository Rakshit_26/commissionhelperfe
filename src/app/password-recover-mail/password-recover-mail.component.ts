import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm } from '@angular/forms';
// import { MyAlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { MaterialModule } from '../material/material';
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DialogOnChangePasswordComponent } from '../dialog-on-change-password/dialog-on-change-password.component';
import { DialerService } from 'app/shared/services/dialer/dialer.service';

declare var $: any;
@Component({
  selector: 'app-password-recover-mail',
  templateUrl: './password-recover-mail.component.html',
  styleUrls: ['./password-recover-mail.component.css']
})
export class PasswordRecoverMailComponent  implements OnInit {

  // --------------------- Variables ---------------------------------------------//
  changePasswordForm: FormGroup;

  ConfirmPassword = '';
  Password = '';
  href: String = '';
  userId_str: any;
  message: string;
  length = false;
  specialCharacter = false;
  upperCase = false;
  lowerCase = false;
  numericCharacter = false;
  passwordInput;
  isTextFieldType = 'password';
  // variable
  show_button: Boolean = false;
  show_eye: Boolean = false;

  //Function
  showPassword() {
    this.show_button = !this.show_button;
    this.show_eye = !this.show_eye;
  }
  // -----------------------------------------------------------------------------//


  // ------------------------------- Constructor ---------------------------------//
  constructor(
    private toastrService: ToastrService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    public router: Router,
    private service: DialerService,
    private fb: FormBuilder
  ) {
    localStorage.clear();
    // To initialize FormGroup
    this.changePasswordForm = fb.group({
      Password: ['', Validators.required],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.service.comparePasswords });
  }

  // -----------------------------------------------------------------------------//

  ngOnInit() {
    debugger;
    this.href = this.router.url;
    this.userId_str = this.href.split('/');
    this.userId_str = this.userId_str[2];
    debugger
    if (!this.userId_str) {
      this.router.navigate(['/notfound']);
    }
    this.service.checkURL(this.userId_str).subscribe(res => {
      debugger;
      if (!res['success']) {
        this.service.ErrorSuccess('Link has been expired.');
        this.router.navigate(['/notfound']);
      }
      this.passwordInput = document.getElementById('passwordInfo');
      this.passwordInput.classList.add('toolBox');
      this.passwordInput.classList.remove('tooltiptext');

    }, err => {
      this.service.ErrorSuccess('Link has been expired.');
      this.router.navigate(['/notfound']);
    })

  }


  checkPassword(fb: FormGroup) {
    // debugger;
    this.passwordInput = document.getElementById('passwordInfo');
    const password = fb['currentTarget']['value'];
    if (password.length >= 8) {
      this.length = true;
    }
    if (password.match(/\d/)) {
      this.numericCharacter = true;
    }
    if (password.match(/[A-Z]/)) {
      this.upperCase = true;
    }
    if (password.match(/[a-z]/)) {
      this.lowerCase = true;
    }
    if (password.match(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/)) {
      this.specialCharacter = true;
    }
    if (password.length < 8) {
      this.length = false;
    }
    if (!password.match(/\d/)) {
      this.numericCharacter = false;
    }
    if (!password.match(/[A-Z]/)) {
      this.upperCase = false;
    }
    if (!password.match(/[a-z]/)) {
      this.lowerCase = false;
    }
    if (!password.match(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/)) {
      this.specialCharacter = false;
    }

    this.passwordInput.classList.add('toolBox');
    this.passwordInput.classList.remove('tooltiptext');

    if (this.length && this.lowerCase && this.specialCharacter && this.upperCase && this.numericCharacter) {
      this.passwordInput.classList.add('toolBox');
      this.passwordInput.classList.remove('tooltiptext');
    }

  }

  removeSuggestions(fb: FormGroup) {
    // this.passwordInput = document.getElementById('passwordInfo');
    // this.passwordInput.classList.remove('toolBox');
    // this.passwordInput.classList.add('tooltiptext');
  }

  changePassword(form: NgForm) {
    debugger;
    // console.log(form);
    const userDetails = {
      token: this.userId_str,
      password: form['Password'],
    };
    // console.log(userDetails);
    this.service.changePassword(userDetails).subscribe(res => {
      debugger;
      // console.log(response);
      if (res['success']) {
        const dialogRef = this.dialog.open(DialogOnChangePasswordComponent, {
          width: '490px',
          height: '340px'
        })
      }
    }, error => {
      // console.log(error);
      this.message = error.error.errorMessage;
      this.service.ErrorSuccess(this.message);
    });
  }

  // OpenDialogBox(){
  //   const dialogRef = this.dialog.open(DialogOnChangePasswordComponent, {
  //     width: '490px',
  //     height: '355px'
  //   })
  // }
  // --------------------------- Dialog Box and Tostr ---------------------------//


  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: ['warning']
    });
  }

}
