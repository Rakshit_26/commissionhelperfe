import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordRecoverMailComponent } from './password-recover-mail.component';

describe('PasswordRecoverMailComponent', () => {
  let component: PasswordRecoverMailComponent;
  let fixture: ComponentFixture<PasswordRecoverMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordRecoverMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordRecoverMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
