import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './layouts/full/full-layout.component';
import { TestComponent } from './test/test.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { SignupComponent } from './signup/signup.component';
import { PasswordRecoverMailComponent } from './password-recover-mail/password-recover-mail.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'prefix' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'signup', component: SignupComponent },
  //{ path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) , canActivate: [AuthGuard]},
  //{ path: 'salesforce/:id', loadChildren: () => import('./salesforce/salesforce.module').then(m => m.SalesforceModule)},


  { path: 'changePassword/:userId', component: PasswordRecoverMailComponent },
  //{ path: 'notfound', loadChildren: () => import('./not-found/not-found.module').then(m => m.NotFoundModule) },

  //{ path: '**', redirectTo: '/notfound' },


  {
    path: 'layout',
    component: FullLayoutComponent,
    children: [
      { path: 'test', component: TestComponent, canActivate: [AuthGuard] },

      { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), canActivate: [AuthGuard] },
      { path: 'cm', loadChildren: () => import('./base-menu/base-menu.module').then(m => m.BaseMenuModule), canActivate: [AuthGuard] },
      // { path: 'sf', loadChildren: () => import('./salesforce/salesforce.module').then(m => m.SalesforceModule), canActivate: [AuthGuard] },
      //      { path: 'agent', loadChildren: () => import('./agent/agent.module').then(m => m.AgentModule) , canActivate: [AuthGuard]},
      { path: 'agent', loadChildren: () => import('./agent/agent.module').then(m => m.AgentModule) },
      { path: 'salesforce/:id', loadChildren: () => import('./salesforce/salesforce.module').then(m => m.SalesforceModule) },
      { path: 'pipeline-admin', loadChildren: () => import('./pipeline-admin/pipeline-admin.module').then(m => m.PipelineAdminModule) },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
