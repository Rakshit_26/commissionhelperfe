import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Observable }    from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DialerService } from 'app/shared/services/dialer/dialer.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [DialerService]
  
})
export class SignupComponent {

  Titles:any = [
    { value: 'Mr' , Name:'Mr'},  
    { value: 'Mrs' , Name:'Mrs'},
    { value:'Miss' , Name:'Miss'}
    ]

  // --------------------- Variables ---------------------------------------------//
  formGroup: FormGroup;
  titleAlert: string = 'This field is required';
  post: any = '';

       
    // ------------------------------- Constructor ---------------------------------//
    constructor(
      private formBuilder: FormBuilder,
      public router: Router,
      private service: DialerService,
      ) { }

  ngOnInit() {
    this.createForm(); 
	this.formGroup = new FormGroup({
       firstName: new FormControl()

    });   
  }

  createForm() {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.formGroup = this.formBuilder.group({
      'username': [null, [Validators.required, Validators.pattern(emailregex)], this.checkInUseEmail],
      'firstname': [null, Validators.required],
      'lastname': [null, Validators.required],
      'password': [null, [Validators.required, this.checkPassword]],
      'title': [null, Validators.required],
    });
  }

  get firstname() {
    return this.formGroup.get('firstname') as FormControl;
  }
  get lastname() {
    return this.formGroup.get('lastname') as FormControl;
  }
  
  checkPassword(control) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }

  checkInUseEmail(control) {
    // mimic http database access
    let db = ['tony@gmail.com'];
    return new Observable(observer => {
      setTimeout(() => {
        let result = (db.indexOf(control.value) !== -1) ? { 'alreadyInUse': true } : null;
        observer.next(result);
        observer.complete();
      }, 4000)
    })
  }

  getErrorEmail() {
    return this.formGroup.get('username').hasError('required') ? 'Field is required' :
      this.formGroup.get('username').hasError('pattern') ? 'Not a valid emailaddress' :
        this.formGroup.get('username').hasError('alreadyInUse') ? 'This emailaddress is already in use' : '';
  }

  getErrorPassword() {
    return this.formGroup.get('password').hasError('required') ? 'Field is required (at least eight characters, one uppercase letter and one number)' :
      this.formGroup.get('password').hasError('requirements') ? 'Password needs to be at least eight characters, one uppercase letter and one number' : '';
  }

  
  onSubmit(post) {
    debugger;

    const userDetails={
      firstname: post.firstname,
      lastname: post.lastname,
      username: post.username,
      password: post.password,
      title: post.title,
      userType: 'admin'
    }
this.service.signUp(userDetails).subscribe(res=>{
  debugger;
  if(res['success']){
    // this.service.showSuccessMessage('You Have Successfully Signed In')
    this.router.navigate(['/login']);
  }
}, err=>{
    debugger;
    // this.service.showErrorMessage('Error occured while Signing In')
    //alert(err['error']['message']);
  })
  }
  
  login(){
    this.router.navigate(['/home'])
  }
  
}
