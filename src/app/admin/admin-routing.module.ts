import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentComponent } from './Components/agent/agent.component';
import { AgentListComponent } from './Components/agent-list/agent-list.component';
import { CompanycomponentComponent } from './Components/companycomponent/companycomponent.component';
import { CompanydetailsComponent } from './Components/companydetails/companydetails.component';
import { CompanyhistoryService } from 'app/shared/services/companyhistory.service';
import { DialerListComponent } from './Components/dialer-list/dialer-list.component';
import { HomeComponent } from './Components/home/home.component';
import { LocationComponent } from './Components/location/location.component';
import { LocationListComponent } from './Components/location-list/location-list.component';
import { UserComponent } from './Components/user/user.component';
import { UserListComponent } from './Components/user-list/user-list.component';
import { DialerListDetailsComponent } from './Components/dialer-list-details/dialer-list-details.component';
import { AgentLivetrackingComponent } from './Components/agent-livetracking/agent-livetracking.component';


const routes: Routes = [
  { path: 'agent',component: AgentComponent },
  { path: 'agent-list',component: AgentListComponent },
  { path: 'agentlivetracking',component: AgentLivetrackingComponent },
  { path: 'companycomponent', component: CompanycomponentComponent },
  { path: 'companydetails', component: CompanydetailsComponent },
  { path: 'companyhistory', component: CompanyhistoryService },
  { path: 'dialer-list', component: DialerListComponent },
  { path: 'dialer-list-details', component: DialerListDetailsComponent},
  { path: 'home', component: HomeComponent },
  { path: 'location', component: LocationComponent },
  { path: 'location-list', component: LocationListComponent },
  { path: 'locationlist', component: LocationListComponent },
  { path: 'user', component: UserComponent },
  { path: 'user-list', component: UserListComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

