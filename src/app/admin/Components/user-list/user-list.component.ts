import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { UserComponent } from '../user/user.component';

import{ComissionHelperService} from 'app/shared/services/comission-helper.service'


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [ComissionHelperService]
})
export class UserListComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  userData = [];
  gridView = [];
  message: any;
  TimeZoneListDetails = [];
  url = '';
  data: Variable;
  Inactive = true;
  Active = false;
  limit: number = 10;
  skip: number = 0;
  length: number = 0;
  previousPageIndex: number = 0;
  pageIndex: number = 0;
  pageSize: number = 10;
  pageLimit: number[] = [5, 10];
  initialPage = {
    length: 10,
    pageIndex: 0,
    pageSize: 10,
    previousPageIndex: 0
  };
  property = 'firstname';
  order = 'asc';

  displayedColumns = ['company', 'title', 'Role', 'edit', 'delete'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  constructor(private router: Router, private service: ComissionHelperService,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllUser();
  }

  getAllUser() {
    debugger;
    this.service.getAllUsers().subscribe((res) => {
      debugger;
      this.userData = res['UserList'];
      this.gridView = res['UserList']
      this.userData = this.userData.map(elm => ({
        title: elm.title,
        username: elm.username,
        company: elm.company,
        State: elm.State,
        Country: elm.Country,

        _id: elm._id
        // userId: elm.userId
      }))

      this.dataSource = new MatTableDataSource(this.userData);
      this.dataSource.sort = this.sort;
      this.limit = 10;
      this.length = res['totalCount'];
      this.dataSource.paginator = this.paginator;

      this.initialPage = {
        "length": this.length,
        "pageIndex": 0,
        "pageSize": 10,
        "previousPageIndex": 0
      }
    }, (err) => {
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }

  userpopup(user): void {
    const dialogRef = this.dialog.open(UserComponent, {
      width: '650px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    })
  }

  editpopup(user) {
    const dialogRef = this.dialog.open(UserComponent, {
      width: '650px',
      disableClose: true,
      data: {
        user
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });

  }

  saveDetail() {
    debugger;
    this.router.navigate(['/dashboard/pipelineadmin/userlist'])
  }

  editUserDetail(userData) {
    debugger;
    this.router.navigate(['/dashboard/pipelineadmin/agentform'], { state: { userData } })
  }

  delete(_id) {
    this.service.delete(_id).subscribe(res => {
      debugger;
      if (res['success']) {
        debugger;
        this.service.showToaster('User has been deleted');
        this.ngOnInit()

      }
    }, err => {
      console.log(err);
      this.service.ErrorSuccess('User Can not be deleted!');
    }
    )
  }
}