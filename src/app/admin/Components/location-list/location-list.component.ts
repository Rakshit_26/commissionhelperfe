import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { LocationComponent } from '../location/location.component';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { LocationService } from '../../../shared/services/location.service';
// import { LocationService } from 'src/app/location.service';


@Component({
    selector: 'app-location-list',
    templateUrl: './location-list.component.html',
    styleUrls: ['./location-list.component.scss'],
    providers: [LocationService]
})
export class LocationListComponent implements OnInit {
    opened: boolean = false;

    dialogAction: string = '';
    dialogAction1: boolean = true;// true-add false-update
    formGroup: FormGroup = new FormGroup({
        _id: new FormControl(''),
        Name: new FormControl(''),
        Address: new FormControl(''),
        City: new FormControl(''),
        State: new FormControl(''),
        Country: new FormControl('')
    });


    locationList: any[];
    dataSource: MatTableDataSource<any>;
    locationData = [];
    message: any;
    TimeZoneListDetails = [];
    url = '';
    data: Variable;
    Inactive = true;
    Active = false;
    limit: number = 10;
    skip: number = 0;
    length: number = 0;
    previousPageIndex: number = 0;
    pageIndex: number = 0;
    pageSize: number = 10;
    pageLimit: number[] = [5, 10];
    initialPage = {
        length: 10,
        pageIndex: 0,
        pageSize: 10,
        previousPageIndex: 0
    };
    property = 'firstname';
    order = 'asc';




    public displayedColumns = ['id', 'Name', 'Address', 'City', 'State', 'Country', 'Usercount', 'edit', 'delete'];
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;

    constructor(private router: Router, private service: LocationService, public dialog: MatDialog, private formBuilder: FormBuilder) {
        this.formBuilder.group({
                    Name: ['', Validators.required],
                    Address: ['', Validators.required],
                    City: ['', Validators.required],
                    State: ['', Validators.required],
                    Country: ['', Validators.required]
                });
    }

    ngOnInit(): void {
        this.getCenterDetail();
    }

    getCenterDetail() {
        debugger;
        this.service.getCenterDetail().subscribe((res) => {
            debugger;
            this.locationData = res['listDetails'];
            this.locationData = this.locationData.map(elm => ({
                Name: elm.Name,
                Address: elm.Address,
                City: elm.City,
                State: elm.State,
                Country: elm.Country,
                Usercount: elm.Usercount,
                _id: elm._id
                // userId: elm.userId
            }))
            this.locationList = this.locationData;
            this.locationList = this.locationList;
            console.log(this.locationList);
            this.dataSource = new MatTableDataSource(this.locationData);
            this.dataSource.sort = this.sort;
            this.limit = 10;
            this.length = res['totalCount'];
            this.dataSource.paginator = this.paginator;

            this.initialPage = {
                "length": this.length,
                "pageIndex": 0,
                "pageSize": 10,
                "previousPageIndex": 0
            }
        }, (err) => {
            this.message = err.error['errorMessage'];
            this.service.ErrorSuccess(this.message);
        });
    }
    cancel() {
        this.opened = false;
    }
    close() {
        this.opened = false;
    }
    onSubmit(location) {
        if (!this.dialogAction1) {

            console.log('update' + location);
            console.log(location);
            this.service.editCenterDetail(location).subscribe(res => {
                if (res['success']) {
                    this.service.showToaster('Location has been updated');
                    this.opened = false; this.ngOnInit()
                }
            }, (err) => {
                console.log(err);
                this.service.showToaster('Location can not be updated!');
            }
            );
        } else if (this.dialogAction1) {
            this.service.saveCenterDetail(location).subscribe(res => {
                if (res['success']) {
                    this.service.showToaster('Location has been registeres');
                    this.opened = false; this.ngOnInit()
                }
            }, (err) => {
                console.log(err);
                this.service.showToaster('Location can not be registered!');
            }
            );
        } else {
        }
    }
    locationpopup(location): void {
        this.dialogAction = 'Add Location';
        this.dialogAction1 = true;
        this.opened = true;
        this.formGroup.setValue({
            _id: '',
            Name: '',
            Address: '',
            City: '',
            State: '',
            Country: ''
        });
        /*const dialogRef = this.dialog.open(LocationComponent, {
            width: '650px',
            disableClose: true,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.ngOnInit();
            }
        })*/
    }

    editpopup(location) {
        this.opened = true;
        this.dialogAction = 'Update Location';
        this.dialogAction1 = false;
        console.log(location.Name);
        this.formGroup.setValue({
            _id: location._id,
            Name: location.Name,
            Address: location.Address,
            City: location.City,
            State: location.State,
            Country: location.Country
        });
        /*this.formGroup = this.formBuilder.group({
            _id: [location._id],
            Name: [location.Name, Validators.required],
            Address: [location.Address, Validators.required],
            City: [location.City, Validators.required],
            State: [location.State, Validators.required],
            Country: [location.Country, Validators.required]
        });*/
        /*const dialogRef = this.dialog.open(LocationComponent, {
            width: '650px',
	position:{right:0 +'px',top:  0+'px',left:100+'px'},
            disableClose: true,
            data:location
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.ngOnInit();
            }
        });*/

    }

    saveCenterDetail() {
        debugger;
        this.router.navigate(['/dashboard/pipelineadmin/locationform'])
    }

    editCenterDetail(locationData) {
        debugger;
        this.router.navigate(['/dashboard/pipelineadmin/agentform'], { state: { locationData } })
    }

    deleteCenterDetail(_id) {
        this.service.deleteCenterDetail(_id).subscribe(res => {
            if (res['success']) {
                this.service.showToaster('Location has been deleted');
                this.ngOnInit()

            }
        }, err => {
            console.log(err);
            this.service.ErrorSuccess('Location Can not be deleted!');
        }
        )
    }
}
