import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DataSource } from '@angular/cdk/table';
import { CompanycomponentService } from '../../../shared/services/companycomponent.service';
declare var $: any;

@Component({
  selector: 'app-companycomponent',
  templateUrl: './companycomponent.component.html',
  styleUrls: ['./companycomponent.component.scss'],
  providers: [CompanycomponentService]
 
})
export class CompanycomponentComponent  {

  search ={
    "CompanyName":"",
    "OrgId":"",
    "CompanyUrl":"",
    "CallCount":"",
    "LicenseStartDate":"",
    "LicenseEndDate":"",
  }
  
  gridData: any[];

  dataSource: MatTableDataSource<any>;
  message: any;
  showAdvanceFilter = false;
  allCompanies = [];
  public displayedColumns = ['S No','CompanyName', 'OrgId', 'Company_url', 'License', 'License_start_date', 'License_end_date','callCount','History','viewDetails','deactivateCompany'];
  length: number = 0;
  limit: number = 10;
  filter:string="";
  maxDate : any;
  startDate : any;
  property:any = 'test';
  order:any='desc';
  pageLimit: number[] = [5, 10] ;
  pageSize:number =10;
  pageIndex : number = 1;
  previousPageIndex:number =0;
  initialPage ={
    "length": 1,
    "pageIndex": 0,
    "pageSize": 5,
    "previousPageIndex": 0
  }

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

   constructor(private router: Router, public dialog: MatDialog,public service : CompanycomponentService ) { 
    this.dataSource = new MatTableDataSource<any>();
   }
   
   ngOnInit() {
    debugger;
    this.getAllCompany();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllCompany(){
  
  debugger;
    this.service.getAllCompany(this.pageIndex, this.filter, this.pageSize,this.property, this.order).subscribe((res) => {
      debugger;
        this.allCompanies = res['Company'];
        this.gridData=res['Company'];
        this.dataSource = new MatTableDataSource(this.allCompanies);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.limit = this.pageSize
        this.length = res['totalCount'];;
        // this.initialPage ={
        //   "length": this.length,
        //   "pageIndex": 0,
        //   "pageSize": 10,
        //   "previousPageIndex": 0
        // }
       //this.changePage(this.initialPage);
    }, (err) => {
     
      // console.log(err);
     debugger;
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }

   ngAfterViewInit() {
  
  }

  changePage(event) {
    //debugger;
    this.pageSize = event.pageSize
    this.pageIndex = event.pageIndex +1
    this.previousPageIndex = event.previousPageIndex
    this.service.getAllCompany(this.pageIndex, this.filter, this.pageSize,this.property, this.order).subscribe((res) => {
      //debugger;
          //if (res['statusCode'] === 200){
            this.allCompanies = res['Company'];
            console.log(this.allCompanies)
            if(this.pageIndex === 0){
            
              this.dataSource =  new MatTableDataSource(this.allCompanies);
                this.dataSource.sort = this.sort;
           
            }
            if(this.allCompanies.length >0){
              this.dataSource =  new MatTableDataSource(this.allCompanies);
               this.dataSource.sort = this.sort;
            }
            else{
             debugger;
              this.pageIndex = event.pageIndex-1
              this.previousPageIndex = event.previousPageIndex
              this.pageSize = event.pageSize -1
              if(this.pageIndex < 0){
                this.pageIndex = 0;
              }
              this.service.getAllCompany( this.pageIndex,this.filter, this.pageSize,this.property, this.order).subscribe((res) => {
                if (res['statusCode'] === 200){
                  this.allCompanies = res['Company'];
                  this.dataSource =  new MatTableDataSource(this.allCompanies);
                  this.dataSource.sort = this.sort;
                  
                }
              }, (err) => {
               
                // console.log(err);
                 
                this.message = err['error']['errorMessage'];
                this.service.ErrorSuccess(this.message);
            });
             }
          //}
        }, (err) => {
           
            // console.log(err);
             
            this.message = err['error']['errorMessage'];
            this.service.ErrorSuccess(this.message);
        });
  }
  date(){
debugger;
  
    $('#startdate').attr('max', this.search.LicenseEndDate);
  
  }
  
  date2(){
    debugger;
 
    $('#enddate').attr('min', this.startDate);
  }
  startdateFunction(date){

    this.startDate = date;
  }
  endDate(date){

    this.maxDate = date;
  }

  clearFilters(){
    this.search ={

      "CompanyName":"",
      "OrgId":"",
      "CompanyUrl":"",
      "CallCount":"",
      "LicenseStartDate":"",
      "LicenseEndDate":"",
    }
    this.filter="";
    this.getAllCompany()
  }

filters(search){
debugger;
this.filter="";
this.pageSize=10;
this.pageIndex=1;
  if (search.CompanyName != null && search.CompanyName != '') {

    this.filter = this.filter + "&CompanyName=" + search.CompanyName

  }

  if (search.OrgId != null && search.OrgId != "" ) {

    this.filter = this.filter + "&OrgId=" + search.OrgId

  }
  if (search.CompanyUrl != null && search.CompanyUrl != "" ) {

    this.filter = this.filter + "&Company_url=" + search.CompanyUrl

  }
  if (search.CallCount != null && search.CallCount != "") {

    this.filter = this.filter + "&callCount=" + search.CallCount

  }
  if (search.LicenseStartDate != null && search.LicenseStartDate != "") {

    this.filter = this.filter + "&License_start_date=" + search.LicenseStartDate

  }
  if (search.LicenseEndDate != null && search.LicenseEndDate != "") {

    this.filter = this.filter + "&License_end_date=" + search.LicenseEndDate

  }

  this.getAllCompany()
}



  deactivateUsers(CompanyId){
  
    this.service.deleteCompany(CompanyId).subscribe(res=>{
      this.service.warningSuccess('Company Deleted Successfully');
      this.initialPage={
        "length": this.length,
        "pageIndex": this.pageIndex,
        "pageSize": this.pageSize,
        "previousPageIndex": this.previousPageIndex
      }
      this.changePage(this.initialPage)
      this.length -=1
      this.getAllCompany();
    }, error => {
      
      
    })
  }

  viewDetails(companyData){
    debugger;
    localStorage.setItem('companyId', companyData._id) // companyData._Id
    this.router.navigate(['/dashboard/pipelineadmin/companyDetails'], {state: {companyData}});
  }

  viewHistory(companyData){
    debugger;
    localStorage.setItem('companyId', companyData._id) // companyData._Id
    this.router.navigate(['/dashboard/pipelineadmin/companyHistory'], {state: {companyData}});
  }
  
  
   sortData(event){
    debugger;
    this.order=event.direction;
    this.property=event.active;
   
    this.service.getAllCompany(this.pageIndex,this.filter, this.pageSize,this.property, this.order).subscribe(res =>{
      debugger;
       this.allCompanies = res['Company'];
       this.length = res['totalCount'];
   
       this.dataSource = new MatTableDataSource(this.allCompanies);
     
       this.dataSource.sort = this.sort;
    
       
   
   }, (err) => {
    
     // console.log(err);
    
     this.message = err.error['errorMessage'];
     this.service.ErrorSuccess(this.message);
   });
   
  }
  
  showAdvancedFilter(value){
    this.showAdvanceFilter = value;
 }
  
}

