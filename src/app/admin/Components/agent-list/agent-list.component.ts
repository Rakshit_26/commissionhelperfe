import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { AgentService } from '../../../shared/services/agent/agent.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { THIS_EXPR, ThrowStmt } from '@angular/compiler/src/output/output_ast';
import { TimeZoneList } from '../../../shared/services/timezone';
import { AgentComponent } from '../agent/agent.component';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
declare var $: any;
@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.scss'],
  providers: [AgentService, TimeZoneList]
})
export class AgentListComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  locationData = [];
  message: any;
  TimeZoneListDetails = [];
  url = '';
  data: Variable;
  Inactive = true;
  Active = false;
  limit: number = 10;
  skip: number = 0;
  length: number = 0;
  previousPageIndex: number = 0;
  pageIndex: number = 0;
  filter: string = '';
  search = {
    firstname: '',
    lastname: '',
    username: '',
    AvailableStartTime: '',
    AvailableEndTime: '',
    timeOffset: '',
  }
 status={
  Active:'',
  Inactive:'',
  Onbreak:'',
  Invitation:'',
 } 
  pageSize: number = 10;
  pageLimit: number[] = [5, 10];
  initialPage = {
    length: 10,
    pageIndex: 0,
    pageSize: 10,
    previousPageIndex: 0
  };
  property = 'firstname';
  order = 'asc'
  d = new Date();
  n = this.d.getTimezoneOffset();
  public agentList: any[];
  public displayedColumns = ['S No', 'firstname','username', 'Status', 'AvailableStartTime', 'AvailableEndTime', 'timeOffset', 'deactivate', 'edit', 'delete'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private router: Router, private service: AgentService, private dialog: MatDialog, private timeZoneList: TimeZoneList) { }

  ngOnInit() {
  //   $("#switch").kendoSwitch({
  //     change: function (e) {
        
  //     }
  // });
    this.GetallAgentStatus()
    this.TimeZoneListDetails = this.timeZoneList.aryIannaTimeZones;
    this.getAllAgent();
   
  }

  getAllAgent() {
    debugger;
    let timeOffsetDifference = this.n;
    this.service.getAllAgent(this.pageIndex, this.filter, this.limit, this.property, this.order, timeOffsetDifference).subscribe((res) => {
     
      debugger;
     
      this.locationData = res['agentList'];
    
      for (let i = 0; i < this.locationData.length; i += 1) {
        let AgentAvailableStartTime = '';
        let AgentAvailableEndTime = '';
        if ((this.locationData[i].AvailableStartTime - timeOffsetDifference) < 0){
          this.locationData[i].AvailableStartTime = this.locationData[i].AvailableStartTime + 1440;
        }
        if ((this.locationData[i].AvailableEndTime - timeOffsetDifference) < 0){
          this.locationData[i].AvailableEndTime = this.locationData[i].AvailableEndTime + 1440;
        }
        if (Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60) < 10) {
          AgentAvailableStartTime = `0${Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60)}:`
        } else {
          AgentAvailableStartTime = `${Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60)}:`
          if(parseInt(AgentAvailableStartTime)>=24){
            AgentAvailableStartTime=(parseInt(AgentAvailableStartTime)-24).toString();
          }
        }
        if (((this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60) < 10) {
          AgentAvailableStartTime = AgentAvailableStartTime + `0${(this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60}`
        } else {
          AgentAvailableStartTime = AgentAvailableStartTime + `${(this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60}`
        }
        if (Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60) < 10) {
          AgentAvailableEndTime = `0${Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60)}:`
        } else {
          AgentAvailableEndTime = `${Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60)}:`
          if(parseInt(AgentAvailableEndTime)>=24){
            AgentAvailableEndTime=(parseInt(AgentAvailableEndTime)-24).toString();
          }
        }
        if (((this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60) < 10) {
          AgentAvailableEndTime = AgentAvailableEndTime + `0${(this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60}`
        } else {
          AgentAvailableEndTime = AgentAvailableEndTime + `${(this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60}`
        }
      
        this.locationData[i].AvailableStartTime1 = AgentAvailableStartTime;
        this.locationData[i].AvailableEndTime1 = AgentAvailableEndTime;
      }
      this.locationData = this.locationData.map(elm => ({
        AvailableStartTime: elm.AvailableStartTime ? elm.AvailableStartTime1 : null,
        AvailableEndTime: elm.AvailableEndTime ? elm.AvailableEndTime1 : null,
        timeOffset: elm.timeOffset,
        AgentStatus: elm.AgentStatus,
        createdAt: new Date(elm.createdAt),
        agentId: elm.agentId.toString(),
        title: elm.title,
        username: elm.username,
        firstname: elm.firstname,
        lastname: elm.lastname,
        userType: 3,
        userId: elm.userId,
        Status: elm.Status
      }))
      this.agentList = res['agentList'];

      // this.dataSource = new MatTableDataSource(this.locationData);
      // this.dataSource.sort = this.sort;
      // this.limit = 10
      // this.length = res['totalCount'];
      // this.dataSource.paginator = this.paginator;

      // this.initialPage = {
      //   length: this.length,
      //   pageIndex: 0,
      //   pageSize: 10,
      //   previousPageIndex: 0
      // }
      // this.changePage(this.initialPage)

    }, (err) => {
      // 
      // console.log(err);

      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });

  }

  GetallAgentStatus(){
    debugger;
    this.service.allAgentStatus().subscribe(res => {
      debugger;
      console.log(res);
      if(res['success']){
        this.status.Active = res['Active'];
        this.status.Inactive = res['Inactive'];
        this.status.Invitation = res['Invitation'];
        this.status.Onbreak = res['Onbreak'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }
  

  openDialog(userId) {
    debugger;
    this.service.getPasswordResetUrl(userId, '#/changePassword').subscribe(res => {
      debugger;
      console.log(res);
      if (res['success']) {
        this.url = res['response']
        this.dialog.open(DialogBoxComponentForURL, {
          width: '650px',
          data: { url: this.url },
          disableClose: false
        })
      }

    }
      , err => {
        debugger;
        console.log(err);
        this.service.ErrorSuccess('Agent Can not be deleted!');
      }
    )
  }

  deleteAgent(userId) {
    this.service.deleteAgent(userId).subscribe(res => {
      if (res['success']) {
        debugger;
        this.service.showToaster('Agent has been deleted');
        this.ngOnInit()

      }
    }, err => {
      console.log(err);
      this.service.ErrorSuccess('Agent Can not be deleted!');
    }
    )
  }

  clearFilters() {
    this.search = {
      firstname: '',
      lastname: '',
      username: '',
      AvailableStartTime: '',
      AvailableEndTime: '',
      timeOffset: '',
    }
    this.filter = '';
    this.getAllAgent()
  }


  agentpopup(): void {
debugger;
    const dialogRef = this.dialog.open(DialogBoxComponentForURL, {
      width: '650px',
      disableClose: true,

  });
  //   dialogRef.afterClosed().subscribe(result => {
  //     if(result){
  //       this.ngOnInit();
  //     }
  // })
}
  
editpopup(agent){
  const dialogRef = this.dialog.open(DialogBoxComponentForURL, {
    width: '650px',
    disableClose: true,
    data: {
      agent
    }
});
  dialogRef.afterClosed().subscribe(result => {
    if(result){
      this.ngOnInit();
    }
    
  // if (result === undefined || result === {} || result === ''){
  //   return;
  // }
  // // result._id=list._id
  // this.service.editAgent(result).subscribe(res =>{
  //   debugger;
  //   this.service.showToaster('Agent is Updated');
  //   this.ngOnInit();
  // }, err =>{
  //   debugger;
  // })
});

}
  filters(search) {
    debugger;
    this.filter = '';
    this.pageSize = 10;
    this.pageIndex = 0;
    if (search.firstname != null && search.firstname != '') {

      this.filter = this.filter + '&firstname=' + search.firstname

    }

    if (search.lastname != null && search.lastname != '') {

      this.filter = this.filter + '&lastname=' + search.lastname

    }
    if (search.username != null && search.username != '') {

      this.filter = this.filter + '&username=' + search.username

    }
    if (search.AvailableStartTime != null && search.AvailableStartTime != '') {

      this.filter = this.filter + '&AvailableStartTime=' + search.AvailableStartTime

    }
    if (search.AvailableEndTime != null && search.AvailableEndTime != '') {

      this.filter = this.filter + '&AvailableEndTime=' + search.AvailableEndTime

    }
    if (search.timeOffset != null && search.timeOffset != '') {

      this.filter = this.filter + '&timeOffset=' + search.timeOffset

    }
    
    this.getAllAgent()
    
  }
filtercard(status){
  debugger;
  this.filter = '';
  if (status != null && status != '') {

    this.filter = this.filter + '&Status=' + status

  }
  this.getAllAgent()
}


  addAgent() {
    debugger;
    this.router.navigate(['/dashboard/pipelineadmin/agentform'])
  }

  changePage(event) {
    // console.log(event);

    this.pageSize = event.pageSize
    this.pageIndex = event.pageIndex
    this.previousPageIndex = event.previousPageIndex
    this.service.getAllAgent(event.pageIndex, this.filter, event.pageSize, this.property, this.order,this.n).subscribe((res) => {
      if (res['statusCode'] === 200) {
        this.locationData = res['content']['response'];
        if (this.locationData.length > 0) {
          this.dataSource = new MatTableDataSource(this.locationData);
          this.dataSource.sort = this.sort;
        }
        else {
          this.pageIndex = event.pageIndex - 1
          this.previousPageIndex = event.previousPageIndex
          this.pageSize = event.pageSize - 1
          this.service.getAllAgent(event.pageIndex - 1, this.filter, event.pageSize, this.property, this.order,this.n).subscribe((res) => {
            if (res['statusCode'] === 200) {
              this.locationData = res['content']['response'];
              this.dataSource = new MatTableDataSource(this.locationData);
              this.dataSource.sort = this.sort;
            }
          }, (err) => {
            // 
            // console.log(err);
            // 
            this.message = err['error']['errorMessage'];
            this.service.ErrorSuccess(this.message);
          });
          this.dataSource = new MatTableDataSource(this.locationData);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }
    }, (err) => {
      // 
      // console.log(err);
      // 
      this.message = err['error']['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });

  }

  public toggle(event: MatSlideToggleChange, agent) {
    debugger;


    if (event.checked) {

      let agentDetails = {
        userId: agent['userId'],
        AgentStatus: 'Inactive',
      };
      this.service.activateAgent(agentDetails).subscribe(res => {
        debugger;
        this.service.warningSuccess(` deactivate Successfully`);
        this.initialPage = {
          length: this.length,
          pageIndex: this.pageIndex,
          pageSize: this.pageSize,
          previousPageIndex: this.previousPageIndex
        }

        this.ngOnInit()
        //this.length -=1
        // this.getAllCompany();
      }, error => {
        this.message = error['error']['errorMessage'];
        this.service.ErrorSuccess(this.message);
      })
    } else if (!event.checked) {
      debugger;
      let agentDetails = {
        userId: agent['userId'],
        AgentStatus: 'Active',
      };
      this.service.activateAgent(agentDetails).subscribe(res => {
        this.service.showToaster(` activate Successfully`);
        this.initialPage = {
          length: this.length,
          pageIndex: this.pageIndex,
          pageSize: this.pageSize,
          previousPageIndex: this.previousPageIndex
        }
        this.ngOnInit();

        //this.length -=1
        //this.getAllCompany();
      }, error => {
        this.message = error['error']['errorMessage'];
        this.service.ErrorSuccess(this.message);
      })
    }
  }

  editAgent(agentData) {
    debugger;
    this.router.navigate(['/dashboard/pipelineadmin/agentform'], { state: { agentData } })
  }

  sortData(event) {
    this.order = event.direction;
    this.pageIndex = 0;
    this.pageSize = 10;
    if (event.active == 'firstname') {
      this.property = 'FirstName';
    }
    if (event.active == 'lastname') {
      this.property = 'LastName';
    }
    //  if(event.active == 'companyName'){
    //   this.property='Company.CompanyName';
    //  }
    if (event.active == 'username') {
      this.property = 'Email';
    }
    if (event.active == 'AgentStatus') {
      this.property = 'Status';
    }

    this.ngOnInit();
  }

}



@Component({
  selector: 'dialogbox',
  templateUrl: 'dialogbox.html',
  styleUrls: ['./agent-list.component.scss'],
  providers: []
})

export class DialogBoxComponentForURL {

  url: any = '';
  copyflag = false;

  constructor(public dialogRefForImportCandidate: MatDialogRef<DialogBoxComponentForURL>, @Inject(MAT_DIALOG_DATA) public data: any, public router: Router, public dialog: MatDialog) {
    this.url = this.data.url;
  }
  closedialog() {
    this.dialogRefForImportCandidate.close();
  }
  copyUrl() {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.copyflag = true;
  }
}