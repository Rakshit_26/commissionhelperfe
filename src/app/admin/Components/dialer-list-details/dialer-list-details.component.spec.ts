import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialerListDetailsComponent } from './dialer-list-details.component';

describe('DialerListDetailsComponent', () => {
  let component: DialerListDetailsComponent;
  let fixture: ComponentFixture<DialerListDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialerListDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialerListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
