import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialerListComponent } from './dialer-list.component';

describe('DialerListComponent', () => {
  let component: DialerListComponent;
  let fixture: ComponentFixture<DialerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
