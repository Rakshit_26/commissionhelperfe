import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DataSource } from '@angular/cdk/table';
// import { CompanycomponentService } from 'src/app/companycomponent.service';
import { AgentService } from '../../../shared/services/agent/agent.service';
declare var $: any;

@Component({
  selector: 'app-dialer-list',
  templateUrl: './dialer-list.component.html',
  styleUrls: ['./dialer-list.component.scss'],
  providers: [AgentService]
})
export class DialerListComponent  {
  dataSource: MatTableDataSource<any>;
  message: any;
  showAdvanceFilter = false;
  allCompanies = [];
  public displayedColumns = ['S No', 'CallScript', 'agent', 'dials', 'managerusername', 'salesRepusername', 'listName', 'CompanyName', 'viewDetails'];
  length: number = 0;
  limit: number = 10;
  filter: string = "";
  totalCount = 0;
  AllList: any = [];
  maxDate : any;
  startDate : any;
  AgentId = '';
  DialerListId = '';
  property: any = 'test';
  order: any = 'desc';
  pageLimit: number[] = [5, 10] ;
  pageSize: number = 10;
  pageIndex : number = 1;
  gridView :[] ;
  public mySelection: string[] = [];
  previousPageIndex: number = 0;
  initialPage = {
    "length": 1,
    "pageIndex": 0,
    "pageSize": 5,
    "previousPageIndex": 0
  }
  formData: any = {};

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  pageEvent: PageEvent;
   constructor(private router: Router, public dialog: MatDialog, public service : AgentService ) {
    this.dataSource = new MatTableDataSource<any>();
   }

   ngOnInit() {
  
    this.getDialerList();
    this.getAgentDropdown();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  getDialerList(){

  this.service.getAllDialerList().subscribe((res) => {
      debugger;
        // this.allCompanies = res['dialerList'];
        // this.dataSource = new MatTableDataSource(this.allCompanies);
        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
        // this.limit = this.pageSize  
        // this.length = res['totalCount'];

      this.dataSource = new MatTableDataSource(res['dialerList']);
      this.totalCount = res['dialerList'].length;
      this.gridView=res['dialerList']
      this.dataSource.paginator = this.paginator;
      this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
            case 'managerusername': return item.managerId.username;
            case 'salesRepusername': return item.salesRepId.username;
            default: return item[property];
          }

        };
      this.dataSource.sort = this.sort;



    }, (err) => {

      // console.log(err);
     debugger;
     this.message = err.error['errorMessage'];
     this.service.ErrorSuccess(this.message);
    });
  }

  getAgentDropdown(){
    debugger;
    this.service.getAgentList().subscribe(res => {
      debugger;
      console.log(res);
      if (res['success']){
        this.AllList = res['AgentList'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }

  agentDetails: any

postAgentDetails(event , data){
  debugger;
  this.agentDetails = {
  AgentId: event.value,
  DialerListId: data._id
}
  debugger;
  this.service.agentListInfo(this.agentDetails).subscribe((res) => {
    debugger;



  }, (err) => {
    this.message = err.error['errorMessage'];
    this.service.ErrorSuccess(this.message);
  });
}

  onAgentChange(event){
    debugger

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  changePage(event) {

  }

  viewDetails(contactList){
    debugger;
    localStorage.setItem('contactList', contactList._id) // companyData._Id
    this.router.navigate(['/dashboard/pipelineadmin/dialerlistdetails'], {state: {contactList}});
  }


  //  sortData(event){
  //   debugger;
  //   this.order=event.direction;
  //   this.property=event.active;

  //   this.service.getAllDialerList().subscribe(res =>{
  //     debugger;
  //      this.allCompanies = res['dialerList'];
  //      this.length = res['totalCount'];

  //      this.dataSource = new MatTableDataSource(this.allCompanies);

  //      this.dataSource.sort = this.sort;



  //  }, (err) => {

  //    // console.log(err);

  //    this.message = err.error['errorMessage'];
  //    this.service.ErrorSuccess(this.message);
  //  });

  // }

  showAdvancedFilter(value){
    this.showAdvanceFilter = value;
 }

}

