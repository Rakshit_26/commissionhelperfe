import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgentService } from '../../../shared/services/agent/agent.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public user;

  constructor(private router:Router,private service: AgentService) { }

  ngOnInit(): void {
  
  }
 
  logout(){
    localStorage.clear();
this.router.navigate(['/login'])
  }

}
