import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LocationListComponent } from "../location-list/location-list.component";
import { LocationService } from 'app/shared/services/location.service';

@Component({
    selector: 'app-location',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.scss'],
    providers: [LocationService]
})
export class LocationComponent implements OnInit {

    locationData: any;
    controlName: string;
    matchingControlName: string;
    updateLocationButton = false
    locationDataforEdit: any ;
    locationdataList: any;
    message: any;
    title: any = [
        { value: 'Mr', Name: 'Mr' },
        { value: 'Mrs', Name: 'Mrs' },
        { value: 'Miss', Name: 'Miss' }
    ]

    formGroup: FormGroup;
    submitted = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        public router: Router,
        private service: LocationService,
        public dialogRef: MatDialogRef<LocationListComponent>
    ) {
        if (this.data != null) {
            this.locationDataforEdit = this.data;
            this.updateLocationButton = true;
        }
    }
    ngOnInit(): void {
        debugger;

        if ((this.locationDataforEdit !== undefined) && (Object.keys(this.locationDataforEdit) && Object.keys(this.locationDataforEdit).length !== 0 && this.locationDataforEdit.constructor === Object)) {
            this.updateLocationButton = true;
            this.formGroup = this.formBuilder.group({
                'Name': [this.locationDataforEdit.Name, Validators.required],
                'Address': [this.locationDataforEdit.Address, Validators.required],
                'City': [this.locationDataforEdit.City, Validators.required],
                'State': [this.locationDataforEdit.State, Validators.required],
                'Country': [this.locationDataforEdit.Country, Validators.required],

            });
        }
        else {
            debugger;
            this.updateLocationButton = false;
            this.formGroup = this.formBuilder.group({
                'Name': ['', Validators.required],
                'Address': ['', Validators.required],
                'City': ['', Validators.required],
                'State': ['', Validators.required, Validators.email],
                'Country': ['', Validators.required]
            });
        }
    }

    get f() { return this.formGroup.controls; }

    onSubmit(post) {
        debugger;
        this.submitted = true;

        // stop here if form is invalid
        if (this.formGroup.invalid) {
            return;
        }
        if (this.updateLocationButton) {
            post._id = this.locationDataforEdit._id;
            this.service.editCenterDetail(post).subscribe(res => {
                if (res['success']) {
                    debugger;

                    this.service.showToaster('Location has been Updated');
                }
            }, err => {
                console.log(err);

                this.service.ErrorSuccess('Location Can not be updated!');
            })
        }
        else {
            // post.url = "http://localhost:4200/#/changePassword";
            this.service.saveCenterDetail(post)
                .subscribe((res) => {
                    if (res['success']) {
                        // console.log('sucessssssssssssssssssss');

                        debugger;
                        this.service.showToaster('Location has been registered');
                    }
                }, (err) => {
                    console.log(err);
                    this.service.ErrorSuccess('Location Can not be registered!');
                })
        }
        this.dialogRef.close(true);
    }
}
