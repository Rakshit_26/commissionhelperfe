import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { AgentService } from '../../../shared/services/agent/agent.service';
import { SocketService } from '../../../shared/services/socket/socket.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-agent-livetracking',
  templateUrl: './agent-livetracking.component.html',
  styleUrls: ['./agent-livetracking.component.scss'],
  providers: [SocketService]
})
export class AgentLivetrackingComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  message: any;
  showAdvanceFilter = false;
  allCompanies = [];
  gridView=[];
  public displayedColumns = ['S No', 'firstname', 'lastname', 'agentId', 'username', 'AvailableStartTime', 'AvailableEndTime', 'timeOffset', 'AgentStatus', 'color', 'CallingContactId'];
  length: number = 0;
  limit: number = 10;
  filter: string = "";
  totalCount = 0;
  maxDate: any;
  startDate: any;
  private _docSub: Subscription;
  property: any = 'test';
  order: any = 'desc';
  pageLimit: number[] = [5, 10];
  // pageSize:number =10;
  // pageIndex : number = 1;
  pageSize: number = 10;
  pageIndex: number = 1;
  previousPageIndex: number = 0;
  initialPage = {
    "length": 1,
    "pageIndex": 0,
    "pageSize": 5,
    "previousPageIndex": 0
  }
  d = new Date();
  n = this.d.getTimezoneOffset();

  locationData: any;

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  pageEvent: PageEvent;
  constructor(private router: Router, public dialog: MatDialog, public service: AgentService, private socketservice: SocketService) {
    this.dataSource = new MatTableDataSource<any>();
  }
  ngOnInit() {
    debugger;
    let timeOffsetDifference = this.n;
    //this.getTrackAgent();  
    this.socketservice.trackAgent();

    this._docSub = this.socketservice.agentList.subscribe(doc => {
      this.locationData = doc['AgentList']
      this.gridView = doc['AgentList']
      debugger;
      console.log(doc);
      for (let i = 0; i < this.locationData.length; i += 1) {
        let AgentAvailableStartTime = '';
        let AgentAvailableEndTime = '';
        if ((this.locationData[i].AvailableStartTime - timeOffsetDifference) < 0) {
          this.locationData[i].AvailableStartTime = this.locationData[i].AvailableStartTime + 1440;
        }
        if ((this.locationData[i].AvailableEndTime - timeOffsetDifference) < 0) {
          this.locationData[i].AvailableEndTime = this.locationData[i].AvailableEndTime + 1440;
        }
        if (Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60) < 10) {
          AgentAvailableStartTime = `0${Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60)}:`
        } else {
          AgentAvailableStartTime = `${Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60)}:`
          if (parseInt(AgentAvailableStartTime) >= 24) {
            AgentAvailableStartTime = (parseInt(AgentAvailableStartTime) - 24).toString();
          }
        }
        if (((this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60) < 10) {
          AgentAvailableStartTime = AgentAvailableStartTime + `0${(this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60}`
        } else {
          AgentAvailableStartTime = AgentAvailableStartTime + `${(this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60}`
        }
        if (Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60) < 10) {
          AgentAvailableEndTime = `0${Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60)}:`
        } else {
          AgentAvailableEndTime = `${Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60)}:`
          if (parseInt(AgentAvailableEndTime) >= 24) {
            AgentAvailableEndTime = (parseInt(AgentAvailableEndTime) - 24).toString();
          }
        }
        if (((this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60) < 10) {
          AgentAvailableEndTime = AgentAvailableEndTime + `0${(this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60}`
        } else {
          AgentAvailableEndTime = AgentAvailableEndTime + `${(this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60}`
        }

        this.locationData[i].AvailableStartTime1 = AgentAvailableStartTime;
        this.locationData[i].AvailableEndTime1 = AgentAvailableEndTime;
      }

      this.locationData = this.locationData.map(elm => ({
        AgentStatus: elm.AgentStatus,
        AvailableEndTime: elm.AvailableEndTime1,
        AvailableStartTime: elm.AvailableStartTime1,
        CallingContactId: elm.CallingContactId,
        CallingLeadId: elm.CallingLeadId,
        CompanyId: null,
        agentId: elm.agentId,
        createdAt: elm.createdAt,
        isDeleted: elm.isDeleted,
        timeOffset: elm.timeOffset,
        _id: elm._id
      }));
      this.dataSource = new MatTableDataSource(this.locationData);
      this.totalCount = this.locationData.length;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'firstname': return item.agentId.firstname;
          case 'lastname': return item.agentId.lastname;
          case 'agentId': return item.agentId._id;
          case 'username': return item.agentId.username;
          default: return item[property];
        }

      };
      this.dataSource.sort = this.sort;

    }
    )

  }
  // ngOnDestroy() {
  //   this._docSub.unsubscribe();
  //   }
  getTrackAgent() {
    // let timeOffsetDifference = this.n;
    // debugger;
    // this.service.getAllTrackAgent().subscribe((res) => {
    //   debugger;
    //   this.locationData = res['agentList'];
    //   for (let i = 0; i < this.locationData.length; i += 1) {
    //     let AgentAvailableStartTime = '';
    //     let AgentAvailableEndTime = '';
    //     if ((this.locationData[i].AvailableStartTime - timeOffsetDifference) < 0) {
    //       this.locationData[i].AvailableStartTime = this.locationData[i].AvailableStartTime + 1440;
    //     }
    //     if ((this.locationData[i].AvailableEndTime - timeOffsetDifference) < 0) {
    //       this.locationData[i].AvailableEndTime = this.locationData[i].AvailableEndTime + 1440;
    //     }
    //     if (Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60) < 10) {
    //       AgentAvailableStartTime = `0${Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60)}:`
    //     } else {
    //       AgentAvailableStartTime = `${Math.floor((this.locationData[i].AvailableStartTime - timeOffsetDifference) / 60)}:`
    //       if (parseInt(AgentAvailableStartTime) >= 24) {
    //         AgentAvailableStartTime = (parseInt(AgentAvailableStartTime) - 24).toString();
    //       }
    //     }
    //     if (((this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60) < 10) {
    //       AgentAvailableStartTime = AgentAvailableStartTime + `0${(this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60}`
    //     } else {
    //       AgentAvailableStartTime = AgentAvailableStartTime + `${(this.locationData[i].AvailableStartTime - timeOffsetDifference) % 60}`
    //     }
    //     if (Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60) < 10) {
    //       AgentAvailableEndTime = `0${Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60)}:`
    //     } else {
    //       AgentAvailableEndTime = `${Math.floor((this.locationData[i].AvailableEndTime - timeOffsetDifference) / 60)}:`
    //       if (parseInt(AgentAvailableEndTime) >= 24) {
    //         AgentAvailableEndTime = (parseInt(AgentAvailableEndTime) - 24).toString();
    //       }
    //     }
    //     if (((this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60) < 10) {
    //       AgentAvailableEndTime = AgentAvailableEndTime + `0${(this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60}`
    //     } else {
    //       AgentAvailableEndTime = AgentAvailableEndTime + `${(this.locationData[i].AvailableEndTime - timeOffsetDifference) % 60}`
    //     }

    //     this.locationData[i].AvailableStartTime1 = AgentAvailableStartTime;
    //     this.locationData[i].AvailableEndTime1 = AgentAvailableEndTime;
    //   }

    //   this.locationData = this.locationData.map(elm => ({
    //     AgentStatus: elm.AgentStatus,
    //     AvailableEndTime: elm.AvailableEndTime1,
    //     AvailableStartTime: elm.AvailableStartTime1,
    //     CallingContactId: elm.CallingContactId,
    //     CallingLeadId: elm.CallingLeadId,
    //     CompanyId: null,
    //     agentId: elm.agentId,
    //     createdAt: elm.createdAt,
    //     isDeleted: elm.isDeleted,
    //     timeOffset: elm.timeOffset,
    //     _id: elm._id
    //   }));
    //   this.dataSource = new MatTableDataSource(this.locationData);
    //   this.totalCount = this.locationData.length;
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sortingDataAccessor = (item, property) => {
    //     switch (property) {
    //       case 'firstname': return item.agentId.firstname;
    //       case 'lastname': return item.agentId.lastname;
    //       case 'agentId': return item.agentId._id;
    //       case 'username': return item.agentId.username;
    //       default: return item[property];
    //     }

    //   };
    //   this.dataSource.sort = this.sort;
    // }, (err) => {

    //   // console.log(err);
    //   debugger;
    //   this.message = err.error['errorMessage'];
    //   this.service.ErrorSuccess(this.message);
    // });

    this.socketservice.trackAgent();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    this.dataSource.filterPredicate = (data, filter) => {
      const dataStr = data.agentId.firstname + data.agentId.lastname + data.agentId._id + data.agentId.username;
      return dataStr.indexOf(filter) != -1;
    }
    if (this.dataSource.paginator) {

      this.dataSource.paginator.firstPage();
    }
  }

  refresh() {
    debugger;
    this.getTrackAgent();
  }

  changePage(event) {


  }

  viewDetails(contactList) {
    debugger;
    localStorage.setItem('contactList', contactList._id) // companyData._Id
    this.router.navigate(['/dashboard/pipelineadmin/dialerlistdetails'], { state: { contactList } });
  }




  showAdvancedFilter(value) {
    this.showAdvanceFilter = value;
  }

}



