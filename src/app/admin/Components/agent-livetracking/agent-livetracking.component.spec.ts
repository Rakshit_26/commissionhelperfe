import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentLivetrackingComponent } from './agent-livetracking.component';

describe('AgentLivetrackingComponent', () => {
  let component: AgentLivetrackingComponent;
  let fixture: ComponentFixture<AgentLivetrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentLivetrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentLivetrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
