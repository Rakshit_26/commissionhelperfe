import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgentService } from '../../../shared/services/agent/agent.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TimeZoneList } from '../../../shared/services/timezone';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AgentListComponent } from '../agent-list/agent-list.component';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.scss'],
  providers: [AgentService, TimeZoneList]
})
export class AgentComponent implements OnInit {
  agentData: any;
  controlName: string;
  matchingControlName: string;
  TimeZoneListDetails = [];
  updateAgentButton = false
  agentDataforEdit: any = {}
  agentdataList: any;
  message: any
  Titles: any = [
    { value: 'Mr', Name: 'Mr' },
    { value: 'Mrs', Name: 'Mrs' },
    { value: 'Miss', Name: 'Miss' }
  ]

  formGroup: FormGroup;
  submitted = false;

   d = new Date();
   n = this.d.getTimezoneOffset();


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    public router: Router,
    private service: AgentService, private timeZoneList: TimeZoneList,
     public dialogRef: MatDialogRef<AgentListComponent>
) {
  if (this.data != null){
    this.agentDataforEdit = this.data.agent;
    this.updateAgentButton = true;
  }
    // this.agentDataforEdit = this.router.getCurrentNavigation().extras.state != null ? this.router.getCurrentNavigation().extras.state.agentData : {};
    // if (this.agentDataforEdit != {}) {
    //   this.updateAgentButton = true;

    // }
                                                              
  }



  ngOnInit(): void {
    debugger;

    this.TimeZoneListDetails = this.timeZoneList.aryIannaTimeZones;

    if ((this.agentDataforEdit !== undefined) && (Object.keys(this.agentDataforEdit).length !== 0 && this.agentDataforEdit.constructor === Object)) {

      this.updateAgentButton = true;
      this.formGroup = this.formBuilder.group({
        'firstname': [this.agentDataforEdit.firstname, Validators.required],
        'lastname': [this.agentDataforEdit.lastname, Validators.required],
        'title': [this.agentDataforEdit.title, Validators.required],
        'shift': [this.agentDataforEdit.shift],
        'username': [this.agentDataforEdit.username, [Validators.required, Validators.email]],
        'AvailableStartTime': [this.agentDataforEdit.AvailableStartTime, Validators.required],
        'AvailableEndTime': [this.agentDataforEdit.AvailableEndTime, Validators.required],
        'timeOffset': [this.agentDataforEdit.timeOffset, Validators.required]
      });
    }
    else {
      debugger;
      this.updateAgentButton = false;
      this.formGroup = this.formBuilder.group({
        'firstname': ['', Validators.required],
        'lastname': ['', Validators.required],
        'title': ['', Validators.required],
        'shift': [''],
        'username': ['', [Validators.required, Validators.email]],
        'AvailableStartTime': ['', Validators.required],
        'AvailableEndTime': ['', Validators.required],
        'timeOffset': ['', Validators.required]
      });
    }
  }


  get f() { return this.formGroup.controls; }

  onSubmit(post) {
    debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.formGroup.invalid) {
      return;
    }
    if (this.updateAgentButton) {
      post.userId = this.agentDataforEdit.userId;
      post.timeDifference=this.n;
      this.service.editAgent(post).subscribe(res => {
        if (res['success']) {
          debugger;

          this.service.showToaster('Agent has been Updated');
          // this.router.navigate(['/dashboard/pipelineadmin/agentlist']);
        }
      }, err => {
        console.log(err);

        this.service.ErrorSuccess('Agent Can not be updated!');
      })
    }
    else {
      post.url = "http://localhost:4200/#/changePassword"
      post.timeDifference=this.n;
      this.service.addAgent(post).subscribe(res => {
        if (res['success']) {
          debugger;
          this.service.showToaster('Agent has been registered');
          // this.router.navigate(['/dashboard/pipelineadmin/agentlist']);
        }
      }, err => {
        console.log(err);
        this.service.ErrorSuccess('Agent Can not be registered!');
      })
    }
    this.dialogRef.close(true);
  }

  // cancel() {
  //   this.dialogRef.close();
  // }

  // cancel() {
  //   this.router.navigate(['/dashboard/pipelineadmin/agentlist']);
  // }

}
