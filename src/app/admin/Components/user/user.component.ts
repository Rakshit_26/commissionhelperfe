import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserListComponent } from "../user-list/user-list.component";
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userData: any;
controlName: string;
matchingControlName: string;
updateUserButton = false
userDataforEdit: any = {}
userdataList: any;
message: any;
title: any = [
{ value: 'Mr', Name: 'Mr' },
{ value: 'Mrs', Name: 'Mrs' },
{ value: 'Miss', Name: 'Miss' }
]
formGroup: FormGroup;
submitted = false;

constructor(
@Inject(MAT_DIALOG_DATA) public data: any,
private formBuilder: FormBuilder,
public router: Router,
private service: ComissionHelperService,
public dialogRef: MatDialogRef<UserListComponent>
)
{
if (this.data != null){
this.userDataforEdit = this.data.user;
this.updateUserButton = true;
}
}
ngOnInit(): void {
debugger;

if ((this.userDataforEdit !== undefined) && (Object.keys(this.userDataforEdit) && Object.keys(this.userDataforEdit).length !== 0 && this.userDataforEdit.constructor === Object)) {

this.updateUserButton = true;
this.formGroup = this.formBuilder.group({
'Title': [this.userDataforEdit.title, Validators.required],
'Username': [this.userDataforEdit.username, Validators.required],
'Company': [this.userDataforEdit.company, Validators.required],
'Role': [this.userDataforEdit.Role, Validators.required],

});
}
else {
debugger;
this.updateUserButton = false;
this.formGroup = this.formBuilder.group({
'Title': ['', Validators.required],
'Username': ['', Validators.required],
'Company': ['', Validators.required],
'Role': ['', Validators.required],

});
}
}

get f() { return this.formGroup.controls; }

onSubmit(post) {
debugger;
this.submitted = true;

// stop here if form is invalid
if (this.formGroup.invalid) {
return;
}
if (this.updateUserButton) {
post._id = this.userDataforEdit._id;
this.service.edit(post).subscribe(res => {
if (res['success']) {
debugger;

this.service.showToaster('User has been Updated');
}
}, err => {
console.log(err);

this.service.ErrorSuccess('User Can not be updated!');
})
}
else {

this.service.saveDetail(post)
.subscribe((res) => {
if (res['success']) {


debugger;
this.service.showToaster('User has been registered');
}
}, (err) => {
console.log(err);
this.service.ErrorSuccess('User Can not be registered!');
})
}
this.dialogRef.close(true);
}
}