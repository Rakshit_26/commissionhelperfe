import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { HomeComponent } from './Components/home/home.component';
import { UserComponent } from './Components/user/user.component';
import { UserListComponent } from './Components/user-list/user-list.component';
import { LocationComponent } from './Components/location/location.component';
import { LocationListComponent } from './Components/location-list/location-list.component';
import { HeaderComponent } from './Components/header/header.component';
import { DialerListComponent } from './Components/dialer-list/dialer-list.component';
import { DialerListDetailsComponent } from './Components/dialer-list-details/dialer-list-details.component';
import { CompanyhistoryComponent } from './Components/companyhistory/companyhistory.component';
import { CompanydetailsComponent } from './Components/companydetails/companydetails.component';
import { CompanycomponentComponent } from './Components/companycomponent/companycomponent.component';
import { AgentListComponent } from './Components/agent-list/agent-list.component';
import { AgentComponent } from './Components/agent/agent.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { MaterialModule } from '../material/material';
import { SharedModule } from 'app/shared/shared.module';
import { AgentLivetrackingComponent } from './Components/agent-livetracking/agent-livetracking.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from '../../environments/environment';
import { InputsModule } from '@progress/kendo-angular-inputs';
;
const config: SocketIoConfig = {
  url: environment.URL, options: {
    query: { Authorization: localStorage.token }
  }
};


@NgModule({
  declarations: [HomeComponent, UserComponent, UserListComponent, LocationComponent, LocationListComponent, HeaderComponent, DialerListComponent, DialerListDetailsComponent, CompanyhistoryComponent, CompanydetailsComponent, CompanycomponentComponent, AgentLivetrackingComponent, AgentListComponent, AgentComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    GridModule,
    ButtonsModule,
    MaterialModule,
    InputsModule,
    ReactiveFormsModule,
    DialogModule,
    SharedModule,
    SocketIoModule.forRoot(config)
  ]
})
export class AdminModule { }
