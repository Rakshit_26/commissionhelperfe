import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseMenuRoutingModule } from './base-menu-routing.module';
import { HomeComponent } from './Components/home/home.component';
import { SharedModule } from "../shared/shared.module";



@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    BaseMenuRoutingModule,
    SharedModule
  ]
})
export class BaseMenuModule { }
