import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userName: any
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    debugger;
    this.userName = localStorage.userName
  }
  company(){
    this.router.navigate(['/dashboard/pipelineadmin/company'])
  }
   
  agent(){
    debugger;
    this.router.navigate(['/dashboard/pipelineadmin/agentlist'])
  }
}
