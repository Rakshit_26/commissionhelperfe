import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DialerSessionComponent } from './dialer-session/dialer-session.component';
import { MyListComponent } from './my-list/my-list.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { SettingComponent } from './setting/setting.component';
import { ReportsComponent } from './reports/reports.component';
import { SFComponent } from './sf/sf.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
    path: '', component: SFComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'dialerSession', component: DialerSessionComponent },
      { path: 'scheduler', component: SchedulerComponent },
      { path: 'mylist', component: MyListComponent },
      { path: 'setting', component: SettingComponent },
      { path: 'report', component: ReportsComponent }
    ]

  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesforceRoutingModule { }
