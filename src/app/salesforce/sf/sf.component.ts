import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DialerService } from 'app/shared/services/dialer/dialer.service';
import { RoleService } from 'app/shared/services/role/role.service';
import { SocketService } from 'app/shared/services/socket/socket.service';
import { RouteInfo } from 'app/shared/sidebar/sidebar.metadata';

@Component({
  selector: 'app-sf',
  templateUrl: './sf.component.html',
  styleUrls: ['./sf.component.scss'],
  providers:[SocketService,DialerService,RoleService]
})
export class SFComponent implements OnInit {

  URL = '';
  accessToken: any;
  _docSub: Subscription;
  contactData: any;
  constructor(
    private router: Router,
    private dialerService: DialerService,
    private roleService: RoleService,
    private socketservice: SocketService
    ) {}

  ngOnInit(): void {
    debugger;
    this.newidebar();
    this.URL = this.router.url;
    if(!this.URL){
        this.router.navigate(['/notfound']);
      }
    this.accessToken = this.URL.split('/');
    this.accessToken = this.accessToken[3];
    this.dialerService.verifytoken(this.accessToken).subscribe(res=>{
      debugger;
        if(res['success']){
          debugger;
          localStorage.setItem('token', this.accessToken);
          localStorage.setItem('oauthToken', res['User']['oauthToken']);
          localStorage.setItem('refreshToken', res['User']['refreshToken']);
          localStorage.setItem('role', res['User']['Role']);
          localStorage.setItem('userName', res['User']['username']);
          localStorage.setItem('userid', res['User']['_id']);
          //this.router.navigate([this.roleService.checkRoutes()]);
          this._docSub = this.socketservice.connect.subscribe((doc) =>{
            debugger;
            console.log(doc)
           }
          );
          this.socketservice.getForwardedCall.subscribe(res=> {
            debugger;
            this.contactData = res['Contact'];
            alert(`Call Forwarded!  Contact No.: +${res['Contact']['ContactList']['Phone']}`);
          });
          this.socketservice.createConnection();
          this.router.navigate([this.roleService.checkRoutes()]);
        } else{
          this.router.navigate(['/notfound']);
        }
      }, err=>{
        this.router.navigate(['/notfound']);
      })
        }
        


newidebar() {
  debugger;
    var token = localStorage.getItem('token');
     const SFROUTES: RouteInfo[] = [
      {
        path: `/layout/salesforce/${token}/home`, title: 'Home', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },
      {
        path: `/layout/salesforce/${token}/dialerSession`, title: 'dialer Session', icon: 'fa fa-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },
      {
        path: `/layout/salesforce/${token}/list`, title: 'list', icon: 'fa fa-id-badge', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },
      {
        path: '/layout/dashboard/pipelineadmin/dialerlist', title: ' Dialer Lists', icon: 'fa fa-list-ul', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },
      {
        path: '/layout/dashboard/pipelineadmin/company', title: 'Company', icon: 'fa fa-globe', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },
      {
        path: '/layout/dashboard/pipelineadmin/locationlist', title: 'Location', icon: 'fa fa-globe', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },
      {
        path: '/layout/dashboard/pipelineadmin/userlist', title: 'User', icon: 'fa fa-globe', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      },

      // {
      //     path: '/full-layout', title: 'Full Layout', icon: 'ft-layout', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      // },
      // {
      //     path: '/content-layout', title: 'Content Layout', icon: 'ft-square', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      // },
      // {
      //     path: '', title: 'Menu Levels', icon: 'ft-align-left', class: 'has-sub', badge: '1', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
      //     submenu: [
      //         { path: 'javascript:;', title: 'Second Level', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
      //         {
      //             path: '', title: 'Second Level Child', icon: '', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
      //             submenu: [
      //                 { path: 'javascript:;', title: 'Third Level 1.1', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
      //                 { path: 'javascript:;', title: 'Third Level 1.2', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
      //             ]
      //         },
      //     ]
      // },
      // {
      //     path: '/changelog', title: 'ChangeLog', icon: 'ft-file', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
      // },
      // { path: 'https://pixinvent.com/apex-angular-4-bootstrap-admin-template/documentation', title: 'Documentation', icon: 'ft-folder', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
      // { path: 'https://pixinvent.ticksy.com/', title: 'Support', icon: 'ft-life-buoy', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },

    ];



  }
}
