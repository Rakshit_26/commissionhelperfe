import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SettingComponent } from '../setting/setting.component';
import { HttpClient } from '@angular/common/http';
import { SalesforceService } from '../../shared/services/salesforce.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AgentService } from '../../shared/services/agent/agent.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';




@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.scss']
})
export class MyDialogComponent implements OnInit {
  NameData: FormGroup;
  message: any;
  updateVoiceFile = false;
  token = localStorage.token;

  constructor(private fb: FormBuilder, public router: Router,
    public dialogRef: MatDialogRef<SettingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, public service: SalesforceService, public service1: AgentService) { }

  ngOnInit(): void {
    if (this.data == {} || !this.data) {

      this.updateVoiceFile = false;
      this.NameData = this.fb.group({
        name: [''],
        audio: [''],

      });
    }
    else {
      this.updateVoiceFile = true;
      this.NameData = this.fb.group({
        name: [this.data.voiceMessages.name],
        audio: [],

      });
    }
  }

  cancel() {
    this.dialogRef.close();
  }


  onfileSelected(event) {
    // this.selectedFile = <File>event.target.files[0];

    // if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.NameData.get('audio').setValue(file);
    console.log(event);
    // }
  }



  GetAllVoice() {
    debugger;
    
    this.service.getallVoiceMessage().subscribe(res => {
      debugger
      console.log(res);
     
    }, err => {
      debugger
    })
  }




  onupload(post) {
    debugger;

    if (this.updateVoiceFile) {

      if (!this.NameData.get('audio').value) {
        post._id = this.data.voiceMessages._id;
        post.voiceMsgUrl = this.data.voiceMessages.voiceMsgUrl;
        post.name=post.name;
        post.types = '1';

      }
      else{
        const formData = new FormData();
        formData.append('name', this.NameData.get('name').value);
        formData.append('types', "1");
        formData.append('audio', this.NameData.get('audio').value);
        formData.append('voiceMsgUrl', this.data.voiceMessages.voiceMsgUrl)
        formData.append('_id',this.data.voiceMessages._id)
        post=formData;

      }

      this.dialogRef.close(post);
    }
    else {

      debugger;
      const formData = new FormData();
      formData.append('name', this.NameData.get('name').value);
      formData.append('types', "1");
      formData.append('audio', this.NameData.get('audio').value);
      debugger;
      this.dialogRef.close(formData);
    }


  }


}
