import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SalesforceService } from '../../shared/services/salesforce.service';
import { DialerService } from '../../shared/services/dialer.service';
import { ExportService } from  '../../shared/services/export.service';
import * as XLSX from 'xlsx';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { Observable } from 'rxjs';
import {ConfirmationdialogComponent} from '../confirmationdialog/confirmationdialog.component'
import { BaddataComponent } from './baddata/baddata.component';


let selectedIds:any=[]




@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.scss'],
  providers: [SalesforceService, DialerService]
})
export class MyListComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  gridData:any =[];
  public connectRate :String="93%";
  AllList: any = [];
  IDArray = [];
  listId = [];
  Listdata;
  Listdata1: Variable;
  Listdata2: Variable;
  data: Variable;
  export: Variable;
  isChecked;
  forkJoin;
  zipFile;
  getReport:any
  index:number;
  sessionIds:any;
  displayedColumns = ['call', 'listName', 'CallScript','records', 'ListSource', 'RecordType','Noofdata','badData', 'Dials', 'Connect', 'connectRate','Action']; //'lastAttempted', 'LastConnected', 'coverage', 'averageAttempts',
  selectIndex = 0;
  @ViewChild('audioPlayer') audioPlayer: ElementRef;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  pageEvent: PageEvent;
  selection = new SelectionModel<any>(true, []);

  constructor(
    public dialog: MatDialog,
    private salesforceService: SalesforceService,
    private service: DialerService,
    private excelService: ExportService,
   

  ) { }

  ngOnInit(): void {
    this.getAllList();

  }



  getAllList(){
    this.salesforceService.getAllList().subscribe(res=>{
      debugger
      this.AllList = res['Lists'];
      this.gridData=res['Lists'];
      this.dataSource = new MatTableDataSource(this.AllList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, err =>{
      debugger;
      this.service.ErrorSuccess('Unable to load List');
    })

  }
  PostExportasExcel() {
    debugger;
    if(this.selection.selected.length==0){
      this.service.ErrorSuccess('Please select a List ');
      return;
    }
    var data = this.IDArray;
    this.salesforceService.PostExportasExcelCSV({ listId: data }).subscribe(res => {
      this.listId = this.IDArray;
      for (let index = 0; index < res['List'].length; index++) {
      debugger;

      this.excelService.exportAsExcelFile( res['List'][index], 'MyList');
      }


      console.log( res['List'])
      if (res['success']) {
        debugger;
        this.service.showToaster('Excel Saved Successfully');

      }


    }, err => {
      console.log(err);
      this.service.ErrorSuccess('Setting Not Saved!');
    }


    )
  }
  CheckedData(value,isChecked,index) {
    debugger;
    if (isChecked.checked) {
      this.IDArray.push(value._id);
    console.log(this.IDArray);
    }
   else{

    this.IDArray.splice(index,1);
     console.log(this.IDArray);
   }
  }

  Archive(){
    debugger;
    if(this.selection.selected.length==0){
      this.service.ErrorSuccess('Please select a List ');
      return;
    }
    var data = this.IDArray;
    this.salesforceService.PostExportasExcelCSV({ listId: data }).subscribe(res => {
      this.listId = this.IDArray;
      const zip = new JSZip();
      debugger;

      for (let index = 0; index < res['List'].length; index++) {
      debugger;
      const name = res['List'][index].Name + '_export_' + new Date().getTime() + '.xlsx';


      const blob: Blob = this.excelService.createZipBlob( res['List'][index]);
      zip.file(name, blob);
      }

      zip.generateAsync({ type: 'blob' }).then((content) => {
        if (content) {
          FileSaver.saveAs(content, 'name');
        }
      });

      console.log( res['List'])
      if (res['success']) {
        debugger;
        this.service.showToaster('Zip Saved Successfully');

      }
    }, err => {
      console.log(err);
      this.service.ErrorSuccess('Setting Not Saved!');
    }
    )
  }

  openBadData(){
    debugger;
    const dialogRef = this.dialog.open(BaddataComponent, {
      width: '650px',
      disableClose: true,
      
  });
    dialogRef.afterClosed().subscribe(result => {
    
  });
  }
  
  beginUpload(){
    debugger;
    const dialogRef = this.dialog.open(UploadComponent, {
      width: '650px',
      disableClose: true,
  })
  
  }
  openPopUp(type){
    debugger;
    const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      width: '350px',
      disableClose: true,
  });
  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    if (result == true) {
      switch (type) {
        case 'delete':
          this.deleteList();
          break;
        case 'PostExportasExcel':
          this.PostExportasExcel();
          break;
          case 'Archive':
            this.Archive();
            break;
            case 'TransferOwnership':
          this.TransferOwnership();
          break;
          case 'beginUpload':
            this.beginUpload();
            break;
      }
    }
  });
  
  }
listIdObject:any
  deleteList() {
    debugger;
    if(this.selection.selected.length==0){
      this.service.ErrorSuccess('Please select a List ');
      return;
    }
    this.listIdObject = { listId: this.IDArray }
    this.salesforceService.delete(this.listIdObject).subscribe(res => {
      if (res['success']) {
        debugger;
        this.service.showToaster('List has been deleted');
        this.ngOnInit()

      }
    }, err => {
      console.log(err);
      this.service.ErrorSuccess('List Can not be deleted!');
    }
    )
  }
  
  openScript(CallScript, id){
    debugger;
    const dialogRef = this.dialog.open(ScriptComponent, {
      width: '650px',
      disableClose: true,
      data: {
        CallScript,
        _id: id
      }
  });
    dialogRef.afterClosed().subscribe(result => {
    if (result === undefined || result === {} || result === ''){
      return;
    }
    this.salesforceService.updateList(result).subscribe(res =>{
      this.getAllList()
    }, err =>{
      debugger;
      this.service.ErrorSuccess('Cannot update list!');
    })
  });
  }
  // Whether the number of selected elements matches the total number of rows.
  isAllSelected() {
    debugger;
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  //Selects all rows if they are not all selected; otherwise clear selection. /
  masterToggle() {
    debugger
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(row => this.selection.select(row));
  }



  importFromCRM(){
        const dialogRef = this.dialog.open(AddListFormComponent, {
          width: '650px',
          disableClose: true,

      });
        dialogRef.afterClosed().subscribe(result => {
        if (result === undefined || result === {} || result === ''){
          return;
        }

        this.salesforceService.importFromCRM(result).subscribe(res =>{
          debugger;
          this.service.showToaster('List Have been Added');
          this.getAllList();
        }, err =>{
          debugger;
        })
      });

    }
    editList(list) {
    debugger;
    if (list.listSource === 'Excel') {
      this.EdituploadExcel(list);
    }
    else if(list.listSource === 'CRM')
     {
      this.editImportFromCRM(list);
    }

    }
    onOrderChange(event){
      debugger
    }

    EdituploadExcel(list){
      debugger;
      const dialogRef = this.dialog.open(ImportExcelComponent, {
        width: '650px',
        disableClose: true,
        data: {
          list,
          edit: false
        }
    });
      dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result === {} || result === ''){
        return;
      }
      result._id=list._id
      this.salesforceService.editList(result).subscribe(res =>{
        debugger;
        this.service.showToaster('List Have been Updated');
        this.ngOnInit();
      }, err =>{
        debugger;
      })
    });
  }
  editImportFromCRM(list){
    debugger;
    const dialogRef = this.dialog.open(AddListFormComponent, {
      width: '650px',
      disableClose: true,
      data: {
        list,
        edit: false
      }
  });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
    if (result === undefined || result === {} || result === ''){
      return;
    }
    result._id=list._id
    this.salesforceService.editList(result).subscribe(res =>{
      debugger;
      this.service.showToaster('List Have been Updated');
     this.ngOnInit();
    }, err =>{
      debugger;
    })
  });
}
    uploadExcel(){
      debugger;
      const dialogRef = this.dialog.open(ImportExcelComponent, {
        width: '650px',
        disableClose: true,

    });
      dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result === {} || result === ''){
        return;
      }
      this.salesforceService.importExcel(result).subscribe(res =>{
        debugger;
        this.service.showToaster('List Have been Added');
        this.getAllList();
      }, err =>{
        debugger;
      })
    });
  }

 postSalesRepDetails(){
  
   }

  TransferOwnership(){
    debugger;

if(this.selection.selected.length==0){
  this.service.ErrorSuccess('Please select a List ');
  return;
}

    selectedIds = this.selection.selected.map(x=>{
    return x.salesRepId})


    const dialogRef = this.dialog.open(TransferOwnershipComponent, {
      width: '650px',
      disableClose: true
  });
    dialogRef.afterClosed().subscribe(result => {

  });
}
}

@Component({
  selector: 'app-add-list-form',
  templateUrl: './add-list-form.html',
  styleUrls: ['./my-list.component.scss'],
  providers: [SalesforceService, DialerService]
})
export class AddListFormComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  GreetingSource: MatTableDataSource<any>;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  reportList: any = [];
  userList: any = [];
  salesrepList: any = [];
  report: any = {};
  reportName: any = '';
  defaultGreetingId:any ='';
  defaultVmId:any ='';
  greetingMsgList;
  voiceMessageList:any=[];
  vmId = '';
  VMSrc = '';
  totalCount = 0;
  vmPlayFlag = false;
  greetingPlayFlag = false;
  audioPlaying = false;
  audioPlaying1 = false;
  audio: any;
  VoiceList;
  greetingList;
  greeting = '';
  greetingAudio: any;
  formData: any = {};
  edit: boolean = true;

  constructor(
    private dialogRef: MatDialogRef<AddListFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public router: Router,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private salesforceService: SalesforceService,
    private service: DialerService
  ) {
  }

  ngOnInit() {
    this.salesforceService.getAllReports().subscribe(res => {
      debugger;
      console.log(res);
      if (res['success']) {
        this.reportList = res['Output']['records'];
        console.log(this.reportList)
      }
    }, err => {
      debugger;
      console.log(err);
      this.service.ErrorSuccess('No reports available');
    });

    if (this.data != null){
      this.formData = this.data.list;
      this.edit=this.data.edit
    }
    else{
      this.GetAllIsDefaulttpe1()
   this.GetAllIsDefaulttpe2()
    }

    this.getSalesRepList();
    
   this. getManagersList()
   this.getVoiceMessageList()
   this.getGreetings()
  }
  getManagersList(){
    debugger;
    this.salesforceService.getAllManagers().subscribe(res => {
      debugger;
      console.log(res);
      if(res['success']){
        this.userList = res['mangerList'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }
  getSalesRepList(){
    debugger;
    this.salesforceService.getAllSalesRep().subscribe(res => {
      debugger;
      console.log(res);
      if(res['success']){
        this.salesrepList = res['salesRepList'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }



  getVoiceMessageList(){
    debugger;
    this.salesforceService.getallVoiceMessage().subscribe(res => {

      console.log(res);
      this.voiceMessageList = res['voiceMessages'];
      this.dataSource = new MatTableDataSource(res['voiceMessages']);

      this.totalCount = res['voiceMessages']['totalSize'];

    }, err => {

    })
  }


  getGreetings(){
    debugger;
    this.salesforceService.getallGreetings().subscribe(res => {

      console.log(res);
      this.greetingMsgList = res['voiceMessages'];
      this.GreetingSource = new MatTableDataSource(res['voiceMessages']);
      this.totalCount = res['voiceMessages']['totalSize'];

    }, err => {

    })
  }

  GetAllIsDefaulttpe1() {

debugger;
this.salesforceService.getallIsdefaulttype1().subscribe(res => {

  console.log(res);
  this.defaultVmId = res['voiceMessages']['_id'];
  this.formData.vmId = this.defaultVmId

}, err => {

})
  }


GetAllIsDefaulttpe2() {
  debugger;

  this.salesforceService.getallIsdefaulttype2().subscribe(res => {

    console.log(res);
    this.defaultGreetingId = res['voiceMessages']['_id'];

    this.formData.greetingId=this.defaultGreetingId;
  }, err => {

  })
    }


  submit() {
    debugger
    const resData = {
      salesRepId:this.formData.salesRepId,
      managerId: this.formData.managerId,
      listName: this.formData.listName,
      // Name: this.reportName,
      report: this.formData.report,
    vmId: this.formData.vmId,
    greetingId: this.formData.greetingId,
    CallScript: this.formData.CallScript
    };
    this.dialogRef.close(resData);

  }

  cancel() {
    this.dialogRef.close();
  }

  play() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
    this.audio = new Audio();
    this.audio.src = this.VMSrc;
    this.audio.load();
    this.audio.play();
    this.audioPlaying = true;
  }

  pause() {
    this.audio.src = '';
    this.audioPlaying = false;
  }

  audioEnded() {
    this.audio.src = '';
    this.audioPlaying = false;
  }

  play1() {
    this.audio.src = '';
    this.audioPlaying = false;
    this.greetingAudio = new Audio();
    this.greetingAudio.src = this.greeting;
    this.greetingAudio.load();
    this.greetingAudio.play();
    this.audioPlaying1 = true;
  }

  pause1() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }

  audioEnded1() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }
}
@Component({
  selector: 'app-import-Excel',
  templateUrl: './importExcel.html',
  styleUrls: ['./my-list.component.scss'],
  providers: [SalesforceService, DialerService]
})
export class ImportExcelComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  reportList: any = [];
  userList: any = [];
  salesrepList: any = [];
  report: any = {};
  reportName: any ='';
  vmId = '';
  VMSrc = '';
  vmPlayFlag = false;
  greetingPlayFlag = false;
  audioPlaying = false;
  audioPlaying1 = false;
  audio:any;
  greeting = '';
  greetingAudio: any;
  formData: any = {};
  files;
  form;
  arrayBuffer: any;
  ExcelArray: any;
  file: any;
  uploadFiles;
  VoiceList;
  greetingList;
  edit: boolean = true;


  constructor(
    private dialogRef: MatDialogRef<ImportExcelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public router: Router ,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private salesforceService: SalesforceService,
    private service: DialerService
    ) {
  }

  ngOnInit(){
    debugger;
    if (this.data != null){
      this.formData = this.data.list;
      this.edit=this.data.edit
    }

   this.getManagersList()
   this.getSalesRepList()
   this.getVoiceMessageList()
   this.getGreetings()
  }

getManagersList(){
  debugger;
  this.salesforceService.getAllManagers().subscribe(res => {
    debugger;
    console.log(res);
    if(res['success']){
      this.userList = res['mangerList'];
    }
   }, err => {
     debugger;
     console.log(err);
     this.service.ErrorSuccess('Error');
   });
}
getSalesRepList(){
  debugger;
  this.salesforceService.getAllSalesRep().subscribe(res => {
    debugger;
    console.log(res);
    if(res['success']){
      this.salesrepList = res['salesRepList'];
    }
   }, err => {
     debugger;
     console.log(err);
     this.service.ErrorSuccess('Error');
   });
}
getVoiceMessageList(){
  debugger;
  this.salesforceService.getallVoiceMessage().subscribe(res => {
    debugger;
    console.log(res);
    if(res['success']){
      this.VoiceList = res['voiceMessages'];
    }
   }, err => {
     debugger;
     console.log(err);
     this.service.ErrorSuccess('Error');
   });
}
getGreetings(){
  debugger;
  this.salesforceService.getallGreetings().subscribe(res => {
    debugger;
    console.log(res);
    if(res['success']){
      this.greetingList = res['voiceMessages'];
    }
   }, err => {
     debugger;
     console.log(err);
     this.service.ErrorSuccess('Error');
   });
}

  uploadFile(event) {
    debugger;
    let invalidName = false;
    let invalidEmail = false;
    let invalidPhone = false;
    let invalidCity = false;
    let invalidState = false;
     let invalidTitle = false;
    const file = (event.target as HTMLInputElement).files[0];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
    this.arrayBuffer = fileReader.result;
    const data = new Uint8Array(this.arrayBuffer);
    const arr = new Array();
    for (let i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    const bstr = arr.join('');
    const workbook = XLSX.read(bstr, {type: 'binary'});
    let first_sheet_name = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[first_sheet_name];
    // console.log(XLSX.utils.sheet_to_json(worksheet, {raw: true}));
    this.ExcelArray = XLSX.utils.sheet_to_json(worksheet, {raw: true});
    this.ExcelArray.forEach(elm => {
    if (!elm.ContactName || elm.ContactName == '' ){
    invalidName = true;
    }
    if(!elm.Email || elm.Email == ''){
    invalidEmail = true;
    }
    if(!elm.Phone || elm.Phone == ''){
    invalidPhone = true;
    }
    if(!elm.City || elm.City == ''){
    invalidCity = true;
    }
    if(!elm.State || elm.State == ''){
    invalidState = true;
    }
    if(!elm.Title || elm.Title == ''){
      invalidTitle = true;
      }

    });
    if (invalidCity){
    return this.service.ErrorSuccess('City is required field');
    }
    if (invalidName){
    return this.service.ErrorSuccess('Name is required field');
    }
    if (invalidState){
    return this.service.ErrorSuccess('State is required field');
    }
    if (invalidPhone){
    return this.service.ErrorSuccess('Phone is required field');
    }
    if (invalidEmail){
    return this.service.ErrorSuccess('Email is required field');
    }
    if (invalidTitle){
      return this.service.ErrorSuccess('Title is required field');
      }
    // this.sendExcelFile(this.ExcelArray);
    //this.dialogRefForImportCandidate.close(this.ExcelArray);
    };

    fileReader.readAsArrayBuffer(file);

    }
    submit(){
    debugger
    const resData = {
    managerId: this.formData.managerId,
    report: this.ExcelArray,
    listName: this.formData.listName,
    salesRepId: this.formData.salesRepId,
    vmId: this.formData.vmId,
    //greetingId: this.formData.greetingId,
    CallScript: this.formData.CallScript
    };
    this.dialogRef.close(resData);
    }

  cancel(){
    this.dialogRef.close();
  }

  play(){
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
    this.audio = new Audio();
    this.audio.src = this.VMSrc;
    this.audio.load();
    this.audio.play();
    this.audioPlaying = true;
  }

  pause(){
    this.audio.src = '';
    this.audioPlaying = false;
  }

  audioEnded(){
    this.audio.src = '';
    this.audioPlaying = false;
  }

  play1(){
    this.audio.src = '';
    this.audioPlaying = false;
    this.greetingAudio = new Audio();
    this.greetingAudio.src = this.greeting;
    this.greetingAudio.load();
    this.greetingAudio.play();
    this.audioPlaying1 = true;
  }

  pause1(){
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }

  audioEnded1(){
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }
}

@Component({
  selector: 'app-Transfer-Ownership',
  templateUrl: './TransferOwnership.html',
  styleUrls: ['./my-list.component.scss'],
  providers: [SalesforceService, DialerService]
})
export class TransferOwnershipComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  reportList: any = [];
  userList: any = [];
  salesrepList: any = [];
  report: any = {};
  reportName: any = '';
  vmId = '';
  VMSrc = '';
  vmPlayFlag = false;
  greetingPlayFlag = false;
  audioPlaying = false;
  audioPlaying1 = false;
  audio: any;
  VoiceList;
  greetingList;
  greeting = '';
  greetingAudio: any;
  formData: any = {};
salesRepId:any;

  constructor(
    private dialogRef: MatDialogRef<TransferOwnershipComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public router: Router,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
     private salesforceService: SalesforceService,
     private service: DialerService
  ) {
  }

  ngOnInit() {

  this.getSalesRepList();
  }
  getSalesRepList(){
    debugger;
    this.salesforceService.getAllSalesRep().subscribe(res => {
      debugger;
      console.log(res);
      if(res['success']){
        this.salesrepList = res['salesRepList'];


      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }


  submit(event) {
    debugger;
    const resData = {

     salesRepId:this.salesRepId,
      listId:selectedIds
    };
    this.salesforceService.transferOwnership(resData).subscribe(res => {
      debugger;
      console.log(res);

      if(res['success']){
        this.service.showToaster('Transferred Ownership Successfully');
        // this.greetingList = res['voiceMessages'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });

    this.dialogRef.close(resData);

  }

  cancel() {
    this.dialogRef.close();
  }

  play() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
    this.audio = new Audio();
    this.audio.src = this.VMSrc;
    this.audio.load();
    this.audio.play();
    this.audioPlaying = true;
  }

  pause() {
    this.audio.src = '';
    this.audioPlaying = false;
  }

  audioEnded() {
    this.audio.src = '';
    this.audioPlaying = false;
  }

  play1() {
    this.audio.src = '';
    this.audioPlaying = false;
    this.greetingAudio = new Audio();
    this.greetingAudio.src = this.greeting;
    this.greetingAudio.load();
    this.greetingAudio.play();
    this.audioPlaying1 = true;
  }

  pause1() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }

  audioEnded1() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }
}
@Component({
  selector: 'app-add-list-form',
  templateUrl: './script.html',
  styleUrls: ['./my-list.component.scss'],
  providers: [SalesforceService, DialerService]
})
export class ScriptComponent implements OnInit {
CallScript = '';
  constructor(
    private dialogRef: MatDialogRef<ScriptComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public router: Router,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private salesforceService: SalesforceService,
    private service: DialerService
  ) {
  }

  ngOnInit() {
  this.CallScript = this.data.CallScript;
  }


  submit() {
    debugger
    const resData = {
      _id: this.data._id,
      CallScript: this.CallScript
    };
    this.dialogRef.close(resData);
  }

  cancel() {
    this.dialogRef.close();
  }
}
@Component({
  selector: 'app-add-list-form',
  templateUrl: './upload.html',
  styleUrls: ['./my-list.component.scss'],
  providers: [SalesforceService, DialerService]
})
export class UploadComponent implements OnInit {
  CallScript = '';
    constructor(
      private dialogRef: MatDialogRef<UploadComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public router: Router,
      public dialog: MatDialog,
      private _formBuilder: FormBuilder,
      private salesforceService: SalesforceService,
      private service: DialerService
    ) {
    }
  
    ngOnInit() {
 
    }
    uploadFile(){
      debugger;
      // this.salesforceService.getAllSalesRep().subscribe(res => {
      //   debugger;
      //   console.log(res);
      //   if(res['success']){
      //     this.salesrepList = res['salesRepList'];
  
  
      //   }
      //  }, err => {
      //    debugger;
      //    console.log(err);
      //    this.service.ErrorSuccess('Error');
      //  });
    }
  
    
  
    cancel() {
      this.dialogRef.close();
    }
  }
  