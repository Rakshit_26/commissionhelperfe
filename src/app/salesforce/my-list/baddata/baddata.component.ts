import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SalesforceService } from '../../../shared/services/salesforce.service';
import { DialerService } from '../../../shared/services/dialer.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-baddata',
  templateUrl: './baddata.component.html',
  styleUrls: ['./baddata.component.scss']
})
export class BaddataComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<BaddataComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public router: Router,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private salesforceService: SalesforceService,
    private service: DialerService
  ) { }

  ngOnInit(): void {
  }
  


  cancel() {
    this.dialogRef.close();
  }
}
