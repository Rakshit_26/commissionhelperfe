import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaddataComponent } from './baddata.component';

describe('BaddataComponent', () => {
  let component: BaddataComponent;
  let fixture: ComponentFixture<BaddataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaddataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaddataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
