import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialerSessionComponent } from './dialer-session.component';

describe('DialerSessionComponent', () => {
  let component: DialerSessionComponent;
  let fixture: ComponentFixture<DialerSessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialerSessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialerSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
