import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { SalesforceService } from './../../shared/services/salesforce.service';
import { SessionstatusService } from'./../../shared/services/sessionstatus.service';
import { DialerService } from './../../shared/services/dialer.service';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { process, State } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';


@Component({
  selector: 'app-dialer-session',
  templateUrl: './dialer-session.component.html',
  styleUrls: ['./dialer-session.component.scss'],
  providers: [SalesforceService , DialerService]
})
export class DialerSessionComponent implements OnInit {

   public mySelection: string[] = [];
  dataSource: MatTableDataSource<any>;
  displayedColumns = ['call', 'listName', 'RecordType', 'lastAttempted', 'LastConnected', 'Dials', 'Connect', 'connectRate', 'badData', 'coverage', 'averageAttempts'];
  gridData: any[];
 
  selectIndex = 0;
  vmId = '';
  VMSrc = '';
  vmPlayFlag = false;
  greetingPlayFlag = false;
  audioPlaying = false;
  audioPlaying1 = false;
  contactFlag = true;
  leadFlag = false;
  reportsFlag = false;
  callCountFlag = false;
  audio: any;
  VoiceList;
  greetingId;
  greetingList;
  greeting = '';
  greetingAudio: any;
  globalFilter = '';
  totalCount = 0;
  pageSize = 10;
  formData: any = {};
  IDArray = [];
  ReportoneTable;
  ReporttwoTable: any = [];
  data;
  reportarray = [];
  ColumnArray = [];
  Setting: any = {};
  SettingData: any = {};
  CountryCodePrefixed;
 voiceMessageList:any=[];
 sessionDetails:any;
 _a:Variable;
 Columns: any = [];
 


  @ViewChild('audioPlayer') audioPlayer: ElementRef;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  pageEvent: PageEvent;
  selection = new SelectionModel<any>(true, []);

  constructor(
    private sessionStatusService:SessionstatusService,
    private salesforceService: SalesforceService,
    private service: DialerService
  ) { }

  ngOnInit(): void {
    debugger;
    this. getSessionlist();
    this.getAllList();
    this. getVoiceMessageList();
    this.getGreetings();
    this.gettableOneDataReport();
    this.gettabletwoDataReport();
    setTimeout(() => {
      this.matchData();

    }, 2000);

    debugger;
    this.salesforceService.TabletwoReportSetting().subscribe(res => {
      console.log(res);
      this.Columns = res['userColumnReport']['ColumnArray'];


    }, err => {

    })

  
  }

  
  getAllList(){
    this.salesforceService.getAllList().subscribe(res=>{
      debugger;
      this.gridData=res['Lists'];
    
      console.log(res);
    }, err =>{
      debugger;
      this.service.ErrorSuccess('Unable to load List');
    })

  }

  CheckedData(value,isChecked,index) {
    debugger;
    if (isChecked.checked) {
      this.IDArray.push(value._id);
    console.log(this.IDArray);
    }
   else{

    this.IDArray.splice(index,1);
     console.log(this.IDArray);
   }
  }


StartSession(){
  debugger;
  if(!this.sessionStatusService.sessionActive){
    this.sessionStatusService.sessionActive = true;
  }
  else{
    this.sessionStatusService.sessionActive = false;
  }

  
}

  getVoiceMessageList(){
    debugger;
    this.salesforceService.getallVoiceMessage().subscribe(res => {
      debugger;
      console.log(res);
      if(res['success']){
        this.VoiceList = res['voiceMessages'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }
  getGreetings(){
    debugger;
    this.salesforceService.getallGreetings().subscribe(res => {
      debugger;
      console.log(res);
      if(res['success']){
        this.greetingList = res['voiceMessages'];
      }
     }, err => {
       debugger;
       console.log(err);
       this.service.ErrorSuccess('Error');
     });
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  changeVm($event){
 debugger;
 this.vmPlayFlag = true;
  }
  changeGreeting($event){
debugger;
this.greetingPlayFlag = true;
  }

  changeTabs(event) {
    debugger;
    if (event.tab.textLabel == 'Contact') {
      this.selectIndex = 0;
      console.log(0);
      this.contactFlag = true;
      this.leadFlag = false;
      this.reportsFlag = false;
      this.callCountFlag = false;
      this.GetAllContactList();
    }
    else if (event.tab.textLabel == 'Lead') {
      this.selectIndex = 1;
      console.log('Lead');
      this.contactFlag = false;
      this.leadFlag = true;
      this.reportsFlag = false;
      this.callCountFlag = false;
      this.GetAllLeadList();
    }
    else if (event.tab.textLabel == 'Reports') {
      this.selectIndex = 2;
      console.log('Reports');
      this.contactFlag = false;
      this.leadFlag = false;
      this.reportsFlag = true;
      this.callCountFlag = false;
      this.getAllReports();
    }
    else if (event.tab.textLabel == 'Call Counter') {
      this.selectIndex = 3;
      console.log('Call Count');
      this.contactFlag = false;
      this.leadFlag = false;
      this.reportsFlag = false;
      this.callCountFlag = true;
      this.getAlCallCounter();
    }
  }

  play() {
    debugger;
    this.audio.src = '';
    this.audioPlaying1 = false;
    this.audio = new Audio();
    this.audio.src = this.VMSrc;
    this.audio.load();
    this.audio.play();
    this.audioPlaying = true;
  }

  pause() {
    this.audio.src = '';
    this.audioPlaying = false;
  }

  audioEnded() {
    this.audio.src = '';
    this.audioPlaying = false;
  }

  play1() {
    this.audio.src = '';
    this.audioPlaying = false;
    this.greetingAudio = new Audio();
    this.greetingAudio.src = this.greeting;
    this.greetingAudio.load();
    this.greetingAudio.play();
    this.audioPlaying1 = true;
  }

  pause1() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }

  audioEnded1() {
    this.greetingAudio.src = '';
    this.audioPlaying1 = false;
  }

  refresh() {
    debugger
    if (this.selectIndex == 0) {
      this.GetAllContactList();
    }
    else if (this.selectIndex == 1) {
      this.GetAllLeadList();
    }
    else if (this.selectIndex == 2) {
      this.getAllReports();
    }
    else if (this.selectIndex == 3) {
      this.getAlCallCounter();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  

  GetAllContactList() {
    debugger;
    this.displayedColumns = ['call','SalesForceId','AccountName','ContactName','Phone','Email','City','State','createdAt'];
    this.salesforceService.getAllContactList().subscribe(res => {
      debugger
      this.dataSource = new MatTableDataSource(res['ListContacts']);
      this.totalCount = res['ListContacts'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }, err => {
      debugger
    })
  }

  GetAllLeadList() {
    debugger;
    this.displayedColumns = ['call', 'Id', 'Name', 'Phone', 'Email', 'Title', 'City', 'State', 'callCount'];
    this.salesforceService.getAllLead().subscribe(res => {
      debugger
      this.dataSource = new MatTableDataSource(res['Output']['records']);
      this.totalCount = res['Output']['totalSize'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }, err => {
      debugger
    })
  }

  getAllReports() {
    debugger;
    this.displayedColumns = ['call', 'Id', 'Name'];
    this.salesforceService.getAllReports().subscribe(res => {
      debugger
      this.dataSource = new MatTableDataSource(res['Output']['records']);
      this.totalCount = res['Output']['totalSize'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }, err => {
      debugger
    })
  }

  getAlCallCounter() {
    debugger;
  }

  gettableOneDataReport() {

    this.salesforceService.TableoneReportSetting().subscribe(res => {

      console.log(res);
      this.ReportoneTable = res['reportDetails'];


    }, err => {

    })
  }

  matchData() {
    debugger;

    this.ReportoneTable = this.ReportoneTable.filter(item => !this.ReporttwoTable.includes(item));
  }


  gettabletwoDataReport() {

    this.salesforceService.TabletwoReportSetting().subscribe(res => {
      console.log(res);
      this.ReporttwoTable = res['userColumnReport']['ColumnArray'];


    }, err => {

    })

  }


  selectreport(report) {
    debugger
    this._a = report.$index == true ? this.ReportoneTable.push(report) : this.ReportoneTable.splice(0,1);
    this.ReporttwoTable.push(this._a[0]);
    console.log(this.ReporttwoTable);
  }



  postreportData() {
    debugger;
    var data = this.ReporttwoTable;
    this.salesforceService.postReportSetting({ ColumnArray: data }).subscribe(res => {
      debugger;
      this.ColumnArray = this.ReporttwoTable;

      if (res['success']) {
        debugger;
        this.service.showToaster('Settings Saved Successfully');

      }

    })
  }
  deselectreport(report1) {
    this.ReportoneTable.push(...this.ReporttwoTable.splice(this.ReporttwoTable.indexOf(report1), 1));
  }


  // openPopUp(){
  //   debugger;
  //   const dialogRef = this.dialog.open(ActionpopupComponent , {
  //   width: '350px',
  //   disableClose: true,
    
    
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //   console.log('The dialog was closed');
    
  //   debugger;
  //   if(result){
    
  //   //this._id = localStorage.getItem("_id")
  //   this.salesforceService.updateUnavailableContact("5ec7d455790ec92538788eb0").subscribe(res=>{ //this._id
  //   debugger;
  //   console.log(res);
  //   }, err =>{
  //   debugger;
  //   this.service.ErrorSuccess('Updated');
  //   })
  //   }
  //   });
    
  //   }
    
    
 state: State = {
        filter: {
          logic: 'and',
          filters: [{ field: 'AccountName', operator: 'contains', value: 'Chef' },
          { field: 'ContactName', operator: 'contains', value: 'Chef' },
          { field: 'ListId', operator: 'contains', value: 'Chef' },
          { field: 'Status', operator: 'contains', value: 'Chef' },
          { field: 'SalesForceId', operator: 'contains', value: 'Chef' },
          { field: 'Email', operator: 'contains', value: 'Email' }
        
        
        
        ],
          
        }
    };

   
  
    
    getSessionlist(){
    debugger;
  
    this.salesforceService.getContactStatus("5ece2ca04503e2001754c237").subscribe(res=>{ //this.salesRepId
    debugger
    this.gridData=(res['message']);
   
  
    
    console.log(res);
    }, err =>{
    debugger;
    this.service.ErrorSuccess('Unable to load List');
    })
    
    }


}


