import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DialerService } from '../../shared/services/dialer.service';
import * as Chart from 'chart.js';
import { RouteInfo } from 'app/shared/sidebar/sidebar.metadata';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DialerService]
})
export class HomeComponent {

  constructor(private service: DialerService) { }

  userName = '';
  userDetails: any = {};
  cxt: any;
  canvas: any;
  cxtPie: any;
  canvasPie: any;
  cxtDb: any;
  canvasDb: any;
  cxtB: any;
  canvasB: any;
  selectedMonthPie
  selectedMonthDoughnut
  pieChart
  requestedData
  myCharts
  toMonth
  levelsArr
  update
  data

  // dummyData=[{"Dials":40,"Conversations": 20, "Interest":60, "month":"Jan"},
  // {"Dials":80,"Conversations": 20, "Interest":30,"month":"Feb"},
  // {"Dials":10,"Conversations": 60, "Interest":100,"month":"Mar"}]

  months = [
    { month: 'Jan', value: 0, "Dials": 40, "Conversations": 20, "Interest": 60, },
    { month: 'Feb', value: 1, "Dials": 50, "Conversations": 90, "Interest": 90, },
    { month: 'Mar', value: 2, "Dials": 60, "Conversations": 70, "Interest": 40, },
    { month: 'Aprl', value: 3, "Dials": 70, "Conversations": 50, "Interest": 60, },
    { month: 'May', value: 4, "Dials": 80, "Conversations": 30, "Interest": 70, },
    { month: 'June', value: 5, "Dials": 40, "Conversations": 60, "Interest": 40, },
    { month: 'July', value: 6, "Dials": 90, "Conversations": 20, "Interest": 20, },
  ]
  monthss = [
    { months: 'Jan', values: 0, "Coversation": 40, "positive": 30, "Interest": 60, },
    { months: 'Feb', values: 1, "Coversation": 50, "positive": 40, "Interest": 90, },
    { months: 'Mar', values: 2, "Coversation": 60, "positive": 70, "Interest": 40, },
    { months: 'Aprl', values: 3, "Coversation": 70, "positive": 80, "Interest": 60, },
    { months: 'May', values: 4, "Coversation": 80, "positive": 50, "Interest": 70, },
    { months: 'June', values: 5, "Coversation": 40, "positive": 60, "Interest": 40, },
    { months: 'July', values: 6, "Coversation": 90, "positive": 20, "Interest": 20, },
  ]

  range = {
    from: 0,
    to: 0
  }

  ranges = {
    from: 0,
    to: 0
  }


  currentMonth = [50, 20, 30]

  DoughnutChart() {
    debugger;
    this.requestedData = this.monthss.filter(
      d => d.months == "Jan")[0];
    this.currentMonth = [this.requestedData.Coversation, this.requestedData.positive, this.requestedData.Interest]

    // let checs = this.monthss.map(({ Coversation, positive, Interest }) => {
    //   return {Coversation, positive, Interest };
    // })[0]

    this.canvas = document.getElementById('myCharts');
    this.cxt = this.canvas.getContext('2d');
    this.myCharts = new Chart(this.cxt,
      {
        type: 'doughnut',
        data: {
          labels: ['Coversation', 'positive', 'Interest'],
          datasets: [{
            label: '# of Votes',
            data: this.currentMonth,
            backgroundColor: [
              'rgba(255,99,132,1)',
              'rgba(54,162,235,1)',
              'rgba(255,206,86,1)'
            ],
            borderWidth: 1

          }]
        },
        options: {
          responsive: false,
          //display: true
        }

      });
  }
  applyFilterPie(value) {
    debugger;
    switch (value) {
      case "Jan":
        this.requestedData = this.months.filter(
          d => d.month.search(value))[0];
        this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]

        this.pieChart.data.datasets[0].data = this.currentMonth
        this.pieChart.update()

        break;

      case "Feb":

        this.requestedData = this.months.filter(
          d => d.month.search(value))[0];
        this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]

        this.pieChart.data.datasets[0].data = this.currentMonth
        this.pieChart.update()

        break;

      case "Mar":

        this.requestedData = this.months.filter(
          d => d.month.search(value))[0];
        this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]

        this.pieChart.data.datasets[0].data = this.currentMonth
        this.pieChart.update()


        break;
    }
  }

  applyFilterDoughnut(value) {
    debugger;
    switch (value) {
      case "Jan":
        this.requestedData = this.months.filter(
          d => d.month.search(value))[0];
        this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]


        // for doughnut
        this.myCharts.data.datasets[0].data = this.currentMonth
        this.myCharts.update()

        break;

      case "Feb":

        this.requestedData = this.months.filter(
          d => d.month.search(value))[0];
        this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]


        // for doughnut
        this.myCharts.data.datasets[0].data = this.currentMonth
        this.myCharts.update()
        break;

      case "Mar":

        this.requestedData = this.months.filter(
          d => d.month.search(value))[0];
        this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]


        // for doughnut
        this.myCharts.data.datasets[0].data = this.currentMonth
        this.myCharts.update()

        break;
    }
  }



  monthsFilter(values) {
    debugger;


    if (values.from > values.to) {
      alert("wrong selection")

      return
    }


    let monthssUpdated = this.monthss.filter(
      d => d.values >= values.from && d.values <= values.to);



    var conversationsTotal = monthssUpdated.reduce(function (prev, cur) {
      return prev + cur.Coversation;
    }, 0);

    var positiveTotal = monthssUpdated.reduce(function (prev, cur) {
      return prev + cur.positive;
    }, 0);

    var interestTotal = monthssUpdated.reduce(function (prev, cur) {
      return prev + cur.Interest;
    }, 0);

    this.currentMonth = [conversationsTotal, positiveTotal, interestTotal]

    this.myCharts.data.datasets[0].data = this.currentMonth
    this.myCharts.update()

  }


  monthFilter(value) {
    debugger;


    if (value.from > value.to) {
      alert("wrong selection")

      return
    }


    let monthsUpdated = this.months.filter(
      d => d.value >= value.from && d.value <= value.to);



    var dialsTotal = monthsUpdated.reduce(function (prev, cur) {
      return prev + cur.Dials;
    }, 0);

    var conversationsTotal = monthsUpdated.reduce(function (prev, cur) {
      return prev + cur.Conversations;
    }, 0);

    var interestTotal = monthsUpdated.reduce(function (prev, cur) {
      return prev + cur.Interest;
    }, 0);

    this.currentMonth = [dialsTotal, conversationsTotal, interestTotal]

    this.pieChart.data.datasets[0].data = this.currentMonth
    this.pieChart.update()

  }

  PieChart() {
    debugger;

    this.requestedData = this.months.filter(
      d => d.month == "Jan")[0];
    this.currentMonth = [this.requestedData.Dials, this.requestedData.Conversations, this.requestedData.Interest]

    let chec = this.months.map(({ Dials, Conversations, Interest }) => {
      return { Dials, Conversations, Interest };
    })[0]

    this.canvasPie = document.getElementById('myChartsPie');
    this.cxtPie = this.canvasPie.getContext('2d');
    this.pieChart = new Chart(this.cxtPie, {
      type: 'pie',
      data: {
        labels: ['Coversation', 'positive', 'Interest'],
        datasets: [{
          label: '# of Votes',
          data: this.currentMonth,
          backgroundColor: [
            'rgba(255,99,132,1)',
            'rgba(54,162,235,1)',
            'rgba(255,206,86,1)'
          ],
          borderWidth: 1

        }]
      },
      options: {
        responsive: false,
        //display : true
      }

    });
  }

  DoubleBarChart() {
    this.canvasDb = document.getElementById('myDoubleBarChart');
    this.cxtDb = this.canvasDb.getContext('2d');
    let DoubleBarChart = new Chart(this.cxtDb, {
      type: 'bar',
      data: {

        datasets: [{
          label: 'Total Contacts',
          data: [50, 50, 50, 50],
          backgroundColor: [
            'rgba(54,162,235,1)',
            'rgba(54,162,235,1)',
            'rgba(54,162,235,1)',
            'rgba(54,162,235,1)'

          ],
          borderWidth: 1
        }, {
          label: 'Dialed Contacts',
          data: [10, 20, 30, 40],
          backgroundColor: [
            'rgba(255,206,86,1)',
            'rgba(255,206,86,1)',
            'rgba(255,206,86,1)',
            'rgba(255,206,86,1)'

          ],
          type: 'bar'
        }],

        labels: ['January', 'February', 'March', 'April']
      },

      options: {
        responsive: false,
        //display : true
      }

    });
  }
  BarChart() {
    this.canvasB = document.getElementById('myBarChart');
    this.cxtB = this.canvasB.getContext('2d');
    let BarChart = new Chart(this.cxtB, {
      type: 'bar',
      data: {
        labels: ['Apple', 'Banana', 'Kiwifruit', 'Blueberry', 'Orange', 'Grapes'],
        datasets: [{
          label: 'Best Fruits',
          data: [45, 37, 60, 70, 46, 33],
          backgroundColor: [
            'rgba(255,99,132,1)',
            'rgba(54,162,235,1)',
            'rgba(255,206,86,1)',
            'rgba(255,99,132,1)',
            'rgba(54,162,235,1)',
            'rgba(255,206,86,1)'
          ],
          borderWidth: 1

        }]
      },
      options: {
        responsive: false,
        // display : true
      }

    });

  }

  ngOnInit() {
    debugger;
    this.DoughnutChart();
    this.PieChart();
    this.DoubleBarChart();
    this.BarChart();
    this.userName = localStorage.userName;
    this.service.getUserDetails().subscribe(res => {
      console.log(res);
      if (res['success']) {
        this.userDetails = res['user'];
      }
    }, err => {
      console.log(err);
      this.service.ErrorSuccess(err['message']);
    })
  }

}

