import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SettingComponent } from '../setting/setting.component';
import { HttpClient } from '@angular/common/http';
import { SalesforceService } from '.././../shared/services/salesforce.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AgentService } from '.././../shared/services/agent/agent.service';


@Component({
  selector: 'app-greeting-dialog',
  templateUrl: './greeting-dialog.component.html',
  styleUrls: ['./greeting-dialog.component.scss'],

})
export class GreetingDialogComponent implements OnInit {

  NameData: FormGroup;
  message: any;
  name: any;
  updateVoiceFile = false;


  constructor(private fb: FormBuilder,
    public dialogRef2: MatDialogRef<SettingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, public service: SalesforceService, public service1: AgentService) { }

  ngOnInit(): void {
    if (this.data == {} || !this.data) {
      debugger;
      this.updateVoiceFile = false;
      this.NameData = this.fb.group({
        name: [''],
        audio: [''],

      });
    }
    else {

      this.updateVoiceFile = true;
      this.NameData = this.fb.group({

        name: [this.data.voiceMessages.name],
        audio: [''],

      });
    }



  }

  cancel() {
    this.dialogRef2.close();
  }


  onfileSelected(event) {
    // this.selectedFile = <File>event.target.files[0];

    // if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.NameData.get('audio').setValue(file);
    console.log(event);
    // }
  }





  onupload(post) {
    debugger;

    if (this.updateVoiceFile) {

      if (!this.NameData.get('audio').value) {
        post._id = this.data.voiceMessages._id;
        post.voiceMsgUrl = this.data.voiceMessages.voiceMsgUrl;
        post.name = post.name;
        post.types = '2';

      }
      else {
        const formData = new FormData();
        formData.append('name', this.NameData.get('name').value);
        formData.append('types', "2");
        formData.append('audio', this.NameData.get('audio').value);
        formData.append('voiceMsgUrl', this.data.voiceMessages.voiceMsgUrl)
        formData.append('_id', this.data.voiceMessages._id)
        post = formData;

      }
      this.dialogRef2.close(post);
    }
    else {

      debugger;
      const formData = new FormData();
      formData.append('name', this.NameData.get('name').value);
      formData.append('types', "2");
      formData.append('audio', this.NameData.get('audio').value);
      debugger
      this.dialogRef2.close(formData);
    }


  }
}
