import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesforceRoutingModule } from './salesforce-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { DialerSessionComponent } from './dialer-session/dialer-session.component';
import { MyListComponent, AddListFormComponent, ImportExcelComponent, ScriptComponent, TransferOwnershipComponent, UploadComponent } from './my-list/my-list.component';
import { ReportsComponent } from './reports/reports.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { HomeComponent } from './home/home.component';
import { SalesforceComponent } from './components/salesforce.component';
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import { SettingComponent } from './setting/setting.component';
import { GreetingDialogComponent } from './greeting-dialog/greeting-dialog.component';
import { FormsModule }   from '@angular/forms';
import {MaterialModule} from '../material/material';
import {MatTabsModule} from '@angular/material/tabs';




import { SFComponent } from './sf/sf.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from '../../environments/environment';
const config: SocketIoConfig = { url: environment.URL, options: {
  query: { Authorization: localStorage.token }
  } };


@NgModule({
  declarations: [DialerSessionComponent, MyListComponent, ReportsComponent, SchedulerComponent, HomeComponent, SettingComponent,
 
    SalesforceComponent,
   // SalesforceHeaderComponent,
    HomeComponent,
    DialerSessionComponent,
    SchedulerComponent,
    MyListComponent,
    AddListFormComponent,
     MyDialogComponent,
    SettingComponent,
    MyDialogComponent,
    GreetingDialogComponent,
    ReportsComponent,
    SettingComponent,
    ImportExcelComponent,
    ScriptComponent,
  //  RecieveCallDialogComponent,
    TransferOwnershipComponent,
    UploadComponent,
    SFComponent],
  imports: [
    CommonModule,
    SharedModule,
    SalesforceRoutingModule,
    FormsModule,
    MaterialModule,
    SocketIoModule.forRoot(config)

  ],
 // providers: [DayService, WeekService, WorkWeekService, MonthService, MonthAgendaService ] , 
  entryComponents:[
    ImportExcelComponent,
    AddListFormComponent,
    ScriptComponent,
   // RecieveCallDialogComponent,
    TransferOwnershipComponent,
    UploadComponent
  ]

})
export class SalesforceModule { }
