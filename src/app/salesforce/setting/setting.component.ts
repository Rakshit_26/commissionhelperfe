import { Component, OnInit } from '@angular/core';
import { SalesforceService } from '../../shared/services/salesforce.service';
import { MatTableDataSource } from '@angular/material/table';
import { PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MyDialogComponent } from '../my-dialog/my-dialog.component';
import { GreetingDialogComponent } from '../greeting-dialog/greeting-dialog.component';
import { AgentService } from '../../shared/services/agent/agent.service';
import { DialerService } from '../../shared/services/dialer.service';
import { RoleService } from '../../shared/services/role/role.service';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  GreetingSource: MatTableDataSource<any>;
  greeting = '';
  voice = '';
  VoiceList;
  greetingMsgList;
  defaultGreetingId: any = '';
  defaultVmId: any = '';
  greetingList;
  ReportoneTable;
  ReporttwoTable: any = [];
  data;
  reportarray = [];
  ColumnArray = [];
  Setting: any = {};
  SettingData: any = {};
  CountryCodePrefixed;
  voiceMessageList: any = [];
  public reportvalue: string = '';



  _a: any;

  displayedColumns = ['name', 'voiceMsgUrl', 'userId', 'edit', 'delete'];

  selectIndex = 0;
  totalCount = 0;
  pageSize = 10;
  pageEvent: PageEvent;
  pageIndex: number = 0;
  public voiceMsgUrl: any;




  pageLimit: number[] = [5, 10];
  initialPage = {
    "length": 10,
    "pageIndex": 0,
    "pageSize": 10,
    "previousPageIndex": 0


  };

  selection = new SelectionModel<any>(true, []);
  userDetails: any;

  constructor(private salesforceService: SalesforceService, private service: DialerService, public dialog: MatDialog, private agentservice: AgentService, private roleservice: RoleService) { }


  openDialog(): void {

    const dialogRef = this.dialog.open(MyDialogComponent, {

      width: '650px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result === undefined || result === {} || result === '') {

        return;
      }
      this.salesforceService.uploadFile(result).subscribe((res) => {
        debugger;
        console.log(res);
        if (res['success']) {
          debugger;
          this.service.showToaster('File Uploaded Successfully');
          this.GetAllVoice();
        }
      })
    });

  }


  openDialog1() {

    const dialogRef1 = this.dialog.open(GreetingDialogComponent, {
      width: '650px',
      disableClose: true

    });

    dialogRef1.afterClosed().subscribe(result => {

      if (result === undefined || result === {} || result === '') {

        return;
      }
      this.salesforceService.uploadFile(result).subscribe((res) => {
        debugger;
        console.log(res);
        if (res['success']) {
          debugger;
          this.service.showToaster('File Uploaded Successfully');
          this.GetAllGreetings();

        }

      })
    });

  }

  ngOnInit(): void {
    debugger
    this.GetAllVoice();
    this.GetAllGreetings();
    this.GetAllIsDefaulttpe1();
    this.GetAllIsDefaulttpe2();
    this.gettableOneDataReport();
    this.gettabletwoDataReport();
    setTimeout(() => {
      this.matchData();

    }, 2000);
    this.getsetting();


  }


  getRole() {
    this.roleservice.getRole();

  }

  checkrole() {
    this.roleservice.checkRoutes();
  }

  get isAdmin() {

    return localStorage && localStorage.role === 'admin';
  }

  get isManager() {

    return localStorage && localStorage.role === 'manager';
  }



  GetAllVoice() {

    debugger
    this.salesforceService.getallVoiceMessage().subscribe(res => {

      console.log(res);
      this.voiceMessageList = res['voiceMessages'];
      this.dataSource = new MatTableDataSource(res['voiceMessages']);

      this.totalCount = res['voiceMessages']['totalSize'];

    }, err => {

    })
  }



  GetAllGreetings() {
    debugger

    this.salesforceService.getallGreetings().subscribe(res => {

      console.log(res);
      this.greetingMsgList = res['voiceMessages'];
      this.GreetingSource = new MatTableDataSource(res['voiceMessages']);
      this.totalCount = res['voiceMessages']['totalSize'];

    }, err => {

    })
  }



  GetAllIsDefaulttpe1() {
    debugger


    this.salesforceService.getallIsdefaulttype1().subscribe(res => {

      console.log(res);
      this.defaultVmId = res['voiceMessages']['_id'];


    }, err => {

    })
  }

  defaultVoiceMsg(event) {

    debugger;
    let data = {

      _id: event.value

    }
    this.salesforceService.setDefaultVoiceGreetMsg(data).subscribe(res => {

      this.service.showToaster("Default Voice Message Set Successfully");


    }, err => {

    })


  }
  defaultGreetMsg(event) {

    debugger;
    let data = {
      _id: event.value
    }
    this.salesforceService.setDefaultVoiceGreetMsg(data).subscribe(res => {

      this.service.showToaster("Default Greeting Message Set Successfully");


    }, err => {

    })


  }

  GetAllIsDefaulttpe2() {
    debugger


    this.salesforceService.getallIsdefaulttype2().subscribe(res => {

      console.log(res);
      this.defaultGreetingId = res['voiceMessages']['_id'];


    }, err => {

    })
  }


  editGreetingeMessage(voiceMessages) {
    debugger;
    const dialogRef2 = this.dialog.open(GreetingDialogComponent, {
      width: '650px',
      disableClose: true,
      data: { voiceMessages },

    });
    dialogRef2.afterClosed().subscribe(result => {
      debugger;
      if (result === undefined || result === {} || result === '') {
        return;
      }
      this.salesforceService.editVoiceMessage(result).subscribe(res => {
        if (res['success']) {
          debugger;

          this.service.showToaster('File Updated Successfully');
          this.GetAllGreetings();

        }
      }, err => {
        console.log(err);

        this.service.ErrorSuccess('File Can not be updated!');
      })
    });

  }

  editMessage(voiceMessages) {
    debugger
    const dialogRef1 = this.dialog.open(MyDialogComponent, {
      width: '650px',
      disableClose: true,
      data: { voiceMessages }
    });

    dialogRef1.afterClosed().subscribe(result => {
      debugger;
      if (result === undefined || result === {} || result === '') {
        return;
      }
      this.salesforceService.editVoiceMessage(result).subscribe(res => {
        if (res['success']) {
          debugger;

          this.service.showToaster('File Updated Successfully');
          this.GetAllVoice();

        }
      }, err => {
        console.log(err);

        this.service.ErrorSuccess('File Can not be updated!');
      })
    });

  }



  gettableOneDataReport() {
    this.salesforceService.TableoneReportSetting().subscribe(res => {
      console.log(res);
      this.ReportoneTable = res['reportDetails'];

    }, err => {

    })
  }

  matchData() {
    debugger;
    this.ReportoneTable = this.ReportoneTable.filter(item => !this.ReporttwoTable.includes(item));
  }


  gettabletwoDataReport() {
    this.salesforceService.TabletwoReportSetting().subscribe(res => {
      console.log(res);
      this.ReporttwoTable = res['userColumnReport']['ColumnArray'];
    }, err => {

    })

  }


  selectreport(report) {
    debugger
    this.reportvalue = report;
    this._a = report.$index == true ? this.ReportoneTable.push(report) : this.ReportoneTable.splice(0, 1);
    this.ReporttwoTable.push(this._a[0]);
    this._a.splice(report, 1);
    console.log(this.ReporttwoTable);
  }

  deselectreport(report1) {
    debugger
    this.ReportoneTable.push(...this.ReporttwoTable.splice(this.ReporttwoTable.indexOf(report1), 1));
  }

  postreportData() {
    debugger;
    var data = this.ReporttwoTable;
    this.salesforceService.postReportSetting({ ColumnArray: data }).subscribe(res => {
      debugger;
      this.ColumnArray = this.ReporttwoTable;

      if (res['success']) {
        debugger;
        this.service.showToaster('Settings Saved Successfully');

      }

    })
  }

  postSetting() {
    debugger;

    this.salesforceService.postSetting({ Setting: this.SettingData }).subscribe(res => {
      this.Setting = this.SettingData;
      if (res['success']) {
        debugger;
        this.service.showToaster('Settings Saved Successfully');

      }


    }, err => {
      console.log(err);
      this.service.ErrorSuccess('Setting Not Saved!');
    }


    )
  }




  getsetting() {

    debugger;
    this.salesforceService.getSetting().subscribe(res => {

      console.log(res);
      this.SettingData = res['Setting'];


    }, err => {
      console.log(err);
      this.service.ErrorSuccess('Setting Not Saved!');

    })

  }

  deleteVoice(_id) {
    debugger;
    this.salesforceService.deleteVoiceMessage(_id).subscribe(res => {
      if (res['success']) {
        debugger;
        this.agentservice.showToaster('File Deleted Successfully');
        this.ngOnInit()

      }
    }, err => {
      console.log(err);
      this.agentservice.ErrorSuccess('File Cannot Be deleted!');
    })
  }
  deletefile(_id) {
    debugger;
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '350px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.deleteVoice(_id);
      }
    });

  }


  
}
