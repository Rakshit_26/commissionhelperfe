import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DialerService } from 'app/shared/services/dialer/dialer.service';
import { RoleService } from 'app/shared/services/role/role.service';
import { SocketService } from 'app/shared/services/socket/socket.service';

@Component({
  selector: 'app-salesforce',
  templateUrl: './salesforce.component.html',
  styleUrls: ['./salesforce.component.css'],
  providers: [DialerService, RoleService, SocketService]
})
export class SalesforceComponent implements OnInit {
  URL = '';
  accessToken: any;
  _docSub: Subscription;
  contactData: any;
  constructor(
    private router: Router,
    private dialerService: DialerService,
    private roleService: RoleService,
    private socketservice: SocketService
    ) {}

  ngOnInit(): void {
    debugger;
    this.URL = this.router.url;
    if(!this.URL){
        this.router.navigate(['/notfound']);
      }
    this.accessToken = this.URL.split('/');
    this.accessToken = this.accessToken[2];
    this.dialerService.verifytoken(this.accessToken).subscribe(res=>{
        if(res['success']){
          debugger;
          localStorage.setItem('token', this.accessToken);
          localStorage.setItem('oauthToken', res['User']['oauthToken']);
          localStorage.setItem('refreshToken', res['User']['refreshToken']);
          localStorage.setItem('role', res['User']['Role']);
          localStorage.setItem('userName', res['User']['username']);
          localStorage.setItem('userid', res['User']['_id']);
          this._docSub = this.socketservice.connect.subscribe((doc) =>{
            debugger;
            console.log(doc)
          }
          );
          this.socketservice.getForwardedCall.subscribe(res=> {
            debugger;
            this.contactData = res['Contact'];
            alert(`Call Forwarded!  Contact No.: +${res['Contact']['ContactList']['Phone']}`);
          });
          this.socketservice.createConnection();
          this.router.navigate([this.roleService.checkRoutes()]);
        } else{
          this.router.navigate(['/notfound']);
        }
      }, err=>{
        this.router.navigate(['/notfound']);
      })
    }
  //   setting(){     
  //  this.router.navigate(['/Setting'])
  //   }
    
}
