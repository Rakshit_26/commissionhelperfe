import { Component, OnInit } from '@angular/core';
import { SalesforceService } from '../../shared/services/salesforce.service';
import { DialerService } from '../../shared/services/dialer.service';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit {
  eventdata: any =[];

  constructor(private salesforceService: SalesforceService, private service: DialerService, ) { }
 public Scheduledata: any = { dataSource: this.eventdata};
  ngOnInit(): void {
    debugger

    this.GetSchedulerdata();
    setTimeout(() => {
      this.Scheduledata.dataSource = this.eventdata;
    }, 5000);
    

  }

  public selectedDate: Date = new Date(2018, 1, 15);
 
//   private dataManager: DataManager = new DataManager({
//     url: 'https://dailerloginbackend.herokuapp.com/v0.1/scheduler/getsalesRepTime',
//     adaptor: new ODataV4Adaptor,
//     crossDomain: true
//  });

  public onActionBegin(schedulerdata) {
    debugger;

    let data = {
      "StartTime": schedulerdata.addedRecords[0]["StartTime"],
      "EndTime": schedulerdata.addedRecords[0]["EndTime"],
      "Subject": schedulerdata.addedRecords[0]["Subject"],
      "Description": schedulerdata.addedRecords[0]["Description"],
      "Id":schedulerdata.addedRecords[0]["Id"]
    };


    this.salesforceService.schedulerPost(data).subscribe(res => {
      debugger

      console.log(this.Status);
      debugger
      this.service.showToaster("Scheduled Successfully");


    }, err => {

    })
  }

  GetSchedulerdata() {
    debugger;
    this.salesforceService.schedulerGet().subscribe(res => {
      debugger
    // console.log(res);
    this.eventdata = res['message'];
   this.Scheduledata.dataSource=this.eventdata;
   debugger
   console.log(this.Scheduledata.dataSource)
  
    return this.eventdata;
    }, err => {

    })
  }

  Status(Status: any) {
    throw new Error("Method not implemented.");
  }

}
