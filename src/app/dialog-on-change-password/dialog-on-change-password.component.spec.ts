import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogOnChangePasswordComponent } from './dialog-on-change-password.component';

describe('DialogOnChangePasswordComponent', () => {
  let component: DialogOnChangePasswordComponent;
  let fixture: ComponentFixture<DialogOnChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogOnChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogOnChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
