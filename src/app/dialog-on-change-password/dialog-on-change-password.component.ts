import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog-on-change-password',
  templateUrl: './dialog-on-change-password.component.html',
  styleUrls: ['./dialog-on-change-password.component.css']
})
export class DialogOnChangePasswordComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<DialogOnChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  login(){
    this.router.navigate(['/login']);
    this.dialogRef.close();
  }

}
