
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog,} from '@angular/material/dialog';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-comission-team',
  templateUrl: './comission-team.component.html',
  styleUrls: ['./comission-team.component.scss']
})
export class ComissionTeamComponent implements OnInit {
  
  dataSource: any;

  userData = [];
  gridView = [];
  message: any;
  TimeZoneListDetails = [];
  opened: boolean = false;
  public IsUpdateAgent: boolean = false;
  
  public deleteDialogState: boolean = false;
  public _idToBeDeleted: any = null;
  public isFilterOn: boolean = false;
  
  checkBoxUserSelectedRow: any;
  public mySelectionUser: any[] = [];
  keyChangeUser(e) {
  console.log('Selected items:', e);
  this.checkBoxUserSelectedRow = e[0];
  console.log('id of selected checkbox' + this.checkBoxUserSelectedRow);
  }
    constructor(private router: Router, private service: ComissionHelperService,  public dialog: MatDialog, private formBuilder: FormBuilder) { }
   
    ngOnInit(): void {
      this.getAllTeam();
    }
   
    getAllTeam(){
      this.service.getAllTeam().subscribe(res=>{
        this.userData=res['allTeam']
        console.log(this.userData)
         console.log(res);
    

        });
      }
 
 

      goTo(id){
        this.router.navigate(['/layout/pipeline-admin/comission-team-edit'],{
          queryParams: {data: btoa(JSON.stringify( id))}});
   
      }




userpopup(){
  debugger;
  this.router.navigate(['/layout/pipeline-admin/comission-team-add'])
  }
  delete(_id) {
    debugger;
    this.service.delete(_id).subscribe(res => {
    if (res['success']) {
    debugger;
    this.service.showToaster('User has been deleted');
    this.ngOnInit()
    
    }
    }, err => {
    console.log(err);
    this.service.ErrorSuccess('User Can not be deleted!');
    }
    )
    }
    
  }