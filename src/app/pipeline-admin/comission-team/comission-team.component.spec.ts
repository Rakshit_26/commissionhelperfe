import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionTeamComponent } from './comission-team.component';

describe('ComissionTeamComponent', () => {
  let component: ComissionTeamComponent;
  let fixture: ComponentFixture<ComissionTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
