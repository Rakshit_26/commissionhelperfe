import { Component, OnInit } from '@angular/core';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-commission-product-group',
  templateUrl: './commission-product-group.component.html',
  styleUrls: ['./commission-product-group.component.scss']
})
export class CommissionProductGroupComponent implements OnInit {
  Data =[
    {
      "Name":" xyz",
    }
  ]
  productData:any =[];
  userData = [];
  gridView = [];
  message: any;
  TimeZoneListDetails = [];
  opened: boolean = false;
  public IsUpdateAgent: boolean = false;

  public deleteDialogState: boolean = false;
  public _idToBeDeleted: any = null;
  public isFilterOn: boolean = false;
  
  checkBoxUserSelectedRow: any;
  public mySelectionUser: any[] = [];
  

  keyChangeUser(e) {
  console.log('Selected items:', e);
  this.checkBoxUserSelectedRow = e[0];
  console.log('id of selected checkbox' + this.checkBoxUserSelectedRow);
  }

  dialogAction: string = '';
  dialogAction1: boolean = true;// true-add false-update
  constructor(private service: ComissionHelperService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllProductGroup()
  }
  userpopup(){
    debugger;
    this.router.navigate(['./layout/pipeline-admin/commission-product-group-add'])
    }
    delete(_id) {
      
  }
  deleteItem(_id) {
      this._idToBeDeleted = _id;
      this.deleteDialogState = true;
  }
    closeDeleteDialog(state) {
      this.deleteDialogState = false;
      if (state == 'no') {
          } else if (state == 'yes' && this._idToBeDeleted != null) {
                this.delete(this._idToBeDeleted);
          } else {
      }
  }
  activateHoliday() {
    
  }
  getAllProductGroup() {

    debugger;
    this.service.getAllProductGroup().subscribe((res) => {
      debugger;
      
this.productData=(res['allProducts']);

    }, (err) => {

   
      debugger;
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }
  edit(id) {
    debugger;
   
    console.log(id);
    
    this.router.navigate(['./layout/pipeline-admin/commission-product-group-edit'], {
    queryParams: {data: btoa(JSON.stringify( id))}})
     }
}
