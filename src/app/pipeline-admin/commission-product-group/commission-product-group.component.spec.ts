import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionProductGroupComponent } from './commission-product-group.component';

describe('CommissionProductGroupComponent', () => {
  let component: CommissionProductGroupComponent;
  let fixture: ComponentFixture<CommissionProductGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionProductGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionProductGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
