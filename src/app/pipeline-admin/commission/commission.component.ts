//import { ProductcommissionService } from './../../shared/services/productcommission.service';
//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommissionService } from 'app/shared/services/pipelineadmin/commission.service';

const now = new Date();
@Component({
  selector: 'app-commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.scss']
})
export class CommissionComponent implements OnInit {
  message: any;
  dataSource: any;
  userData = [];
  gridView = [];
  
  TimeZoneListDetails = [];
  opened: boolean = false;
  public IsUpdateAgent: boolean = false;

  public deleteDialogState: boolean = false;
  public _idToBeDeleted: any = null;
  public isFilterOn: boolean = false;
  
  checkBoxUserSelectedRow: any;
  public mySelectionUser: any[] = [];
  

  keyChangeUser(e) {
  console.log('Selected items:', e);
  this.checkBoxUserSelectedRow = e[0];
  console.log('id of selected checkbox' + this.checkBoxUserSelectedRow);
  }

  dialogAction: string = '';
  dialogAction1: boolean = true;// true-add false-update
  constructor(public service:CommissionService,private router: Router, private route : ActivatedRoute) {
  }

  ngOnInit(): void {
    debugger;
    this.getAllCommission();
  }
  
  getAllCommission() {
    debugger;
    this.service.getAllCommission().subscribe((res) => {
    debugger;
    this.dataSource = res['allCommission'];
    
   
    },
    (err) => {
    this.message = err.error['errorMessage'];
    this.service.ErrorSuccess(this.message);
    });
    }

  /*getAllCommission() {

    debugger;
    this.service.getAllCommission().subscribe((res) => {
      debugger;
      
this.DataSource=(res['allCommission']);

      this.dataSource = new MatTableDataSource(res['allCommission']);
      this.totalCount = res['allCommission'].length;
      this.dataSource.paginator = this.paginator;
   
      this.dataSource.sort = this.sort;



    }, (err) => {

      // console.log(err);
      debugger;
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }*/
  edit(id) {
    debugger;
   
    console.log(id);
    
    this.router.navigate(['./layout/pipeline-admin/product-details'], {
    queryParams: {data: btoa(JSON.stringify( id))}})
     }
     delete(_id) {
      debugger;
      this.service.delete(_id).subscribe(res => {
      if (res['success']) {
      debugger;
      this.service.showToaster('Product has been deleted');
      this.ngOnInit()
      
      }
      }, err => {
      console.log(err);
      this.service.ErrorSuccess('Product Can not be deleted!');
      }
      )
      }



  userpopup(){
    debugger;
    this.router.navigate(['./layout/pipeline-admin/commission-add'])
    }
}
