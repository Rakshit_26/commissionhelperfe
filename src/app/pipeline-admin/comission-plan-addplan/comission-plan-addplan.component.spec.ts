import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionPlanAddplanComponent } from './comission-plan-addplan.component';

describe('ComissionPlanAddplanComponent', () => {
  let component: ComissionPlanAddplanComponent;
  let fixture: ComponentFixture<ComissionPlanAddplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionPlanAddplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionPlanAddplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
