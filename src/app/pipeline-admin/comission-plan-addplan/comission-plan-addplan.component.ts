import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

@Component({
  selector: 'app-comission-plan-addplan',
  templateUrl: './comission-plan-addplan.component.html',
  styleUrls: ['./comission-plan-addplan.component.scss']
})
export class ComissionPlanAddplanComponent implements OnInit {
 
  formGroup: FormGroup = new FormGroup({
    planName: new FormControl(''),
    commissionAmount: new FormControl(''),
     
});
dialogAction: string = '';
dialogAction1: boolean = true;// true-add false-update
submitted = false;
  constructor(private formBuilder: FormBuilder,private service: ComissionHelperService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      planName: new FormControl(''),
      commissionAmount: new FormControl(''),
  
  });
    
  }
  cancel() {
    this.submitted = false;
    this.router.navigate(['/layout/pipeline-admin/comission-plan'])

}
onSubmit(post) {
  console.log(post);
  debugger;



let PrimaryUser = {
   planName: post.planName,
commissionAmount: post.commissionAmount,

};

debugger;
this.submitted = true;

if (this.dialogAction1) {
  console.log(PrimaryUser,);

  this.service.addplan(PrimaryUser) .subscribe(res => {
    this.router.navigate(['./layout/pipeline-admin/comission-plan'],);
      if (res['success']) {
          this.service.showToaster('Plan registered sucessfully');
         
      }
  }, (err) => {
      console.log(err);
      this.service.showToaster('Plan can not be sucessfully');
  }
  );
} 
}
}
