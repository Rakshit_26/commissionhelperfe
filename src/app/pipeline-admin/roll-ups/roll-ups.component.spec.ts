import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RollUpsComponent } from './roll-ups.component';

describe('RollUpsComponent', () => {
  let component: RollUpsComponent;
  let fixture: ComponentFixture<RollUpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RollUpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RollUpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
