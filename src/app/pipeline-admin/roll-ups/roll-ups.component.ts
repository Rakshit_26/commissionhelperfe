import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-roll-ups',
  templateUrl: './roll-ups.component.html',
  styleUrls: ['./roll-ups.component.css']
})
export class RollUpsComponent implements OnInit {

  constructor() { }

  btnshow1 = 'show';
  btnshow2 = 'hide';
  toggle1(): void {
    if (this.btnshow1 === 'hide')
    { this.btnshow1 = 'show'; }
    else {
      this.btnshow1 = 'hide';
    }
  }
  toggle2(): void {
    if (this.btnshow2 === 'hide')
    { this.btnshow2 = 'show'; }
    else {
      this.btnshow2 = 'hide';
    }
  }
  
  ngOnInit(): void {
  }

}
