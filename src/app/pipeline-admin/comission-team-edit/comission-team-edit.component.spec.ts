import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionTeamEditComponent } from './comission-team-edit.component';

describe('ComissionTeamEditComponent', () => {
  let component: ComissionTeamEditComponent;
  let fixture: ComponentFixture<ComissionTeamEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionTeamEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionTeamEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
