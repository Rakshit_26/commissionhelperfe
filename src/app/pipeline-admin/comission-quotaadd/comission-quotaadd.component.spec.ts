import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionQuotaaddComponent } from './comission-quotaadd.component';

describe('ComissionQuotaaddComponent', () => {
  let component: ComissionQuotaaddComponent;
  let fixture: ComponentFixture<ComissionQuotaaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionQuotaaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionQuotaaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
