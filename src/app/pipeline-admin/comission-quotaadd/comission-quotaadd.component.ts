import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup} from '@angular/forms';
//import { QuotaService } from 'app/shared/services/pipelineadmin/quota.service';
import { Router } from '@angular/router';
import { ComissionHelperService } from 'app/shared/services/comission-helper.service'
@Component({
  selector: 'app-comission-quotaadd',
  templateUrl: './comission-quotaadd.component.html',
  styleUrls: ['./comission-quotaadd.component.scss']
})
export class ComissionQuotaaddComponent implements OnInit {
  formGroup:FormGroup; 
  quotaname:string;
  cadence:string;
  dialogAction: string = '';
dialogAction1: boolean = true;// true-add false-update
submitted = false;
    constructor(private service:ComissionHelperService,private fb:FormBuilder,private router:Router) {
      this.formGroup=this.fb.group({
        quotaname:[''],
        cadence:['']
     })
    
      }
  
    ngOnInit(): void {
      
    }
  
  
        cancel()
      {
        this.router.navigate(['/layout/pipeline-admin/comission-quota']); 
         }
         onSubmit(post) {
          console.log(post);
          debugger;
        
        
        
        let PrimaryUser = {
          quotaname: post.quotaname,
          cadence: post.cadence,
        
        };
        
        debugger;
        this.submitted = true;
        
        if (this.dialogAction1) {
          console.log(PrimaryUser,);
        
          this.service.addQuota(PrimaryUser) .subscribe(res => {
            this.router.navigate(['./layout/pipeline-admin/comission-quota'],);
              if (res['success']) {
                  this.service.showToaster('Plan registered sucessfully');
                 
              }
          }, (err) => {
              console.log(err);
              this.service.showToaster('Plan can not be sucessfully');
          }
          );
        } 
        }
  
        }
