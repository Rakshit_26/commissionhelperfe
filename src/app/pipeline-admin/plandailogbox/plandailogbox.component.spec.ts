import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlandailogboxComponent } from './plandailogbox.component';

describe('PlandailogboxComponent', () => {
  let component: PlandailogboxComponent;
  let fixture: ComponentFixture<PlandailogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlandailogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlandailogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
