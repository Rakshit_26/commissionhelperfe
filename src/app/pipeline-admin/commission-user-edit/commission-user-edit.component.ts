import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

@Component({
  selector: 'app-commission-user-edit',
  templateUrl: './commission-user-edit.component.html',
  styleUrls: ['./commission-user-edit.component.scss']
})
export class CommissionUserEditComponent implements OnInit {
  formGroup: FormGroup = new FormGroup({
    Name: new FormControl(''),
    contactNumber: new FormControl(''),
     password: new FormControl(''),
    confirmPassword: new FormControl(''),
username: new FormControl(''),
Company: new FormControl(''),   
});

submitted = false;
userType: any;
Role:any;
choose:any;
  id: any;
  data: any;
  data1: any;
_id:any;
/*setvalue(post)
{console.log(post)}*/
  constructor(private formBuilder: FormBuilder,private service: ComissionHelperService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(parsam => {
      this.id = JSON.parse(atob(parsam.data));
      
      console.log('Lender id', this.id);
      
      debugger;
      this.service.getAllUsers().subscribe(res => {
      debugger;
      console.log(res);
      this.data1 = res['getall'];
      
      for (let i = 0; i < this.data1.length; i++) {
      if (this.id == this.data1[i]._id) {
      console.log('id', this.id, 'data1', this.data1[i]._id)
      this.data = this.data1[i];
      console.log('data1 data', this.data);
      }
      }
      
      this.formGroup = this.formBuilder.group({
        'Name':[this.data.Name],
        'contactNumber':[this.data.contactNumber],
        'password':[this.data.password],
        'confirmPassword':[this.data.confirmPassword],
       
        'Company': [this.data.Company],
        'UserType':[this.data.UserType],
        'Role':[this.data.Role],
        'username':[this.data.username],
        
        });
      });
      });
      }
        
        
  
  cancel() {
    this.submitted = false;
    this.router.navigate(['/layout/pipeline-admin/user'])

}
onSubmit(post) {
  

debugger;   
this.service.edit(post).subscribe(res => {
  console.log(res);
  if (res['success']) {
  debugger;
  this.service.showToaster('User has been updated');
  this.ngOnInit()
  
  }
  }, err => {
  console.log(err);
  this.service.ErrorSuccess('User Can not be updated!');
  });
  }
 
}

