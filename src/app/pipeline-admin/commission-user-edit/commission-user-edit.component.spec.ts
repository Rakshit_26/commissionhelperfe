import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionUserEditComponent } from './commission-user-edit.component';

describe('CommissionUserEditComponent', () => {
  let component: CommissionUserEditComponent;
  let fixture: ComponentFixture<CommissionUserEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionUserEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionUserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
