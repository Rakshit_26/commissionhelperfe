import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanAssignmentsComponent } from './plan-assignments.component';

describe('PlanAssignmentsComponent', () => {
  let component: PlanAssignmentsComponent;
  let fixture: ComponentFixture<PlanAssignmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanAssignmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
