import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan-assignments',
  templateUrl: './plan-assignments.component.html',
  styleUrls: ['./plan-assignments.component.css']
})
export class PlanAssignmentsComponent implements OnInit {

  constructor() { }
 
  btnshow1 = 'show';
  btnshow2 = 'hide';
  toggle1(): void {
    if (this.btnshow1 === 'hide')
    { this.btnshow1 = 'show'; }
    else {
      this.btnshow1 = 'hide';
    }
  }
  toggle2(): void {
    if (this.btnshow2 === 'hide')
    { this.btnshow2 = 'show'; }
    else {
      this.btnshow2 = 'hide';
    }
  }
  
  ngOnInit(): void {
  }

}
