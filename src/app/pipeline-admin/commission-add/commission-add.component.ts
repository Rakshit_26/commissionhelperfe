import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { CommissionService } from 'app/shared/services/pipelineadmin/commission.service';

@Component({
  selector: 'app-commission-add',
  templateUrl: './commission-add.component.html',
  styleUrls: ['./commission-add.component.scss']
})
export class CommissionAddComponent implements OnInit {
  [x: string]: any;
  dialogAction: string = '';
  dialogAction1: boolean = true;// true-add false-update
  ProductDataforEdit: any ;
 product:any;
 public entity: string = 'Commission';
    CommissionDataforEdit: any ;
 commission:any;
  formGroup: FormGroup = new FormGroup({

    commissionType: new FormControl(''),
    assignFor: new FormControl(''),
    assignTo: new FormControl(''),
    product: new FormControl(''),
    percentage: new FormControl(''),
    appliedFrom: new FormControl(''),
    appliedTo: new FormControl(''),




  });


  submitted = false;
  constructor(public service:CommissionService,private route:ActivatedRoute,private router: Router,  private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    
   
    }
      onSubmit(post) {

        debugger
        this.submitted = true;
    
    
    
    console.log(this.dialogAction1);
    if (this.dialogAction1==false) {
    
    
    console.log(post);
    this.service.editcomission(this.commission,post).subscribe(res => {
    console.log(res);
    //this.UserData[this.index]=addUser;
    if (res['success']) {
    this.service.showToaster('Product has been updated');
    
    }
    }, (err) => {
    console.log(err);
    this.service.showToaster('Product can not be updated!');
    }
    );
    }
    else if (this.dialogAction1==true) {
      debugger;
         
        console.log(this.formGroup.value);
        this.service.addcommission(post).subscribe(res =>{
          console.log(res)
          if (res['success']) {
            this.service.showToaster('Product has been add');
            
            }
        },(err) => {
          console.log(err);
          this.service.showToaster('Product can not be add!');
          }
          
        )
        this.router.navigate(['./Productdetails-List'])
    }
      }



  cancel() {
    this.submitted = false;
    this.router.navigate(['./layout/pipeline-admin/commission'])
    }
}
