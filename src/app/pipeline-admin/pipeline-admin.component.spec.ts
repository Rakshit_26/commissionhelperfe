import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipelineAdminComponent } from './pipeline-admin.component';

describe('PipelineAdminComponent', () => {
  let component: PipelineAdminComponent;
  let fixture: ComponentFixture<PipelineAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipelineAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
