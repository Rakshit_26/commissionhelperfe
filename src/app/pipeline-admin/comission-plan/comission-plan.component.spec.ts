import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionPlanComponent } from './comission-plan.component';

describe('ComissionPlanComponent', () => {
  let component: ComissionPlanComponent;
  let fixture: ComponentFixture<ComissionPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
