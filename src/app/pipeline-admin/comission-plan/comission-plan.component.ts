import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComissionHelperService } from 'app/shared/services/comission-helper.service'


@Component({
  selector: 'app-comission-plan',
  templateUrl: './comission-plan.component.html',
  styleUrls: ['./comission-plan.component.scss']
})
export class ComissionPlanComponent implements OnInit {

  dataSource: any;

userData = [];
gridView = [];
message: any;
TimeZoneListDetails = [];
opened: boolean = false;
public IsUpdateAgent: boolean = false;

public deleteDialogState: boolean = false;
public _idToBeDeleted: any = null;
public isFilterOn: boolean = false;

checkBoxUserSelectedRow: any;
public mySelectionUser: any[] = [];
keyChangeUser(e) {
console.log('Selected items:', e);
this.checkBoxUserSelectedRow = e[0];
console.log('id of selected checkbox' + this.checkBoxUserSelectedRow);
}
  constructor(private route: ActivatedRoute, private router: Router,private service:ComissionHelperService) { 
    
  }

   ngOnInit(): void {
    
 this.getAllPlans()
  
}  getAllPlans(){
  this.service.getAllPlans().subscribe(res=>{
    this.userData=res['allplans']
   
     console.log(res);
  });
}
  goTo(id){
    
     this.router.navigate(['/layout/pipeline-admin/comission-planmember'],{
       queryParams: {data: btoa(JSON.stringify( id))}});
    }
   
   delete(_id) {
      debugger;
      this.service.deletePlan(_id).subscribe(res => {
      if (res['success']) {
      debugger;
      this.service.showToaster('Plan has been deleted');
      this.ngOnInit()
      
      }
      }, err => {
      console.log(err);
      this.service.ErrorSuccess('Plan Can not be deleted!');
      }
      )
      }
     planpopup(){
        debugger;
        this.router.navigate(['/layout/pipeline-admin/comission-plan-addplan'])
        }
}
