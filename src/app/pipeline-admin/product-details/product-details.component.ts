// import { Component, OnInit, ViewChild, inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
// import { UserService } from '../../../shared/services/user.service';

import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { stringify } from 'querystring';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  dialogAction: string = '';
  dialogAction1: boolean = true;// true-add false-update
  ProductDataforEdit: any ;
 product:any;
 public entity: string = 'Product';
  formGroup: FormGroup = new FormGroup({

    productName: new FormControl(''),
    productCode: new FormControl(''),
    category: new FormControl(''),
    unitQuantity: new FormControl(''),
    priceBook: new FormControl(''),
    price: new FormControl(''),
    description: new FormControl(''),




  });


  submitted = false;
  allProduct: any;
  id: any;
  constructor( private ToastrService: ToastrService,private service: ComissionHelperService, private route:ActivatedRoute,private router: Router, public dialog: MatDialog, private formBuilder: FormBuilder) {
 
  }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
  ngOnInit(): void {
    this.route.queryParams.subscribe(parsam => {

      this.product = JSON.parse(atob(parsam.data));
      
      console.log('id',this.product);
      
      if (this.product != null) {
      this.service.getAllProduct().subscribe(res =>{
      console.log(res);
      this.allProduct=res['allProducts'];
      console.log(this.allProduct);
      for(let i=0;i<this.allProduct.length;i++)
      {
      if(this.product == this.allProduct[i]._id)
      {
      this.ProductDataforEdit=this.allProduct[i];
      console.log(this.ProductDataforEdit);
      }
      }
      console.log(3);
      if ((this.ProductDataforEdit != undefined) && (Object.keys(this.ProductDataforEdit) && Object.keys(this.ProductDataforEdit).length != 0 && this.ProductDataforEdit.constructor == Object)) {
      this.dialogAction1 = false;
      console.log('inside',this.ProductDataforEdit);
      this.formGroup = this.formBuilder.group({
      'productName': [this.ProductDataforEdit.productName, Validators.required],
      'productCode': [this.ProductDataforEdit.productCode, Validators.required],
      'category': [this.ProductDataforEdit.category, Validators.required],
      'unitQuantity': [this.ProductDataforEdit.unitQuantity, Validators.required],
      'priceBook': [this.ProductDataforEdit.priceBook, Validators.required],
      'price': [this.ProductDataforEdit.price, Validators.required],
      'description': [this.ProductDataforEdit.description, Validators.required],
      
      });
      }
      else {
      debugger;
      this.dialogAction1= true;
      this.formGroup = this.formBuilder.group({
     productName: ['', Validators.required],
     productCode: ['', Validators.required],
     category: ['', Validators.required],
     unitQuantity: ['', Validators.required],
     priceBook: ['', Validators.required],
     price: ['', Validators.required],
     description:['', Validators.required]
      });
      }
      
      });
      
      
      
      
      }
      });
      debugger;
      
      
      
      
      
      }
      
      get f() { return this.formGroup.controls; }
    
  onSubmit(post) {

    debugger
    this.submitted = true;



console.log(this.dialogAction1);
if (this.dialogAction1==false) {


console.log(post);
this.service.editProduct(this.id,post).subscribe(res => {
console.log(res);
//this.UserData[this.index]=addUser;
if (res['success']) {
this.service.showToaster('Product has been updated');

}
}, (err) => {
console.log(err);
this.service.showToaster('Product can not be updated!');
}
);
}
else if (this.dialogAction1==true) {
  debugger;
     
    console.log(this.formGroup.value);
    this.service.addProduct(post).subscribe(res =>{
      console.log(res)
      if (res['success']) {
        this.service.showToaster('Product has been add');
        
        }
    },(err) => {
      console.log(err);
      this.service.showToaster('Product can not be add!');
      }
      
    )
    this.router.navigate(['./Productdetails-List'])
}
  }


  cancel() {
    this.submitted = false;
    this.router.navigate(['./layout/pipeline-admin/productdetails-list'])
    }

  


}
