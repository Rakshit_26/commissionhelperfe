import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComissionHelperService} from'app/shared/services/comission-helper.service';

@Component({
  selector: 'app-comission-quota-quotauser',
  templateUrl: './comission-quota-quotauser.component.html',
  styleUrls: ['./comission-quota-quotauser.component.scss']
})
export class ComissionQuotaQuotauserComponent implements OnInit {
  data: any;
  username1:any;
  formGroup: FormGroup;
  userQuota: any;
  addUser: any;
  Quotaname: any=[];
  Quotaname1: any;
  quotaData: any;
  choose: string;
  cadence: any;
  ids: void;
  
  constructor(private router: Router,private service: ComissionHelperService,private fb: FormBuilder ,  private route: ActivatedRoute,) { }
  
  ngOnInit(): void {
  
  this.formGroup = this.fb.group({
    teamName: ['']
  });
 
  this.route.queryParams.subscribe(parsam => {
    
    this.data= JSON.parse(atob(parsam.data));
    console.log(this.data);
    this.cadence=this.data.cadence
    switch(this.cadence)
    {
    case 'Yearly':this.choose='1';break;
    case 'Quarterly':this.choose='2';break;
    case 'Monthly':this.choose='3';break;
     }
   this.getQuotaById();   
   this.getAllUser(); 
})

}
getQuotaById(){
  this.service.getQuotaById(this.data.id).subscribe(res =>{
    this.userQuota= res['getQuota'];
    console.log('userQuota',res);
   this.ids=this.userQuota.names;
   console.log('name',this.ids)
    this.getAllUser();

});
 }
 loop(){
  console.log('we are in loop',this.userQuota.names);
 let data1=this.userQuota.names;
 let data2=this.addUser
 let count2=data2.length
 console.log('data 1 name',data1,data1.length);
 let count=data1.length
 console.log('data 2 NAME ',data2,count2);
 for(let i=0; i<count; i++)
 {
   console.log('loop 1 is running');
   for(let j=0; j<count2; j++)
   {
   if (data1[i]== data2[j]._id)
   
   {
     console.log('loop 2 is running');
     this.Quotaname.push(this.addUser[j])
   }
   }
  
 }
 console.log('Quotaname',this.Quotaname);
 this.Quotaname1=this.Quotaname
}
getAllUser(){
  this.service.getAllUsers().subscribe(res=>{
    this.addUser=res['getall']
    console.log(this.addUser)
     console.log('addUser',res);
 this.loop();
 });
}
  goto() {
    this.router.navigate(['/layout/pipeline-admin/comission-quota'], {

      queryParams: {
        data: btoa(JSON.stringify(this.data))
      }
    });
  }
  goTo1(id,cadence) {
    debugger;
    let Quotaname1={
      _id:id,
      cadence:cadence
  }
  console.log(Quotaname1);
    this.router.navigate(['/layout/pipeline-admin/comission-quota-type'], {

      queryParams: {
        data: btoa(JSON.stringify(this.Quotaname1))
      }
    });
  }
//getQuotaByIdid,Role) {
    

  onSubmit2() {
    this.router.navigate(['/layout/pipeline-admin/comission-quota-user'], {

      queryParams: {
        data: btoa(JSON.stringify(this.data))
      }
    });
  }
  delete(_id) {
    debugger;
    this.service.deleteQuota(_id).subscribe(res => {
    if (res['success']) {
    debugger;
    this.service.showToaster('user has been deleted');
    this.ngOnInit()
    
    }
    }, err => {
    console.log(err);
    this.service.ErrorSuccess('user Can not be deleted!');
    }
    )
    }
}
