import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionQuotaQuotauserComponent } from './comission-quota-quotauser.component';

describe('ComissionQuotaQuotauserComponent', () => {
  let component: ComissionQuotaQuotauserComponent;
  let fixture: ComponentFixture<ComissionQuotaQuotauserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionQuotaQuotauserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionQuotaQuotauserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
