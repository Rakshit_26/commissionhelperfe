import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionProductGroupEditComponent } from './commission-product-group-edit.component';

describe('CommissionProductGroupEditComponent', () => {
  let component: CommissionProductGroupEditComponent;
  let fixture: ComponentFixture<CommissionProductGroupEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionProductGroupEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionProductGroupEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
