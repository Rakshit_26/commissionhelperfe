import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { stringify } from 'querystring';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-commission-product-group-edit',
  templateUrl: './commission-product-group-edit.component.html',
  styleUrls: ['./commission-product-group-edit.component.scss']
})
export class CommissionProductGroupEditComponent implements OnInit {
  formGroup: FormGroup = new FormGroup({
    GroupName: new FormControl(''),
  });
  public tabSelected: boolean = false;


  id:any;
  
  public rightSideMyListColumns: any[] = [];
  
  public leftSideMyListColumns: any[] = [];
    productlist: any;
    productlists: any;
  data: any;
  submitted: boolean;
  
  public addColumnMyListToRightSide(column){
    let index1 = -1;
    this.leftSideMyListColumns.find((item,index) => {
      if(item.productName == column.productName)
      {
        index1 = index;
        return true;
      }
    });
    this.rightSideMyListColumns.push(column);
    this.leftSideMyListColumns.splice(index1, 1);
  }
  public addColumnMyListToLeftSide(column): void {
    let index1 = -1
    this.rightSideMyListColumns.find((item,index) => {
      if(item.productName == column.productName)
      {
        index1 = index;
        return true;
      }
    });
    this.leftSideMyListColumns.push(column);
    this.rightSideMyListColumns.splice(index1, 1);
   
  }
  
  
  _a: any;
  selection = new SelectionModel<any>(true, []);
  userDetails: any;
  
    constructor(private ToastrService: ToastrService,private formBuilder: FormBuilder,private service: ComissionHelperService,private router: Router,private route: ActivatedRoute  ,public dialog: MatDialog, ) { }
  
    ngOnInit(): void {
      this.getProductGroupInfo() 
      this.getMyListColumns()
    }
    getProductGroupInfo() {
      this.route.queryParams.subscribe(parsam => {
      this.id = JSON.parse(atob(parsam.data));
      
      console.log( this.id);
      this.service.getProductGroupInfo(this.id).subscribe(res => {
      
      
      console.log(res);
      this.data = res['allProducts'];
      
      console.log( this.data);
      
      
      this.formGroup = this.formBuilder.group({
      'GroupName': [this.data.GroupName],
      'productName': [this.data.productName],
      });
      });
      });
    }
    getMyListColumns() {
      debugger;
      this.service.getAllProduct().subscribe(res => {
  
        console.log(res);
  
        this.productlist = res['allProducts']
        this.leftSideMyListColumns =  this.productlist;//Complete
        console.log(this.leftSideMyListColumns);
       if(this.productlist && this.productlists!= undefined && this.productlist.length > 0){
           this.rightSideMyListColumns =  this.productlist;
           console.log(this.rightSideMyListColumns);
          this.rightSideMyListColumns.forEach(item1 => {
            let index1 = -1
            this.leftSideMyListColumns.find((item,index) => {
              if(item.productName == item1.productName)
              {
                index1 = index;
                return true;
              }
            });
            this.leftSideMyListColumns.splice(index1, 1);
          });
        }
  
        
      }, err => {
  
      })
    }
    
    postreportData(post) {
      debugger;
      let arr=[];
      
      this.rightSideMyListColumns.forEach(element => {
        arr.push(element._id);
      });
          let PrimaryUser = {
      
            GroupName: post.GroupName,
            productIds: arr,
          }
      this.service.editProductGroup(PrimaryUser,this.id).subscribe(res => {

        if (res['success']) {
          this.service.showToaster('product update Successfully');
  
        }
  
      })
    }
    cancel() {
      this.submitted = false;
      this.router.navigate(['./layout/pipeline-admin/commission-product-group'])
      }
  }