

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { PlandailogboxComponent } from '../plandailogbox/plandailogbox.component';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

export interface PlanRuleInfo {
  Rule: string;
  Type: string;
  Payoutformula: string;
}
const PLANRULE_DATA: PlanRuleInfo[] = [
  { Rule: 'Opportunity Bonus', Type: 'REDUCED', Payoutformula: '' },
  { Rule: 'Per Deal Commission', Type: 'SET', Payoutformula: '' }

];
@Component({
  selector: 'app-planrules',
  templateUrl: './planrules.component.html',
  styleUrls: ['./planrules.component.css']
})
export class PlanrulesComponent implements OnInit {
  displayedColumns: string[] = ['Rule', 'Type', 'Payoutformula'];
  dataSource = PLANRULE_DATA;
  data: any;
  plandata: any;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private service: ComissionHelperService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(parsam => {
  
      this.data = JSON.parse(atob(parsam.data));
      console.log(this.data);
      });
      this.service.obtainPlanById(this.data).subscribe(res=>{
        console.log(res);
          this.plandata = res['fetchPlan']
          console.log(this.plandata);
    });
  }
  openDailog() {
    this.dialog.open(PlandailogboxComponent);
  }
  goto(): void{
    this.router.navigate(['/layout/pipeline-admin/plan']);
    }
changeuser(): void{
  this.router.navigate(['/layout/pipeline-admin/planuser'],{
    queryParams:{data:btoa(JSON.stringify(this.data)) }
  });
}
changerule(): void{
  this.router.navigate(['/layout/pipeline-admin/planrules'],{
    queryParams:{data:btoa(JSON.stringify(this.data)) }
  });
}
changecode(): void{
  this.router.navigate(['/layout/pipeline-admin/plancustomfield'],{
    queryParams:{data:btoa(JSON.stringify(this.data)) }
  });
}

}
