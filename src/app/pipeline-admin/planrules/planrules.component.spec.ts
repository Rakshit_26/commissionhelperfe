import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanrulesComponent } from './planrules.component';

describe('PlanrulesComponent', () => {
  let component: PlanrulesComponent;
  let fixture: ComponentFixture<PlanrulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanrulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanrulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
