import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpmissionPlanmemberComponent } from './cpmission-planmember.component';

describe('CpmissionPlanmemberComponent', () => {
  let component: CpmissionPlanmemberComponent;
  let fixture: ComponentFixture<CpmissionPlanmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpmissionPlanmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpmissionPlanmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
