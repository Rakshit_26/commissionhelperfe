import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionIndirectCalculatorComponent } from './commission-indirect-calculator.component';

describe('CommissionIndirectCalculatorComponent', () => {
  let component: CommissionIndirectCalculatorComponent;
  let fixture: ComponentFixture<CommissionIndirectCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionIndirectCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionIndirectCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
