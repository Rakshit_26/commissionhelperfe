import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionIndirectCalculatorPage2Component } from './commission-indirect-calculator-page2.component';

describe('CommissionIndirectCalculatorPage2Component', () => {
  let component: CommissionIndirectCalculatorPage2Component;
  let fixture: ComponentFixture<CommissionIndirectCalculatorPage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionIndirectCalculatorPage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionIndirectCalculatorPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
