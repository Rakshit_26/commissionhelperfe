import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionUserAddComponent } from './comission-user-add.component';

describe('ComissionUserAddComponent', () => {
  let component: ComissionUserAddComponent;
  let fixture: ComponentFixture<ComissionUserAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionUserAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionUserAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
