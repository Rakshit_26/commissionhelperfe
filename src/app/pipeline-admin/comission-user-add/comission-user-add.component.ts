import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

@Component({
  selector: 'app-comission-user-add',
  templateUrl: './comission-user-add.component.html',
  styleUrls: ['./comission-user-add.component.scss']
})
export class ComissionUserAddComponent implements OnInit {
  formGroup: FormGroup = new FormGroup({
    Name: new FormControl(''),
    contactNumber: new FormControl(''),
     password: new FormControl(''),
    confirmPassword: new FormControl(''),
username: new FormControl(''),
Company: new FormControl(''),   
});

submitted = false;
userType: any;
Role:any;
choose:any;
setvalue(drp: any) {
  this.choose =drp;
  switch (this.choose) {
      case 'User': this.Role = 'User'; this.userType = 1; break;
      case 'Admin': this.Role = 'Admin'; this.userType = 2; break;
  }
  
  console.log(this.Role, this.userType);
}
/*setvalue(post)
{console.log(post)}*/
  constructor(private formBuilder: FormBuilder,private service: ComissionHelperService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      Name: new FormControl(''),
      contactNumber: new FormControl(''),
      password: new FormControl(''),
      confirmPassword: new FormControl(''),
     Role: new FormControl(''),
      username: new FormControl(''),
        Company: new FormControl(''),

  });

        }
  cancel() {
    this.submitted = false;
    this.router.navigate(['/layout/pipeline-admin/user'])

}
onSubmit(post) {
 
let PrimaryUser = {

  Role: this.Role,
  username: post.username,
  password: post.password,
 // confirmPassword: post.confirmPassword,
   Name: post.Name,
   contactNumber: post.contactNumber,
  Company: post.Company,

  userType: this.userType
};

debugger;
this.submitted = true;


  this.service.addUser(PrimaryUser) .subscribe(res => {
      if (res['success']) {
          this.service.showToaster(' User has been registered');
         
      }
  }, (err) => {
      console.log(err);
      this.service.showToaster('User can not be registered!');
  }
  );

}
}
