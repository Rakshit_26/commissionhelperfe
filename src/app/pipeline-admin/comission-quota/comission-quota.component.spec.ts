import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionQuotaComponent } from './comission-quota.component';

describe('ComissionQuotaComponent', () => {
  let component: ComissionQuotaComponent;
  let fixture: ComponentFixture<ComissionQuotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionQuotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionQuotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
