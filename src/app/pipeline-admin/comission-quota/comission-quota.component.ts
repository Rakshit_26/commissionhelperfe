
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComissionHelperService } from 'app/shared/services/comission-helper.service'


@Component({
  selector: 'app-comission-quota',
  templateUrl: './comission-quota.component.html',
  styleUrls: ['./comission-quota.component.scss']
})
export class ComissionQuotaComponent implements OnInit {

  planData: any;
  QuotaData: any;
  dataSource: any;

  userData = [];
  gridView = [];
  message: any;
  TimeZoneListDetails = [];
  opened: boolean = false;
  public IsUpdateAgent: boolean = false;
  
  public deleteDialogState: boolean = false;
  public _idToBeDeleted: any = null;
  public isFilterOn: boolean = false;
  
  checkBoxUserSelectedRow: any;
  public mySelectionUser: any[] = [];
  keyChangeUser(e) {
  console.log('Selected items:', e);
  this.checkBoxUserSelectedRow = e[0];
  console.log('id of selected checkbox' + this.checkBoxUserSelectedRow);
  }
  constructor(private route: ActivatedRoute, private router: Router,private service:ComissionHelperService) { 
    
  }

   ngOnInit(): void {
    
 this.getAllQuota()
  
}  getAllQuota(){
  this.service.getAllQuota().subscribe(res=>{
    this.QuotaData=res['allQuota']
   
     console.log(res);
  });
}
  goTo(id,cadence){
    let data={
      id:id,
      cadence:cadence
    }
     this.router.navigate(['/layout/pipeline-admin/comission-quota-quotauser'],{
       queryParams: {data: btoa(JSON.stringify( data))}});
    }
   
   delete(_id) {
      debugger;
      this.service.deleteQuota(_id).subscribe(res => {
      if (res['success']) {
      debugger;
      this.service.showToaster('Quota has been deleted');
      this.ngOnInit()
      
      }
      }, err => {
      console.log(err);
      this.service.ErrorSuccess('Quota Can not be deleted!');
      }
      )
      }
      Quotapopup(){
        debugger;
        this.router.navigate(['/layout/pipeline-admin/comission-quotaadd'])
        }
}
