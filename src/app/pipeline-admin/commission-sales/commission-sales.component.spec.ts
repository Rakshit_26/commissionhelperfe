import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionSalesComponent } from './commission-sales.component';

describe('CommissionSalesComponent', () => {
  let component: CommissionSalesComponent;
  let fixture: ComponentFixture<CommissionSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
