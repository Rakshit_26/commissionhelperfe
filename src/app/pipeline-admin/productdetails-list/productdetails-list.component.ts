import { Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ProductDeatailsService } from 'app/shared/services/pipelineadmin/product-deatails.service';
//import { products } from 'app/pages/full-layout-page/full-layout-page.component';
//import { ProductDetailsEditComponent } from '../product-details-edit/product-details-edit.component';

@Component({
  selector: 'app-productdetails-list',

  templateUrl: './productdetails-list.component.html',
  styleUrls: ['./productdetails-list.component.scss']
  

})
export class ProductdetailsListComponent implements OnInit {
  userData = [];
  gridView = [];
  message: any;
  TimeZoneListDetails = [];
  opened: boolean = false;
  public IsUpdateAgent: boolean = false;

  public deleteDialogState: boolean = false;
  public _idToBeDeleted: any = null;
  public isFilterOn: boolean = false;
  
  checkBoxUserSelectedRow: any;
  public mySelectionUser: any[] = [];
  keyChangeUser(e) {
  console.log('Selected items:', e);
  this.checkBoxUserSelectedRow = e[0];
  console.log('id of selected checkbox' + this.checkBoxUserSelectedRow);
  }

  dialogAction: string = '';
  dialogAction1: boolean = true;// true-add false-update
  formGroup: FormGroup = new FormGroup({
      _id: new FormControl(''),
      Name: new FormControl(''),
      Address: new FormControl(''),
      City: new FormControl(''),
      State: new FormControl(''),
      Country: new FormControl('')
  });
 
  productData: any;
 
  constructor(private router: Router, public service: ProductDeatailsService,private activatedRoute:ActivatedRoute) {
  
  }

  ngOnInit() {
    debugger;
    this.getAllProduct();
    
  }

  getAllProduct() {

    debugger;
    this.service.getAllProduct().subscribe((res) => {
      debugger;
      
this.productData=(res['allProducts']);

  
 


    }, (err) => {

      // console.log(err);
      debugger;
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }
  edit(id) {
    debugger;
   
    console.log(id);
    
    this.router.navigate(['./layout/pipeline-admin/product-details'], {
    queryParams: {data: btoa(JSON.stringify( id))}})
     }
 

    
    delete(_id) {
      this.service.delete(_id).subscribe(res => {
          if (res['success']) {
              this.service.showToaster('PRODUCT has been deleted');
              this.ngOnInit()

          }
      }, err => {
          console.log(err);
          this.service.ErrorSuccess('User Can not be deleted!');
      }
      )
  }
  deleteItem(_id) {
      this._idToBeDeleted = _id;
      this.deleteDialogState = true;
  }
    closeDeleteDialog(state) {
      this.deleteDialogState = false;
      if (state == 'no') {
          } else if (state == 'yes' && this._idToBeDeleted != null) {
                this.delete(this._idToBeDeleted);
          } else {
      }
  }
  /*editProduct(element){
    console.log(element._id);

    const dialogRef =this.dialog.open(ProductDetailsEditComponent,{data:element._id})
    // this.router.navigate(['../edit'], { relativeTo: this.activatedRoute })

  }*/

 
  // onSubmit() {
  //   this.router.navigate(['../ProductDetails'],{relativeTo:this.activatedRoute})
  //   // // console.log(this.formGroup.value);
  //   // // this.productDetails.addProduct(this.formGroup.value).subscribe(res => {
  //   // //   console.log(res)
  //   // })
  // }
  /*navigate() {
    this.router.navigate(['layout/pipeline-admin/product-details'], { relativeTo: this.activatedRoute });
  }*/
  userpopup(){
    debugger;
    this.router.navigate(['./layout/pipeline-admin/product-details'])
    }
}
