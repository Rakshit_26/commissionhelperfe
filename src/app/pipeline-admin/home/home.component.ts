import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userName: any
  constructor(
    private router: Router
  ) { }
  barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    };
    barChartType: ChartType = 'bar';
    barChartLegend = true;
    barChartPlugins = [];
    barChartLabels: Label[] = ['Abhishek Jha'];
    barChartData: ChartDataSets[] = [
    { data: [26],label: 'Number of Uploaded Invoices', backgroundColor: '#FF9933', hoverBackgroundColor:'#FF9933'}
    ];
    
    barChartOptions2: ChartOptions = {
    responsive: true,
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    };
    barChartType2: ChartType = 'bar';
    barChartLegend2 = true;
    barChartPlugins2 = [];
    barChartLabels2: Label[] = ['10Apr 20', '18Apr 20', '26Apr 20', '27Apr 20', '07May 20'];
    barChartData2: ChartDataSets[] = [
    { data: [100000, 200000, 300000, 400000, 500000],label: 'Amount(INR)', backgroundColor: 'green', hoverBackgroundColor:'#0371BD'}
    ];
    
    barChartOptions1: ChartOptions = {
    responsive: true,
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    };
    barChartType1: ChartType = 'bar';
    barChartLegend1 = true;
    barChartPlugins1 = [];
    barChartLabels1: Label[] = ['Accepted', 'Rejected', 'Pending'];
    barChartData1: ChartDataSets[] = [
    { data: [11, 4, 3], label: 'Number Of Uploaded invoice', backgroundColor: 'orange', hoverBackgroundColor:'#3AABFA'}
    ];
    
    barChartOptions3: ChartOptions = {
    responsive: true,
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    };
    barChartType3: ChartType = 'bar';
    barChartLegend3 = true;
    barChartPlugins3 = [];
    barChartLabels3: Label[] = ['Dec19','Jan20', 'Feb20', 'Mar20', 'Apr20', 'May20'];
    barChartData3: ChartDataSets[] = [
    { data: [0,0,0,0,8,1], label: 'Number of funded invoice', backgroundColor: 'violet', hoverBackgroundColor:'#3F6826'}
    ];
    ngOnInit(): void {

        this.chart1();
        this.piechart(); 
        this.donut();  
        this.semi();
          }
        
        
        chart1(){
          var chart = am4core.create("chartdiv", am4charts.XYChart3D);
        
          // Add data
          chart.data = [{
            "invoice": "Approved",
            "Count": 16 
          }, {
            "invoice": "Rejected",
            "Count": 5
          }, {
            "invoice": "Pending",
            "Count": 10
          }];
          
          // Create axes
          let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "invoice";
          categoryAxis.renderer.labels.template.rotation = 270;
          categoryAxis.renderer.labels.template.hideOversized = false;
          categoryAxis.renderer.minGridDistance = 20;
          categoryAxis.renderer.labels.template.horizontalCenter = "right";
          categoryAxis.renderer.labels.template.verticalCenter = "middle";
          categoryAxis.tooltip.label.rotation = 270;
          categoryAxis.tooltip.label.horizontalCenter = "right";
          categoryAxis.tooltip.label.verticalCenter = "middle";
          
          let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.title.text = "invoices";
          valueAxis.title.fontWeight = "bold";
          
          // Create series
          var series = chart.series.push(new am4charts.ColumnSeries3D());
          series.dataFields.valueY = "Count";
          series.dataFields.categoryX = "invoice";
          series.name = "Count";
          series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          series.columns.template.fillOpacity = .8;
          
          var columnTemplate = series.columns.template;
          columnTemplate.strokeWidth = 2;
          columnTemplate.strokeOpacity = 1;
          columnTemplate.stroke = am4core.color("#FFFFFF");
          
          columnTemplate.adapter.add("fill", function(fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
          })
          
          columnTemplate.adapter.add("stroke", function(stroke, target) {
            return chart.colors.getIndex(target.dataItem.index);
          })
          
          chart.cursor = new am4charts.XYCursor();
          chart.cursor.lineX.strokeOpacity = 0;
          chart.cursor.lineY.strokeOpacity = 0;
        }
        
        
        
        piechart() {
        
        
        
        
        
        var chart = am4core.create("chartdiv1", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        
        chart.legend = new am4charts.Legend();
        
        chart.data = [
          {
            Month: "January",
            Amount: 501
          },
          {
            Month: "February",
            Amount: 301.9
          },
          {
            Month: "March",
            Amount: 201.1
          },
          {
            Month: "April",
            Amount: 165.8
          },
          {
            Month: "May",
            Amount: 139.9
          },
          {
            Month: "June",
            Amount: 128.3
          },
          {
            Month: "July",
            Amount: 200
          },
          {
            Month: "August",
            Amount: 150
          },
          {
            Month: "September",
            Amount: 250
          },
          {
            Month: "October",
            Amount: 450
          },
          {
            Month: "November",
            Amount: 150
          },
          {
            Month: "December",
            Amount: 176
          }
        ];
        
        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "Amount";
        series.dataFields.category = "Month";
        
        
        }
        donut() {
        
        
        
        
        
        var chart = am4core.create("chartdiv2", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        
        chart.legend = new am4charts.Legend();
        
        chart.data = [
          {
            Month: "January",
            Amount: 501
          },
          {
            Month: "February",
            Amount: 301.9
          },
          {
            Month: "March",
            Amount: 201.1
          },
          {
            Month: "April",
            Amount: 165.8
          },
          {
            Month: "May",
            Amount: 139.9
          },
          {
            Month: "June",
            Amount: 128.3
          },
          {
            Month: "July",
            Amount: 200
          },
          {
            Month: "August",
            Amount: 150
          },
          {
            Month: "September",
            Amount: 250
          },
          {
            Month: "October",
            Amount: 450
          },
          {
            Month: "November",
            Amount: 150
          },
          {
            Month: "December",
            Amount: 176
          }
        ];
        chart.innerRadius = 100;
        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "Amount";
        series.dataFields.category = "Month";
        
        
        
        
        }
        
        
        semi(){
        
        
        var chart = am4core.create("chartdiv3", am4charts.PieChart);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        
        chart.data = [
          {
            Month: "January",
            Count: 501
          },
          {
            Month: "February",
            Count: 301.9
          },
          {
            Month: "March",
            Count: 201.1
          },
          {
            Month: "April",
            Count: 165.8
          },
          {
            Month: "May",
            Count: 139.9
          },
          {
            Month: "June",
            Count: 128.3
          },
          {
            Month: "July",
            Count: 200
          },
          {
            Month: "August",
            Count: 150
          },
          {
            Month: "September",
            Count: 250
          },
          {
            Month: "October",
            Count: 450
          },
          {
            Month: "November",
            Count: 150
          },
          {
            Month: "December",
            Count: 176
          }
        ];
        chart.radius = am4core.percent(70);
        chart.innerRadius = am4core.percent(40);
        chart.startAngle = 180;
        chart.endAngle = 360;  
        
        var series = chart.series.push(new am4charts.PieSeries());
        series.dataFields.value = "Count";
        series.dataFields.category = "Month";
        
        series.slices.template.cornerRadius = 10;
        series.slices.template.innerCornerRadius = 7;
        series.slices.template.draggable = true;
        series.slices.template.inert = true;
        series.alignLabels = false;
        
        series.hiddenState.properties.startAngle = 90;
        series.hiddenState.properties.endAngle = 90;
        
        chart.legend = new am4charts.Legend();
        
        }
        
        
        
    
    
    }