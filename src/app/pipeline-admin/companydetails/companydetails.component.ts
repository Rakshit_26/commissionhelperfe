import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DataSource } from '@angular/cdk/table';
import { CompanydetailsService } from 'app/shared/services/pipelineadmin/companydetails.service';

@Component({
  selector: 'app-companydetails',
  templateUrl: './companydetails.component.html',
  styleUrls: ['./companydetails.component.css'],
  providers: [CompanydetailsService]
})
export class CompanydetailsComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  message: any;
  company: any;
  allUsers = [];
    public displayedColumns = ['S No','firstname','lastname', 'SalesForceId', 'Status', 'username', 'CompanyId', 'Role','viewDetails'];
  length: number = 0;
  limit: number = 10;
  CompanyId:string;
  pageLimit: number[] = [5, 10] ;
  pageSize:number =10;
  property:any = 'test';
  order:any='desc';
  pageIndex : number = 1;
  previousPageIndex:number =0;
  initialPage ={
    "length": 1,
    "pageIndex": 0,
    "pageSize": 5,
    "previousPageIndex": 0
  }

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private router: Router, public dialog: MatDialog,public service : CompanydetailsService ) { 
    this.dataSource = new MatTableDataSource<any>();
  }

  ngOnInit() {
    debugger;
    this.CompanyId = localStorage.getItem('companyId')
    this.getCompanyDetails();
    this.getCompanyWithUser();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getCompanyDetails(){
  
    debugger;
    this.service.getCompanyById(this.CompanyId).subscribe((res) => {
      debugger;
      this.company = res['Company'];

    }, (err) => {     
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }
  getCompanyWithUser(){
  
    debugger;
    this.service.getCompanyWithUser(this.CompanyId,this.pageIndex, this.pageSize,this.property, this.order).subscribe((res) => {
      debugger;
        this.allUsers = res['User'];
        this.dataSource = new MatTableDataSource(this.allUsers);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.limit = this.pageSize
        this.length = res['totalCount'];
      //   this.initialPage ={
      //     "length": this.length,
      //     "pageIndex": 0,
      //     "pageSize": 10,
      //     "previousPageIndex": 0
      //   }
      //  this.changePage(this.initialPage);

    }, (err) => {     
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }

   ngAfterViewInit() {
    debugger;
  }
  changePage(event) {
    debugger;
    this.pageSize = event.pageSize
    this.pageIndex = event.pageIndex +1
    this.previousPageIndex = event.previousPageIndex
    this.service.getCompanyWithUser(this.CompanyId,this.pageIndex, this.pageSize,this.property, this.order).subscribe((res) => {
      //debugger;
          //if (res['statusCode'] === 200){
            this.allUsers = res['Company'];
            console.log(this.allUsers)
            if(this.pageIndex === 0){
            
              this.dataSource =  new MatTableDataSource(this.allUsers);
                this.dataSource.sort = this.sort;
           
            }
            if(this.allUsers.length >0){
              this.dataSource =  new MatTableDataSource(this.allUsers);
               this.dataSource.sort = this.sort;
            }
            else{
             debugger;
              this.pageIndex = event.pageIndex-1
              this.previousPageIndex = event.previousPageIndex
              this.pageSize = event.pageSize -1
              if(this.pageIndex < 0){
                this.pageIndex = 0;
              }
              this.service.getCompanyWithUser( this.CompanyId,this.pageIndex, this.pageSize,this.property, this.order ).subscribe((res) => {
                if (res['statusCode'] === 200){
                  this.allUsers = res['User'];
                  this.dataSource =  new MatTableDataSource(this.allUsers);
                  this.dataSource.sort = this.sort;
                  
                }
              }, (err) => {
               
                // console.log(err);
                 
                this.message = err['error']['errorMessage'];
                this.service.ErrorSuccess(this.message);
            });
             }
          //}
        }, (err) => {
           
            // console.log(err);
             
            this.message = err['error']['errorMessage'];
            this.service.ErrorSuccess(this.message);
        });
  }
  
  viewDetails(companyData){
    debugger;
    localStorage.removeItem("CompanyId")
    //this.router.navigate(['/dashboard/pipeline-admin/companydetails'], {state: {companyData}});
  }
  
  
  sortData(event){
    debugger;
    this.order=event.direction;
    this.property=event.active;
   
    this.service.getCompanyWithUser(this.CompanyId ,this.pageIndex, this.pageSize,this.property, this.order).subscribe(res =>{
      debugger;
       this.allUsers = res['User'];
       this.length = res['totalCount'];
   
       this.dataSource = new MatTableDataSource(this.allUsers);
     
       this.dataSource.sort = this.sort;
    
       
   
   }, (err) => {
    debugger;
     // console.log(err);
    
     this.message = err.error['errorMessage'];
     this.service.ErrorSuccess(this.message);
   });
   
  }

  
}
