import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgentService } from 'app/shared/services/pipelineadmin/agent.service';
import { FormControl } from '@angular/forms'
  

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user;

  constructor(private router: Router, private service: AgentService) { }

  ngOnInit(): void {

  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login'])
  }

}
export class SelectCustomTriggerExample {
  // Admin = new FormControl();
 
//  AdminList: string[] = ['Users', 'Overwrites', 'Connectors', 'Settings', 'Reports'];
}

