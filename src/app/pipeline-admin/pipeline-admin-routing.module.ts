
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CompanycomponentComponent } from './companycomponent/companycomponent.component';
import { CompanydetailsComponent } from './companydetails/companydetails.component';
import {PipelineAdminComponent } from './pipeline-admin.component';
import { CompanyhistoryComponent } from './companyhistory/companyhistory.component';
import { DialerListComponent } from './dialer-list/dialer-list.component';
import { DialerListDetailsComponent } from './dialer-list-details/dialer-list-details.component';
import { UserComponent } from './user/user.component';
import { PlanrulesComponent } from './planrules/planrules.component';
import { RollUpsComponent } from './roll-ups/roll-ups.component';
import { PlanAssignmentsComponent } from './plan-assignments/plan-assignments.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductdetailsListComponent } from './productdetails-list/productdetails-list.component';
import { CommissionComponent } from './commission/commission.component';
import { CommissionAddComponent } from './commission-add/commission-add.component';
import { ComissionTeamComponent } from './comission-team/comission-team.component';
import { ComissionAddteamComponent } from './comission-addteam/comission-addteam.component';
import { ComissionUserAddComponent } from './comission-user-add/comission-user-add.component';
import { ComissionPlanComponent } from './comission-plan/comission-plan.component';
import { ComissionPlanmemberComponent } from './comission-planmember/comission-planmember.component';
import { ComissionPlanAssignuserComponent } from './comission-plan-assignuser/comission-plan-assignuser.component';
import { ComissionPlanAddplanComponent } from './comission-plan-addplan/comission-plan-addplan.component';
import { ComissionQuotaComponent } from './comission-quota/comission-quota.component';
import { ComissionQuotaaddComponent } from './comission-quotaadd/comission-quotaadd.component';
import { ComissionQuotaQuotauserComponent } from './comission-quota-quotauser/comission-quota-quotauser.component';
import { ComissionQuotaTypeComponent } from './comission-quota-type/comission-quota-type.component';
import { ComissionQuotaUserComponent } from './comission-quota-user/comission-quota-user.component';
import { CommissionUserEditComponent } from './commission-user-edit/commission-user-edit.component';
import { CommissionProductGroupComponent } from './commission-product-group/commission-product-group.component';
import { CommissionProductGroupAddComponent } from './commission-product-group-add/commission-product-group-add.component';
import { CommissionProductGroupEditComponent } from './commission-product-group-edit/commission-product-group-edit.component';
import { ProductGroupComponent } from './product-group/product-group.component';
import { CommissionSalesComponent } from './commission-sales/commission-sales.component';
import { ComissionTeamAddComponent } from './comission-team-add/comission-team-add.component';
import { ComissionTeamEditComponent } from './comission-team-edit/comission-team-edit.component';
import { CommissionDirectCalculatorComponent } from './commission-direct-calculator/commission-direct-calculator.component';
import { CommissionDirectCalculatorPage2Component } from './commission-direct-calculator-page2/commission-direct-calculator-page2.component';
import { CommissionIndirectCalculatorComponent } from './commission-indirect-calculator/commission-indirect-calculator.component';
import { CommissionIndirectCalculatorPage2Component } from './commission-indirect-calculator-page2/commission-indirect-calculator-page2.component';
import { CommissionHelperComponent } from './commission-helper/commission-helper.component';




const routes: Routes = [
  { path: '', component: PipelineAdminComponent,
      children: [
  { path: '', redirectTo: 'home', pathMatch: 'prefix' },
  { path: 'home', component: HomeComponent},
  { path: 'company', component: CompanycomponentComponent },
  { path: 'companyDetails', component: CompanydetailsComponent },
  { path: 'companyHistory', component: CompanyhistoryComponent },

  { path: 'dialerlist', component: DialerListComponent  },
  { path: 'dialerlistdetails', component: DialerListDetailsComponent},
  {path: 'user', component:UserComponent},
{ path: 'commission-user-edit', component:CommissionUserEditComponent},
  { path: 'planrules', component: PlanrulesComponent},

  { path: 'roll-ups', component: RollUpsComponent},
  { path: 'plan-assignments', component: PlanAssignmentsComponent},
  { path:'product-details', component: ProductDetailsComponent},
  { path: 'productdetails-list', component:ProductdetailsListComponent},
  {  path: 'commission', component:CommissionComponent},
  { path: 'commission-add', component:CommissionAddComponent},
  { path: 'comission-team', component:ComissionTeamComponent},
  {path: 'comission-addteam',component:ComissionAddteamComponent},
  {path: 'comission-user-add',component:ComissionUserAddComponent},
  {path: 'comission-plan', component:ComissionPlanComponent},
  {path: 'comission-planmember', component:ComissionPlanmemberComponent},
  {path: 'comission-plan-assignuser',component:ComissionPlanAssignuserComponent},
  {path: 'comission-plan-addplan',component: ComissionPlanAddplanComponent},
  {path:'comission-quota',component:ComissionQuotaComponent},
  {path: 'comission-quotaadd', component:ComissionQuotaaddComponent},
  {path: 'comission-quota-quotauser',component:ComissionQuotaQuotauserComponent},
  {path: 'comission-quota-type',component:ComissionQuotaTypeComponent},
  {path: 'comission-quota-user',component:ComissionQuotaUserComponent},
 { path: 'commission-product-group-edit', component:CommissionProductGroupEditComponent},
{ path: 'commission-product-group', component: CommissionProductGroupComponent},
{ path: 'commission-product-group-add', component:CommissionProductGroupAddComponent},
 {path: 'product-group', component: ProductGroupComponent},
 {path: 'commission-sales',component:CommissionSalesComponent},
{path: 'comission-team-add', component:ComissionTeamAddComponent},
{ path: 'comission-team-edit',component:ComissionTeamEditComponent},
{path: 'commission-direct-calculator', component:CommissionDirectCalculatorComponent},
{path: 'commission-direct-calculator-page2', component:CommissionDirectCalculatorPage2Component},
{path: 'commission-indirect-calculator', component:CommissionIndirectCalculatorComponent},
{path: 'commission-indirect-calculator-page2', component:CommissionIndirectCalculatorPage2Component},
{path: 'commission-Helper', component:CommissionHelperComponent}
]},
  
];

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PipelineAdminRoutingModule { }
