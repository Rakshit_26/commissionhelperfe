import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionQuotaTypeComponent } from './comission-quota-type.component';

describe('ComissionQuotaTypeComponent', () => {
  let component: ComissionQuotaTypeComponent;
  let fixture: ComponentFixture<ComissionQuotaTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionQuotaTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionQuotaTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
