import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComissionHelperService} from'app/shared/services/comission-helper.service';

@Component({
  selector: 'app-comission-quota-type',
  templateUrl: './comission-quota-type.component.html',
  styleUrls: ['./comission-quota-type.component.scss']
})
export class ComissionQuotaTypeComponent implements OnInit {

  data:any;
  userid:any
  user: any;
  userPlan:any;
  selected: any = [];
  update: any;
  empty: any ;
   id:any;
  formGroup: FormGroup;
  
  newdata: any;
  username: any=[];
    usernames: any=[];
    usernamess: any=[];
  addUser: any;
  cadence: any;
  choose: string;
  
    constructor(private router: Router,private service: ComissionHelperService,private fb: FormBuilder ,  private route: ActivatedRoute,) { }
  
    ngOnInit(): void {
     
      this.formGroup = this.fb.group({
        teamName: ['']
      });
    
      this.route.queryParams.subscribe(parsam => {

        this.user = JSON.parse(atob(parsam.data));
        
        console.log('id',this.user);
        this.cadence=this.user.cadence;
        
        switch(this.cadence)
  {
  case 'Yearly':this.choose='1';break;
  case 'Quterly':this.choose='2';;break;
  case 'Monthly':this.choose='3';break;
   }
       
      });  
     }
    
     goto() {
      this.router.navigate(['/layout/pipeline-admin/comission-quota-qoutauser'], {
  
        queryParams: {
          data: btoa(JSON.stringify(this.data))
        }
      });
    }
  


   
}
