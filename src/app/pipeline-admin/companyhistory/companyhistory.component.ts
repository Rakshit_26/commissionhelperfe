
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DataSource } from '@angular/cdk/table';
import { CompanyhistoryService } from 'app/shared/services/pipelineadmin/companyhistory.service';
@Component({
  selector: 'app-companyhistory',
  templateUrl: './companyhistory.component.html',
  styleUrls: ['./companyhistory.component.css']
})
export class CompanyhistoryComponent {
  dataSource: MatTableDataSource<any>;
  message: any;
  CompanyId:string;
  company: any;
  allCompanies = [];
  public displayedColumns = ['S No', 'username', 'Role', 'SalesForceId', 'createdAt', 'Country', 'Address', 'License', 'License_start_date', 'License_end_date', 'callCount'];
  length: number = 0;
  limit: number = 10;
  property:any = 'createdAt';
  order:any='desc';
  pageLimit: number[] = [5, 10] ;
  pageSize:number =10;
  pageIndex : number = 1;
  previousPageIndex:number =0;
  // search = {
  //   OrgId: '',
  //   CompanyName: ''   
  // };
  initialPage ={
    "length": 1,
    "pageIndex": 0,
    "pageSize": 5,
    "previousPageIndex": 0
  }

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

   constructor(private router: Router, public dialog: MatDialog,public service : CompanyhistoryService ) { 
    this.dataSource = new MatTableDataSource<any>();
   }
   
   ngOnInit() {
    debugger;
    this.CompanyId = localStorage.getItem('companyId')
    this.getHistory();
    this.getCompanyDetails();
  }
  getCompanyDetails(){
  
    debugger;
    this.service.getCompanyById(this.CompanyId).subscribe((res) => {
      debugger;
      this.company = res['Company'];

    }, (err) => {     
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }
  getHistory(){
  
  debugger;
 
    this.service.getHistory(this.CompanyId, this.pageIndex, this.pageSize,this.property,this.order).subscribe((res) => {
      debugger;
      this.allCompanies = res['History'];
      if(this.property="createdAt")
      {
        for (let index =this.allCompanies.length-1;  index = 0;  index--) {
          debugger;
          if(this.allCompanies[index].Country!=this.allCompanies[index-1].Country)
          {
            this.allCompanies[index-1].changecountry=true;
          }

          if(this.allCompanies[index].Address!=this.allCompanies[index-1].Address)
          {
            this.allCompanies[index-1].changeaddress=true;
          }

          if(this.allCompanies[index].License_start_date!=this.allCompanies[index-1].License_start_date)
          {
            this.allCompanies[index-1].changeLicense_start_date=true;
          }

          if(this.allCompanies[index].License_end_date!=this.allCompanies[index-1].License_end_date)
          {
            this.allCompanies[index-1].changeLicense_end_date=true;
          }

          if(this.allCompanies[index].callCount!=this.allCompanies[index-1].callCount)
          {
            this.allCompanies[index-1].changecallCount=true;
          }

          if(this.allCompanies[index].License!=this.allCompanies[index-1].License)
          {
            this.allCompanies[index-1].changelicense=true;
          }
          

        }
        // this.allCompanies = res['History'];
        this.dataSource = new MatTableDataSource(this.allCompanies);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.limit = this.pageSize;
        this.length = this.allCompanies.length;
      }
      else{
        // this.allCompanies = res['History'];
        this.dataSource = new MatTableDataSource(this.allCompanies);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.limit = this.pageSize;
        this.length = this.allCompanies.length;

      }
    
        
    }, (err) => {
     
      // console.log(err);
     debugger;
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }

   ngAfterViewInit() {
  
  }

  changePage(event) {
    debugger;
    this.pageSize = event.pageSize
    this.pageIndex = event.pageIndex +1
    this.previousPageIndex = event.previousPageIndex
    this.service.getHistory(this.CompanyId, this.pageIndex, this.pageSize,this.property,this.order).subscribe((res) => {
      //debugger;
          //if (res['statusCode'] === 200){
            this.allCompanies = res['Company'];
            console.log(this.allCompanies)
            if(this.pageIndex === 0){
            
              this.dataSource =  new MatTableDataSource(this.allCompanies);
                this.dataSource.sort = this.sort;
           
            }
            if(this.allCompanies.length >0){
              this.dataSource =  new MatTableDataSource(this.allCompanies);
               this.dataSource.sort = this.sort;
            }
            else{
             debugger;
              this.pageIndex = event.pageIndex-1
              this.previousPageIndex = event.previousPageIndex
              this.pageSize = event.pageSize -1
              if(this.pageIndex < 0){
                this.pageIndex = 0;
              }
              this.service.getHistory(this.CompanyId, this.pageIndex, this.pageSize,this.property, this.order).subscribe((res) => {
                if (res['statusCode'] === 200){
                  this.allCompanies = res['Company'];
                  this.dataSource =  new MatTableDataSource(this.allCompanies);
                  this.dataSource.sort = this.sort;
                  
                }
              }, (err) => {
               
                // console.log(err);
                 
                this.message = err['error']['errorMessage'];
                this.service.ErrorSuccess(this.message);
            });
             }
          //}
        }, (err) => {
           
            // console.log(err);
             
            this.message = err['error']['errorMessage'];
            this.service.ErrorSuccess(this.message);
        });
  }

  viewDetails(companyData){
    debugger;
    localStorage.setItem('companyId', companyData._id) // companyData._Id
    this.router.navigate(['/layout/pipeline-admin/companyDetails'], {state: {companyData}});
  }

  viewHistory(companyData){
    debugger;
    localStorage.setItem('companyId', companyData._id) // companyData._Id
    this.router.navigate(['/layout/pipeline-admin/companyHistory'], {state: {companyData}});
  }
  
  
   sortData(event){
    debugger;
    this.order=event.direction;
    this.property=event.active;
   
    this.service.getHistory(this.CompanyId, this.pageIndex, this.pageSize,this.property,this.order).subscribe(res =>{
      debugger;
       this.allCompanies = res['History'];
       this.length = res['totalCount'];
   
       this.dataSource = new MatTableDataSource(this.allCompanies);
     
       this.dataSource.sort = this.sort;
    
       
   
   }, (err) => {
    
     // console.log(err);
    
     this.message = err.error['errorMessage'];
     this.service.ErrorSuccess(this.message);
   });
   
  }

  
}
