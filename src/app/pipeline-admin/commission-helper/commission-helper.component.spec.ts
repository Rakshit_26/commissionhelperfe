import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionHelperComponent } from './commission-helper.component';

describe('CommissionHelperComponent', () => {
  let component: CommissionHelperComponent;
  let fixture: ComponentFixture<CommissionHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
