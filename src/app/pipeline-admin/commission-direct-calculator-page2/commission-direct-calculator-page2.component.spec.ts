import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionDirectCalculatorPage2Component } from './commission-direct-calculator-page2.component';

describe('CommissionDirectCalculatorPage2Component', () => {
  let component: CommissionDirectCalculatorPage2Component;
  let fixture: ComponentFixture<CommissionDirectCalculatorPage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionDirectCalculatorPage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionDirectCalculatorPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
