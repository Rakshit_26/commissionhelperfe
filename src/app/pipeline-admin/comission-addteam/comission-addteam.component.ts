import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

@Component({
  selector: 'app-comission-addteam',
  templateUrl: './comission-addteam.component.html',
  styleUrls: ['./comission-addteam.component.scss']
})
export class ComissionAddteamComponent implements OnInit {

 
  formGroup: FormGroup;
  teamName: string;
  data: any;
  usid:any;
  newdata:any;
  userobj:any = [];
  user: any;
  dataSource: any;
  selected: any;
  teamname: any;
  constructor(private service: ComissionHelperService,private fb: FormBuilder ,private router: Router,private rout:ActivatedRoute) { 
this.formGroup = this.fb.group({
  teamName: ['']
});

  }
  ngOnInit(): void {
  }
  onSubmit(post) {
    debugger
      console.log(post);
    
      this.service.addTeam(post) .subscribe(res => {
        this.teamname = res['saveteam']
        console.log(this.teamname);
        this.data = this.teamname._id;
        console.log(this.data);
       debugger
       this.router.navigate(['./layout/pipeline-admin/comission-teammember'],{
       
        queryParams: {data: btoa(JSON.stringify( this.data ))}
    });
          if (res['success']) {
              this.service.showToaster('team has been registeres');
              
          }
      }, (err) => {
          console.log(err);
          this.service.showToaster('team can not be registered!');
      }
      );
    } 

    goto(){
      this.router.navigate(['/layout/pipeline-admin/comission-team']); 
    }
  /*  selectTeam(){

      this.service.addTeam(this.formGroup.value).subscribe(res => {
        console.log(res);
        this.teamdata = res['saveteam']
        console.log(this.teamdata);
        this.data = this.teamdata._id;
        console.log(this.data);
        this.service.showToaster('team is added');
        this.router.navigate(['/layout/pipeline-admin/comission-teammember'],{
            
          queryParams: {data: btoa(JSON.stringify( this.data ))}
      });
      }
      )}*/
    }
   
  
   


