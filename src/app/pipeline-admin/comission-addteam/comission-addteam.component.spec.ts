import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionAddteamComponent } from './comission-addteam.component';

describe('ComissionAddteamComponent', () => {
  let component: ComissionAddteamComponent;
  let fixture: ComponentFixture<ComissionAddteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionAddteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionAddteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
