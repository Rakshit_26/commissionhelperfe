import { Component, OnInit, ViewChild, APP_ID } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator,PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DataSource } from '@angular/cdk/table';
import { AgentService } from 'app/shared/services/pipelineadmin/agent.service';

@Component({
  selector: 'app-dialer-list-details',
  templateUrl: './dialer-list-details.component.html',
  styleUrls: ['./dialer-list-details.component.css'],
  providers: [AgentService]
})
export class DialerListDetailsComponent  {
  dataSource: MatTableDataSource<any>;
  message: any;
  showAdvanceFilter = false;
  allCompanies = [];
  _id:string;
  public displayedColumns = ['S No','ContactName', 'AccountName','SalesForceId','Email','Phone', 'City','State','createdAt'];
  length: number = 0;
  limit: number = 10;
  filter:string="";
  company:any;
  maxDate : any;
  ListId:string;
  startDate : any;
  property:any = 'test';
  totalCount = 0;
  order:any='desc';
  pageLimit: number[] = [5, 10] ;
  pageSize:number =10;
  pageIndex : number = 1;
  previousPageIndex:number =0;
  initialPage ={
    "length": 1,
    "pageIndex": 0,
    "pageSize": 5,
    "previousPageIndex": 0
  }

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  pageEvent: PageEvent;
   constructor(private router: Router, public dialog: MatDialog,public service : AgentService ) { 
    this.dataSource = new MatTableDataSource<any>();
   }
   
   ngOnInit() {
    debugger;
    this.ListId = localStorage.getItem('contactList')
    this.getListDetails();
    this. getallList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getListDetails(){
  
  debugger;
    this.service.getDialerListDetails(this.ListId).subscribe((res) => {
      debugger;
      this.company = res['listDetails'];
        //  this.company.push(res['listDetails']);
        //  this.dataSource = new MatTableDataSource(this.allCompanies);
        // this.totalCount = res['listDetails'];
        //  this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
    }, (err) => {
     
     
     debugger;
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }

  getallList(){
  
    debugger;
    this.service.getDialerListDetails(this.ListId).subscribe((res) => {
      debugger;
        this.allCompanies = res['contactList'] ;
        this.dataSource = new MatTableDataSource(this.allCompanies);
        this.totalCount = res['contactList'];
         this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      

    }, (err) => {     
      this.message = err.error['errorMessage'];
      this.service.ErrorSuccess(this.message);
    });
  }





  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  changePage(event) {
   
    this.pageSize = event.pageSize
    this.pageIndex = event.pageIndex +1
    this.previousPageIndex = event.previousPageIndex
    this.service.getDialerListDetails(this.ListId).subscribe((res) => {
      
            this.allCompanies = res['listDetails'];
            console.log(this.allCompanies)
            if(this.pageIndex === 0){
            
              this.dataSource =  new MatTableDataSource(this.allCompanies);
                this.dataSource.sort = this.sort;
           
            }
            if(this.allCompanies.length >0){
              this.dataSource =  new MatTableDataSource(this.allCompanies);
               this.dataSource.sort = this.sort;
            }
            else{
             debugger;
              this.pageIndex = event.pageIndex-1
              this.previousPageIndex = event.previousPageIndex
              this.pageSize = event.pageSize -1
              if(this.pageIndex < 0){
                this.pageIndex = 0;
              }
              this.service.getDialerListDetails(this.ListId).subscribe((res) => {
                if (res['statusCode'] === 200){
                  this.allCompanies = res['listDetails'];
                  this.dataSource =  new MatTableDataSource(this.allCompanies);
                  this.dataSource.sort = this.sort;
                  
                }
              }, (err) => {
               
           
                 
                this.message = err['error']['errorMessage'];
                this.service.ErrorSuccess(this.message);
            });
             }
          
        }, (err) => {
           
         
             
            this.message = err['error']['errorMessage'];
            this.service.ErrorSuccess(this.message);
        });
  }


  
   sortData(event){
    debugger;
    this.order=event.direction;
    this.property=event.active;
   
    this.service.getDialerListDetails(this.ListId).subscribe(res =>{
      debugger;
       this.allCompanies = res['listDetails'];
       this.length = res['totalCount'];
   
       this.dataSource = new MatTableDataSource(this.allCompanies);
     
       this.dataSource.sort = this.sort;
    
       
   
   }, (err) => {
    
    
    
     this.message = err.error['errorMessage'];
     this.service.ErrorSuccess(this.message);
   });
   
  }
  
  showAdvancedFilter(value){
    this.showAdvanceFilter = value;
 }
  
}


