import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionTeamAddComponent } from './comission-team-add.component';

describe('ComissionTeamAddComponent', () => {
  let component: ComissionTeamAddComponent;
  let fixture: ComponentFixture<ComissionTeamAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionTeamAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionTeamAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
