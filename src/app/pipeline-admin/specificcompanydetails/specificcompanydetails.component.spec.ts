import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificcompanydetailsComponent } from './specificcompanydetails.component';

describe('SpecificcompanydetailsComponent', () => {
  let component: SpecificcompanydetailsComponent;
  let fixture: ComponentFixture<SpecificcompanydetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificcompanydetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificcompanydetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
