import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog,} from '@angular/material/dialog';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-comission-plan-assignuser',
  templateUrl: './comission-plan-assignuser.component.html',
  styleUrls: ['./comission-plan-assignuser.component.scss']
})
export class ComissionPlanAssignuserComponent implements OnInit {
  submitted = false;
  AddTeam:any=[];
  data: any;
  user: any;
  userPlan: any;
  userid: any[];
  AddTeam1: any=[];
  constructor(private router: Router, private service: ComissionHelperService, private route: ActivatedRoute, public dialog: MatDialog, private formBuilder: FormBuilder) { }
 
  ngOnInit(): void {
    this.route.queryParams.subscribe(parsam => {
  
      this.data = JSON.parse(atob(parsam.data));
      console.log(this.data);
    });
   
    this.obtainPlanById()
  }
  obtainPlanById(){
    this.service.obtainPlanById(this.data).subscribe(res =>{
      this.userPlan= res['fetchPlan'];
      this.userid=this.userPlan.users;
      console.log('userPlan',res);
      this. getAllUser();
 
  });
   }
   getAllUser(){
    this.service.getAllUsers().subscribe(res=>{
      this.AddTeam=res['getall']
      console.log(this.userid.length);
      console.log(this.AddTeam);
      for(let i=0;i<this.userid.length;i++)
      { 
        
        for(let j=0;j<this.AddTeam.length;j++)
        {
          
          if((this.userid[i]==this.AddTeam[j]._id))
          {
            
            console.log(this.AddTeam[j]._id);
            this.AddTeam.splice(j, 1);
          }
          
        }
        this.AddTeam1=this.AddTeam;
      }
     
      console.log('new',this.AddTeam1)
       console.log(res);
    });
  }
  selectedKeysChange(row: number[]) {
    this.user = row;
console.log(this.user)

  }
  onclick(eve) {
    // console.log(eve); 
  }
  
  AssignUser(){
    debugger;
    console.log(this.user,this.userid);
    for(let i=0;i<this.user.length;i++)
    {
      this.userid.push(this.user[i]);
    }
    console.log('userid',this.userid);
   
    let users={users:this.userid}
    this.service.modifyPlan(this.data.id,users).subscribe(res=>{
console.log(res);
this.router.navigate(['/layout/pipeline-admin/comission-planmember'], {

  queryParams: {
    data: btoa(JSON.stringify(this.data))
  }
});

    });
}
}
