import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionPlanAssignuserComponent } from './comission-plan-assignuser.component';

describe('ComissionPlanAssignuserComponent', () => {
  let component: ComissionPlanAssignuserComponent;
  let fixture: ComponentFixture<ComissionPlanAssignuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionPlanAssignuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionPlanAssignuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
