import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipelineAdminRoutingModule } from './pipeline-admin-routing.module';
import { HomeComponent } from './home/home.component';
import { CompanycomponentComponent } from './companycomponent/companycomponent.component';
import { CompanydetailsComponent } from './companydetails/companydetails.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipelineAdminComponent } from './pipeline-admin.component';
import { SpecificcompanydetailsComponent } from './specificcompanydetails/specificcompanydetails.component';
import { HeaderComponent } from './header/header.component';
import { CompanyhistoryComponent } from './companyhistory/companyhistory.component';
import { DialerListComponent } from './dialer-list/dialer-list.component';
import { DialerListDetailsComponent } from './dialer-list-details/dialer-list-details.component';
import {MatTabsModule} from '@angular/material/tabs';
import { UserComponent } from './user/user.component';
import { PlanrulesComponent } from './planrules/planrules.component';
import { PlandailogboxComponent } from './plandailogbox/plandailogbox.component';
import { MaterialModule } from '../material/material';
import { PlanAssignmentsComponent } from './plan-assignments/plan-assignments.component';
import { RollUpsComponent } from './roll-ups/roll-ups.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductdetailsListComponent } from './productdetails-list/productdetails-list.component';
import { CommissionComponent } from './commission/commission.component';
import { CommissionAddComponent } from './commission-add/commission-add.component';
import { ComissionTeamComponent } from './comission-team/comission-team.component';
import { ComissionAddteamComponent } from './comission-addteam/comission-addteam.component';
import { ComissionUserAddComponent } from './comission-user-add/comission-user-add.component';
import { ComissionPlanComponent } from './comission-plan/comission-plan.component';
import { CpmissionPlanmemberComponent } from './cpmission-planmember/cpmission-planmember.component';
import { ComissionPlanAssignuserComponent } from './comission-plan-assignuser/comission-plan-assignuser.component';
import { ComissionPlanmemberComponent } from './comission-planmember/comission-planmember.component';
import { ComissionPlanAddplanComponent } from './comission-plan-addplan/comission-plan-addplan.component';
import { ComissionQuotaComponent } from './comission-quota/comission-quota.component';
import { ComissionQuotaaddComponent } from './comission-quotaadd/comission-quotaadd.component';
import { ComissionQuotaQuotauserComponent } from './comission-quota-quotauser/comission-quota-quotauser.component';
import { ComissionQuotaTypeComponent } from './comission-quota-type/comission-quota-type.component';
import { ComissionQuotaUserComponent } from './comission-quota-user/comission-quota-user.component';
import { CommissionUserEditComponent } from './commission-user-edit/commission-user-edit.component';
import { CommissionProductGroupComponent } from './commission-product-group/commission-product-group.component';
import { CommissionProductGroupAddComponent } from './commission-product-group-add/commission-product-group-add.component';
import { CommissionProductGroupEditComponent } from './commission-product-group-edit/commission-product-group-edit.component';
import { ProductGroupComponent } from './product-group/product-group.component';
import { CommissionSalesComponent } from './commission-sales/commission-sales.component';
import { ComissionTeamAddComponent } from './comission-team-add/comission-team-add.component';
import { ComissionTeamEditComponent } from './comission-team-edit/comission-team-edit.component';
import { CommissionDirectCalculatorComponent } from './commission-direct-calculator/commission-direct-calculator.component';
import { CommissionDirectCalculatorPage2Component } from './commission-direct-calculator-page2/commission-direct-calculator-page2.component';
import { CommissionIndirectCalculatorComponent } from './commission-indirect-calculator/commission-indirect-calculator.component';
import { CommissionIndirectCalculatorPage2Component } from './commission-indirect-calculator-page2/commission-indirect-calculator-page2.component';
import { CommissionHelperComponent } from './commission-helper/commission-helper.component';
@NgModule({
  declarations: [
    PipelineAdminComponent,
    HomeComponent,
    CompanycomponentComponent,
    CompanydetailsComponent,
    SpecificcompanydetailsComponent,
    HeaderComponent,
    CompanyhistoryComponent,
    DialerListComponent,
    DialerListDetailsComponent,
    UserComponent,
    PlanrulesComponent,
    PlandailogboxComponent,
    PlanAssignmentsComponent,
    RollUpsComponent,
    ProductDetailsComponent,
    ProductdetailsListComponent,
    CommissionComponent,
    CommissionAddComponent,
    ComissionTeamComponent,
    ComissionAddteamComponent,
    ComissionUserAddComponent,
    ComissionPlanComponent,
    CpmissionPlanmemberComponent,
    ComissionPlanAssignuserComponent,
    ComissionPlanmemberComponent,
    ComissionPlanAddplanComponent,
    ComissionQuotaComponent,
    ComissionQuotaaddComponent,
    ComissionQuotaQuotauserComponent,
    ComissionQuotaTypeComponent,
    ComissionQuotaUserComponent,
    CommissionUserEditComponent,
    CommissionProductGroupComponent,
    CommissionProductGroupAddComponent,
    CommissionProductGroupEditComponent,
    ProductGroupComponent,
    CommissionSalesComponent,
    ComissionTeamAddComponent,
    ComissionTeamEditComponent,
    CommissionDirectCalculatorComponent,
    CommissionDirectCalculatorPage2Component,
    CommissionIndirectCalculatorComponent,
    CommissionIndirectCalculatorPage2Component,
    CommissionHelperComponent,
    ],
  imports: [
    CommonModule,
    PipelineAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatDatepickerModule,
    GridModule,
    ButtonsModule,
    InputsModule
  ],
  entryComponents: [
     PlandailogboxComponent
  ]
})
export class PipelineAdminModule { }
