import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionDirectCalculatorComponent } from './commission-direct-calculator.component';

describe('CommissionDirectCalculatorComponent', () => {
  let component: CommissionDirectCalculatorComponent;
  let fixture: ComponentFixture<CommissionDirectCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionDirectCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionDirectCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
