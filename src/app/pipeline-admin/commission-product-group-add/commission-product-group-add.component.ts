import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'

import { ToastrService } from 'ngx-toastr';
import { stringify } from 'querystring';
@Component({
  selector: 'app-commission-product-group-add',
  templateUrl: './commission-product-group-add.component.html',
  styleUrls: ['./commission-product-group-add.component.scss']
})
export class CommissionProductGroupAddComponent implements OnInit {
  formGroup: FormGroup = new FormGroup({
    GroupName: new FormControl(''),
  });
public tabSelected: boolean = false;


id:any;

public rightSideMyListColumns: any[] = [];

public leftSideMyListColumns: any[] = [];
  productlist: any;
  productlists: any;
  submitted: boolean;

public addColumnMyListToRightSide(column){
  let index1 = -1;
  this.leftSideMyListColumns.find((item,index) => {
    if(item.productName == column.productName)
    {
      index1 = index;
      return true;
    }
  });
  this.rightSideMyListColumns.push(column);
  this.leftSideMyListColumns.splice(index1, 1);
}
public addColumnMyListToLeftSide(column): void {
  let index1 = -1
  this.rightSideMyListColumns.find((item,index) => {
    if(item.productName == column.productName)
    {
      index1 = index;
      return true;
    }
  });
  this.leftSideMyListColumns.push(column);
  this.rightSideMyListColumns.splice(index1, 1);
 
}


_a: any;
selection = new SelectionModel<any>(true, []);
userDetails: any;

  constructor(private ToastrService: ToastrService,private formBuilder: FormBuilder,private service: ComissionHelperService,private router: Router,private route: ActivatedRoute  ,public dialog: MatDialog, ) { }

  ngOnInit(): void {
    this.getMyListColumns()
  }
  getMyListColumns() {
    debugger;
    this.service.getAllProduct().subscribe(res => {

      console.log(res);

      this.productlist = res['allProducts']
      this.leftSideMyListColumns =  this.productlist;//Complete
      console.log(this.leftSideMyListColumns);
     if(this.productlist && this.productlists!= undefined && this.productlist.length > 0){
         this.rightSideMyListColumns =  this.productlist;
         console.log(this.rightSideMyListColumns);
        this.rightSideMyListColumns.forEach(item1 => {
          let index1 = -1
          this.leftSideMyListColumns.find((item,index) => {
            if(item.productName == item1.productName)
            {
              index1 = index;
              return true;
            }
          });
          this.leftSideMyListColumns.splice(index1, 1);
        });
      }

      
    }, err => {

    })
  }
  
  postreportData(post) {
    debugger;
let arr=[];

this.rightSideMyListColumns.forEach(element => {
  arr.push(element._id);
});
    let PrimaryUser = {

      GroupName: post.GroupName,
      productIds: arr,
    }
    console.log(PrimaryUser);
    this.service.addProductGroup(PrimaryUser).subscribe(res => {
    
      if (res['success']) {
        this.service.showToaster('Settings Saved Successfully');

      }

    });
  }
  cancel() {
    this.submitted = false;
    this.router.navigate(['./layout/pipeline-admin/commission-product-group'])
    }
}

