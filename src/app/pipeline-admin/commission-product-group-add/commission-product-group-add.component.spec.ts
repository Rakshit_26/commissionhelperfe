import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionProductGroupAddComponent } from './commission-product-group-add.component';

describe('CommissionProductGroupAddComponent', () => {
  let component: CommissionProductGroupAddComponent;
  let fixture: ComponentFixture<CommissionProductGroupAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionProductGroupAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionProductGroupAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
