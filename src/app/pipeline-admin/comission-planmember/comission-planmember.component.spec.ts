import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionPlanmemberComponent } from './comission-planmember.component';

describe('ComissionPlanmemberComponent', () => {
  let component: ComissionPlanmemberComponent;
  let fixture: ComponentFixture<ComissionPlanmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionPlanmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionPlanmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
