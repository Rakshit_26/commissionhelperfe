import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComissionHelperService} from'app/shared/services/comission-helper.service';

@Component({
  selector: 'app-comission-planmember',
  templateUrl: './comission-planmember.component.html',
  styleUrls: ['./comission-planmember.component.scss']
})
export class ComissionPlanmemberComponent implements OnInit {

  data:any;
  userid:any
  user: any;
  userPlan:any;
  selected: any = [];
  update: any;
  empty: any ;
   id:any;
  formGroup: FormGroup;
  
  newdata: any;
   
    username: any=[];
    
  addUser: any;
  username1: any;
    constructor(private router: Router,private service: ComissionHelperService,private fb: FormBuilder ,  private route: ActivatedRoute,) { }
  
    ngOnInit(): void {
     
      this.formGroup = this.fb.group({
        teamName: ['']
      });
    
        this.route.queryParams.subscribe(parsam => {
    
          this.data= JSON.parse(atob(parsam.data));
          console.log(this.data);
         this. obtainPlanById();
        
            this. getAllUser();
            
      
      })
     }
     obtainPlanById(){
      this.service.obtainPlanById(this.data).subscribe(res =>{
        this.userPlan= res['fetchPlan'];
        console.log('userPlan',res);
        this. getAllUser();
   
    });
     }
   
  
     loop(){
       console.log('we are in loop',this.userPlan.users);
      let data1=this.userPlan.users;
      let data2=this.addUser
      let count2=data2.length
      console.log(data1,data1.length);
      let count=data1.length
      console.log(data2,count2);
      for(let i=0; i<count; i++)
      {
        console.log('loop 1 is running');
        for(let j=0; j<count2; j++)
        {
        if (data1[i]== data2[j]._id)
        
        {
          console.log('loop 2 is running');
          this.username.push(this.addUser[j])
        }
        }
       
      }
      console.log('username',this.username);
      this.username1=this.username
     }
   
    getAllUser(){
      this.service.getAllUsers().subscribe(res=>{
        this.addUser=res['getall']
        console.log(this.addUser)
         console.log('addUser',res);
     this.loop();
   
     
            
          });
    }
    
    goto() {
      this.router.navigate(['/layout/pipeline-admin/comission-plan'], {
  
        queryParams: {
          data: btoa(JSON.stringify(this.data))
        }
      });
    }
   

    onSubmit2() {
      this.router.navigate(['/layout/pipeline-admin/comission-plan-assignuser'], {
  
        queryParams: {
          data: btoa(JSON.stringify(this.data))
        }
      });
    }
    

    open1() {
      document.getElementById("t1").style.display = "block";
      document.getElementById("t2").style.display = "none";
      document.getElementById("t3").style.display = "none";
      document.getElementById("t4").style.display = "none";
      document.getElementById("t5").style.display = "none";
      
      }
      
      open2() {
      document.getElementById("t2").style.display = "block";
      document.getElementById("t1").style.display = "none";
      document.getElementById("t3").style.display = "none";
      document.getElementById("t4").style.display = "none";
      document.getElementById("t5").style.display = "none";
      
      
      }
      open3() {
      document.getElementById("t3").style.display = "block";
      document.getElementById("t4").style.display = "none";
      document.getElementById("t5").style.display = "none";
      document.getElementById("t1").style.display = "none";
      document.getElementById("t2").style.display = "none";
      }
      delete(_id) {
        debugger;
        this.service.delete(_id).subscribe(res => {
        if (res['success']) {
        debugger;
        this.service.showToaster('User has been deleted');
        this.ngOnInit()
        
        }
        }, err => {
        console.log(err);
        this.service.ErrorSuccess('User Can not be deleted!');
        }
        )
        }
    
  }
  
