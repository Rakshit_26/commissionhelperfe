import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog,} from '@angular/material/dialog';
import{ComissionHelperService} from 'app/shared/services/comission-helper.service'
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { AddEvent, EditEvent, GridComponent } from '@progress/kendo-angular-grid';
@Component({
  selector: 'app-comission-quota-user',
  templateUrl: './comission-quota-user.component.html',
  styleUrls: ['./comission-quota-user.component.scss']
})
export class ComissionQuotaUserComponent implements OnInit {
  submitted = false;
  AddTeam:any=[];
  data: any;
  user: any;
  userQuota: any;
  userid: any=[];
  newdata: any=[];
 formGroup: FormGroup;
editedRowIndex: number;
isNew: boolean;
createFormGroup = dataItem => new FormGroup({
  'price': new FormControl(dataItem.price),
 
});
  constructor(private router: Router, private service: ComissionHelperService, private route: ActivatedRoute, public dialog: MatDialog, private formBuilder: FormBuilder) { }
 

  
  ngOnInit(): void {
    this.route.queryParams.subscribe(parsam => {
  
      this.data = JSON.parse(atob(parsam.data));
      console.log('data',this.data);
   this.getQuotaById();
  
    });
    
  }
  
  public cellClickHandler({ isEdited, dataItem, rowIndex }): void {
    if (isEdited || (this.formGroup && !this.formGroup.valid)) {
        return;
    }

    if (this.isNew) {
        rowIndex += 1;
    }

    this.saveCurrent();

    this.formGroup = this.createFormGroup(dataItem);
    this.editedRowIndex = rowIndex;

    //this.grid.editRow(rowIndex, this.formGroup);
}
  public cancelHandler(): void {
    this.closeEditor();
}
  
  private closeEditor(): void {
   // this.grid.closeRow(this.editedRowIndex);

    this.isNew = false;
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
}
private saveCurrent(): void {
  if (this.formGroup) {
     // this.service.save(this.formGroup.value, this.isNew);
      this.closeEditor();
  }
}
  getQuotaById(){
    this.service.getQuotaById(this.data.id).subscribe(res =>{
      this.userQuota= res['getQuota'];
      this.userid=this.userQuota.names;
      console.log(this.userid);
      console.log('userQuota',this.userQuota);
      this.getAllUser();
    });
  }
  getAllUser(){
    this.service.getAllUsers().subscribe(res=>{
      this.AddTeam=res['getall']
      console.log(this.userid.length);
      console.log(this.AddTeam);
      for(let i=0;i<this.userid.length;i++)
      { 
        
        for(let j=0;j<this.AddTeam.length;j++)
        {
          
          if((this.userid[i]==this.AddTeam[j]._id))
          {
            
            console.log(this.AddTeam[j]._id);
            this.AddTeam.splice(j, 1);
          }
          
        }
        this.newdata=this.AddTeam;
      }
     
      console.log('new',this.newdata)
       console.log(res);
    });
  }
  selectedKeysChange(row) {
    this.user = row;
console.log(this.user)

  }
 editClick(eve) {
    // console.logget(eve); 
  }
  
  AssignUser(){
    debugger;
    console.log(this.user,this.userid);
    for(let i=0;i<this.user.length;i++)
    {
      this.userid.push(this.user[i]);
    }
    console.log('userid',this.userid);
    let names={names:this.userid}
    this.service.modifyQuota(this.data.id,names).subscribe(res=>{
console.log(res);
this.router.navigate(['/layout/pipeline-admin/comission-quota-quotauser'], {

  queryParams: {
    data: btoa(JSON.stringify(this.data))
  }
});

    });
}

}


