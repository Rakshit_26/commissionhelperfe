import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionQuotaUserComponent } from './comission-quota-user.component';

describe('ComissionQuotaUserComponent', () => {
  let component: ComissionQuotaUserComponent;
  let fixture: ComponentFixture<ComissionQuotaUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionQuotaUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionQuotaUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
