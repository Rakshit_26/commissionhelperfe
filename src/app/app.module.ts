
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import {
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';

import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";

import { AuthService } from './shared/auth/auth.service';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

import { ToastrModule } from 'ngx-toastr';
import { TestComponent } from './test/test.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordRecoverMailComponent } from './password-recover-mail/password-recover-mail.component';
import { SignupComponent } from './signup/signup.component';
import { LoaderComponent } from './loader/loader.component';
import { DialogtoLoginComponent } from './dialogto-login/dialogto-login.component';
import { DialogOnChangePasswordComponent } from './dialog-on-change-password/dialog-on-change-password.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MaterialModule } from './material/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { LoaderInterceptor } from './shared/services/loader/loader.interceptor';
import { TokenInterceptorService } from './shared/services/token-interceptor.service';
import { LoaderService } from './shared/services/loader/loader.service';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: false
};

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}


@NgModule({
  declarations: [AppComponent, FullLayoutComponent, ContentLayoutComponent, TestComponent,
    SignupComponent,
    PasswordRecoverMailComponent,
    LoaderComponent,
    DialogtoLoginComponent,
    DialogOnChangePasswordComponent],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    NgbModule,
    NgxSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot({
      timeOut: 2500,
      positionClass: 'toast-top-right',
      preventDuplicates: false
    }),
    PerfectScrollbarModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    MatStepperModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    MatFormFieldModule,
    MatCardModule,
    ReactiveFormsModule,
    GridModule,
    ButtonsModule,
    

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  providers: [
    AuthService,
    AuthGuard,
    LoaderService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
   // {provide: LocationStrategy, useClass: HashLocationStrategy},
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogtoLoginComponent,
    DialogOnChangePasswordComponent
  ]
})
export class AppModule { }
