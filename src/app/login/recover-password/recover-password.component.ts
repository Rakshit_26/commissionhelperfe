import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators ,FormControl,NgForm } from '@angular/forms';

import { RoleService } from 'app/shared/services/role/role.service';

import { DialerService } from 'app/shared/services/dialer/dialer.service';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css'],
  providers: [DialerService]
})
export class RecoverPasswordComponent implements OnInit {
  PortalURL = environment.PortalUrl
  
  formGroup:FormGroup = new FormGroup({
    Email:new FormControl(''),
    
   
});
url: any = `${environment.PortalUrl}/login/newPassword`    

// --------------------- Variables ---------------------------------------------//
passwordRecoveryForm: FormGroup;
Email:string='';
Password:string='';
mailSentStatus = false;
message:string;
invalidPassword= false;
invalidEmail=false;
// -----------------------------------------------------------------------------//
// ------------------------------- Constructor ---------------------------------//
    constructor(
  
      public router: Router,
      private service: DialerService,
      private fb: FormBuilder,
      private RoleService: RoleService,
      public prismService: DialerService
            ) {
                  // To initialize FormGroup
    this.passwordRecoveryForm = fb.group({
      'Email':['', Validators.required]
      },{ validator: this.checkEmail });
    }

// -----------------------------------------------------------------------------//

ngOnInit() {
  debugger;
  this.formGroup= this.fb.group({
    Email:[''],
      
});
  const token = this.service.getToken();
  if(token) {
    this.backClicked();
  }
}

backClicked() {
  // 
  const route = this.RoleService.checkRoutes();
  this.router.navigate([route]);
}



    goTo1(post){

      let person ={
        Email:post.Email,
        url:this.url
      }
      
      this.service.recoverPassword(person).subscribe(data => {
        // console.log(data);
        this.mailSentStatus = true;
       
        // this.service.showToaster(this.message);
      },error=>{
        this.message = error['error']['errorMessage'];
        // this.service.ErrorSuccess(this.message);
        this.invalidEmail = true;
      })
    }

    // ------------------------------- Validator --------------------------------//

    checkEmail(form:FormGroup) {
      let enteredEmail = form.get('Email');
      let emailCheck = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (enteredEmail.errors == null || 'invalid' in enteredEmail.errors) {
      if (!enteredEmail.value.match(emailCheck)){
        enteredEmail.setErrors({ invalid: true });
      } else{
            enteredEmail.setErrors(null);
          } 
        }
    }

    goTo() {
      this.router.navigate(['./login']);
    }
// -----------------------------------------------------------------------------//

}
