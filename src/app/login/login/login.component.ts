import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm } from '@angular/forms';
// import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialerService } from 'app/shared/services/dialer/dialer.service';
import { RoleService } from 'app/shared/services/role/role.service';
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [DialerService, RoleService]
})
export class LoginComponent implements OnInit {

  // --------------------- Variables ---------------------------------------------//
  loginForm: FormGroup;
  payload = {};
  Email: string = '';
  Password: string = '';
  Role: string;
  message: string;
  invalidPassword= false;
  // -----------------------------------------------------------------------------//

  // ------------------------------- Constructor ---------------------------------//
  constructor(
    // private toastrService: ToastrService,
    private snackBar: MatSnackBar,
    public router: Router,
    private service: DialerService,
    private fb: FormBuilder,
    private _location: Location,
    private RoleService: RoleService,
  ) {

    // To initialize FormGroup
    this.loginForm = fb.group({
      'Email': [null, Validators.required],
      'Password': [null, Validators.required]
    });
  }

  // -----------------------------------------------------------------------------//

  ngOnInit() {
    debugger;
    const token = this.service.getToken();
    if (token) {
      this.backClicked();
    }
  }

  backClicked() {
    debugger
    const route = this.RoleService.checkRoutes();
    this.router.navigate([route]);
  }

  login(form: NgForm) {
    debugger;

    // console.log(form);
    //
    const UserDetails = {
      username: form['Email'],
      password: form['Password']
    };
    this.service.loginUser(UserDetails).subscribe((data) => {
      if (data['success']) {
        debugger;
        localStorage.setItem('token', data['token']);
        localStorage.setItem('role', data['user']['Role']);
        localStorage.setItem('userId', data['user']['_id']);

        localStorage.setItem('userName', data['user']['username']);
        this.message = 'Login Successful';
        // this.service.showToaster(this.message);

        const route = this.RoleService.checkRoutes();
       // this.router.navigateByUrl("layout/test")
        this.router.navigate([route]);
      }
    }, (err) => {
      debugger;
      this.invalidPassword = true;
      const Email = document.getElementById('email');
      Email.className = 'errorTextBox';
      const Password = document.getElementById('password');
      Password.className = 'errorTextBox';
    });
  }

  recoverPassword() {
    this.router.navigate(['/login/recoverPassword']);

  }
  signup() {
    this.router.navigate(['/signup'])
  }
}
