import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [


    {path: '/layout/pipeline-admin/home', title: 'Dashboard', icon: 'fa fa-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/comission-team', title: 'Team', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/productdetails-list', title: 'Product', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/commission-product-group', title: 'Product Group', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/commission-sales', title: "Sales", icon: 'fa fa-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/comission-plan', title: 'Plan', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
   
    { path: '/layout/pipeline-admin/commission', title: 'Commission', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/commission-direct-calculator', title: 'Direct Calculator Commission', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/commission-indirect-calculator', title: 'Indirect Calculator Commission', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
 
    { path: '/layout/pipeline-admin/comission-quota', title: 'Commission History', icon: 'fa fa-tasks', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/layout/pipeline-admin/user', title: "User", icon: 'fa fa-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
   

];
