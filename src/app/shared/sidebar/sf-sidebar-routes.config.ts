import { RouteInfo } from './sidebar.metadata';



    PortalUrl:"https://frontendcodedailer.herokuapp.com"

export const SFROUTES: RouteInfo[] = [



    {
        path: `/layout/salesforce/${localStorage.getItem('token')}/home`, title: 'Home', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: `/layout/salesforce/${localStorage.getItem('token')}/dialerSession`, title: 'dialer Session', icon: 'fa fa-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: `/layout/salesforce/${localStorage.getItem('token')}/mylist`, title: 'My list', icon: 'fa fa-id-badge', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: `/layout/salesforce/${localStorage.getItem('token')}/scheduler`, title: 'scheduler', icon: 'fa fa-list-ul', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: `/layout/salesforce/${localStorage.getItem('token')}/report`, title: 'report', icon: 'fa fa-globe', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: `/layout/salesforce/${localStorage.getItem('token')}/setting`, title: 'setting', icon: 'fa fa-globe', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
   

    // {
    //     path: '/full-layout', title: 'Full Layout', icon: 'ft-layout', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    // },
    // {
    //     path: '/content-layout', title: 'Content Layout', icon: 'ft-square', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    // },
    // {
    //     path: '', title: 'Menu Levels', icon: 'ft-align-left', class: 'has-sub', badge: '1', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
    //     submenu: [
    //         { path: 'javascript:;', title: 'Second Level', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
    //         {
    //             path: '', title: 'Second Level Child', icon: '', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
    //             submenu: [
    //                 { path: 'javascript:;', title: 'Third Level 1.1', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
    //                 { path: 'javascript:;', title: 'Third Level 1.2', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
    //             ]
    //         },
    //     ]
    // },
    // {
    //     path: '/changelog', title: 'ChangeLog', icon: 'ft-file', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    // },
    // { path: 'https://pixinvent.com/apex-angular-4-bootstrap-admin-template/documentation', title: 'Documentation', icon: 'ft-folder', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
    // { path: 'https://pixinvent.ticksy.com/', title: 'Support', icon: 'ft-life-buoy', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },

];
