import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AgentService {
  baseUrl = environment.URL;
  PortalURL = environment.PortalUrl;
  constructor( private http: HttpClient,private toastrService: ToastrService, private router: Router ) { }

  getAllAgent(pagenumber,filter, pageSize, property, order,timeDifference) {
    debugger;
	var link =`${this.baseUrl}/v0.1/auth/getAllAgent?pagesize=${pageSize}${filter}&pageIndex=${pagenumber + 1}&property=${property}&order=${order}&timeDifference=${timeDifference}`;
console.log(link);
    return this.http.get(link);
   }

   addAgent(agentDetails){
    return this.http.post(`${this.baseUrl}/v0.1/auth/addAgent`, agentDetails);
   }

editAgent (post){
  debugger;
  return this.http.post(`${this.baseUrl}/v0.1/auth/editAgent`, post);
}
deleteAgent(userId){
  return this.http.delete(`${this.baseUrl}/v0.1/auth/deleteAgent?userId=${userId}`)
}


getPasswordResetUrl(userId,PortalURL){
  debugger;
  let post={
    userId,
    url:`${this.PortalURL}/${PortalURL}`
  }

  return this.http.post(`${this.baseUrl}/v0.1/auth/TokenAgent`, post);
}

   showToaster(message) {
    this.toastrService.success(message);
  }
  ErrorSuccess(message) {
    this.toastrService.error(message);
  }
  infoSuccess(message) {
    this.toastrService.info(message);
  }
  warningSuccess(message) {
    this.toastrService.warning(message);
  }

  activateAgent(agentDetails){
    debugger;
  
    return this.http.post(`${this.baseUrl}/v0.1/auth/AgentStatus`, agentDetails);
  }

  // getAllDialerList (){
  //   debugger;
  //   return this.http.get(`${this.baseUrl}/v0.1/list/getAllListOfAllCompanies`);
  //   }
 

//------------------------------------Admin Dialer List ---------------------------------------------------//

getAllDialerList (){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/list/getAllListOfAllCompanies`);
}
getDialerListDetails (_id){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/list/getListDetails?_id=${_id}`);
}
getAllTrackAgent(){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/auth/getAllTrackAgent`);
}

getAgentList (){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/auth/getAgentForList`);
}
agentListInfo (agentDetails){
  debugger;
  return this.http.post(`${this.baseUrl}/v0.1/agentListInfo/addListInfo`,agentDetails);
}

getprofile(){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/getOneById`);

}
getDilalerlist(){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/agentListInfo/getContactList`);

}


getdialelistWithContactID(_id){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/list/getListDetails?_id=${_id}`);

}
allAgentStatus(){
  debugger;
var link1 = `${this.baseUrl}/v0.1/auth/AllAgentStatus`;
  return this.http.get(link1);
}
}
