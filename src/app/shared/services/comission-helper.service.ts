import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
@Injectable({
  providedIn: 'root'
})
export class ComissionHelperService {
 
  baseUrl = environment.URL;
  constructor(private http: HttpClient, private toastrService: ToastrService, private router: Router) { }
  deleteQuota(_id) {
    return this.http.delete(`${this.baseUrl}/v0.1/deleteQuota?_id=${_id}`);
  }
  getAllQuota(){
    return this.http.get(`${this.baseUrl}/v0.1/getAllQuota`)
}
modifyQuota(_id,post){
  debugger;
  return this.http.put(`${this.baseUrl}/v0.1/editQuota?_id=${_id}`,post);
  }
  getQuotaById(_id){
    debugger;
    return this.http.get(`${this.baseUrl}/v0.1/getQuotaById?_id=${_id}`);
    }
addQuota(post){
  debugger;
  return this.http.post(`${this.baseUrl}/v0.1/addQuota`, post);
  }
 
////////////////////////////////////products//////////////////////////
  getAllProductGroup(){
    return this.http.get(`${this.baseUrl}/v0.1/getAllProductGroup`)
}

  getAllProduct() {
    debugger;
    return this.http.get(`${this.baseUrl}/v0.1/getAllProduct`);
  }
  addProductGroup(post) {
    debugger;
    return this.http.post( `${this.baseUrl}/v0.1/addProductGroup`, post );
  }
  editProductGroup(post,_id ) {
    debugger;
    return this.http.post( `${this.baseUrl}/v0.1/editProductGroup?_id=${_id}`, post );
  }
  deleteProduct(_id){
    debugger;
    return this.http.delete(`${this.baseUrl}/v0.1/deleteProduct`);
    }
  
    editProduct(_id,data) {
    debugger;
    return this.http.post(`${this.baseUrl}/v0.1/editProduct?_id=${_id}`, data)
  }
  obtainProductById(id: any) {
    return this.http.get(`${this.baseUrl}/v0.1/getProductById?ProductId=${id}`); 
  }
  getProductGroupInfo(_id) {
    return this.http.get(`${this.baseUrl}/v0.1/getProductGroupInfo?_id=${_id}`); 
  }

  addProduct(addproduct: any) {
    debugger;
    return this.http.post(`${this.baseUrl}/v0.1//addProduct`, addproduct);
  }  
  ///////////////// ///////////////////Quota///////////////////////////////////////////////
  getAllPlans(){
    return this.http.get(`${this.baseUrl}/v0.1/getAllPlans`)
}
deletePlan(_id) {
  return this.http.delete(`${this.baseUrl}/v0.1/removePlan?_id=${_id}`);
}
modifyPlan(_id,post){
  debugger;
  return this.http.put(`${this.baseUrl}/v0.1/ModifyPlan?_id=${_id}`,post);
  }
  obtainPlanById(_id){
    debugger;
    return this.http.get(`${this.baseUrl}/v0.1/obtainPlanById?_id=${_id}`);
    }
    addplan(post){
      debugger;
      return this.http.post(`${this.baseUrl}/v0.1/addPlan`, post);
      }
      searchPlan(_id) {
        return this.http.get(`${this.baseUrl}/v0.1/searchPlan`, _id);
    }
  /////////////////////////////Plan////////////////////////////

 getAllUsers(){
 debugger;
 return this.http.get(`${this.baseUrl}/v0.1/getAllUser`);
 
 }
 
 delete(_id){
   debugger;
   return this.http.delete(`${this.baseUrl}/v0.1/deleteUser?_id=${_id}`);
   }

   addUser(post){
     debugger;
     return this.http.post(`${this.baseUrl}/v0.1/addUser`, post);
     }
      
     
      
      obtainUserById(_id){
        debugger;
        return this.http.get(`${this.baseUrl}/v0.1/obtainUserById?_id=${_id}`);
        }
      
        edit (post){
          debugger;
          return this.http.post(`${this.baseUrl}/v0.1/editUser`,post);
        }
      
        saveDetail(userDetails){
          return this.http.post(`${this.baseUrl}/v0.1/auth/signup`, userDetails);
          }

     /////////////////////////team/////////////////////////////
     getAllTeam(){
      debugger;
      return this.http.get(`${this.baseUrl}/v0.1/getAllTeam`);
    } 
     updateTeam(_id,post){
      debugger;
      return this.http.put(`${this.baseUrl}/v0.1/updateTeam?_id=${_id}`,post);
      }
      subTeamList(_id){
        debugger;
        return this.http.get(`${this.baseUrl}/v0.1/getSubTeamById?_id=${_id}`);
        }
        getTeamById(_id){
          debugger;
          return this.http.get(`${this.baseUrl}/v0.1/getTeamById?_id=${_id}`);
          }
          
          deleteTeam(userId){
            debugger
            return this.http.delete(`${this.baseUrl}/v0.1/deleteTeam?_id=${userId}`);
          }
          ModifyTeam(teamid , data){
            debugger;
            return this.http.put(`${this.baseUrl}/v0.1/ModifyTeam?TeamId=${teamid}`,data)
          }
          obtainTeamById(id){
          return this.http.get(`${this.baseUrl}/v0.1/obtainTeamByid?TeamId=${id}`);
          } 
          addTeam(post){
            debugger;
            return this.http.post(`${this.baseUrl}/v0.1/addTeam`, post);
          }
          

          showToaster(message) {
            this.toastrService.success(message);
          }
          ErrorSuccess(message) {
            this.toastrService.error(message);
          }
          infoSuccess(message) {
            this.toastrService.info(message);
          }
          warningSuccess(message) {
            this.toastrService.warning(message);
          }
    }