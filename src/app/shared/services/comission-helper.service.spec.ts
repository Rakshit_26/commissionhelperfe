import { TestBed } from '@angular/core/testing';

import { ComissionHelperService } from './comission-helper.service';

describe('ComissionHelperService', () => {
  let service: ComissionHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComissionHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
