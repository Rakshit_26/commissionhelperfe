import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from 'environments/environment'



@Injectable({
  providedIn: 'root'
})
@Injectable({
  providedIn: 'root'
})
export class CompanydetailsService {

  invitationURl: any;
  baseurl = environment.URL;
  companyList: any;

  constructor(
    private http: HttpClient,
    private toastrService: ToastrService,
    private router: Router
  ) { }
  //tostr notifications

  showToaster(message) {
    this.toastrService.success(message);
  }
  ErrorSuccess(message) {
    this.toastrService.error(message);
  }
  infoSuccess(message) {
    this.toastrService.info(message);
  }
  warningSuccess(message) {
    this.toastrService.warning(message);
  }


  //-------------------------------------------User List-----------------------------------------------//

  getCompanyById(CompanyId) {
      
    return this.http.get(`${this.baseurl}/v0.1/Company/getCompanyById?portalId=${CompanyId}`);
  }
  getCompanyWithUser(CompanyId,pagenumber, pageSize, property, order ) {
      
    return this.http.get(`${this.baseurl}/v0.1/Company/getCompanyWithUser?CompanyId=${CompanyId}&pageSize=${pageSize}&pageIndex=${pagenumber}&property=${property}&order=${order}`);
  }



  
}
