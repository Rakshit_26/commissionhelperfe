import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class RoleService {
Role: string;
  constructor() { }

getRole() {
  debugger;
this.Role = localStorage.role;
return this.Role;
}

checkRoutes() {
  const userRole = this.getRole();
  debugger;
  let route = '';
  switch (userRole) {
                case 'admin': route = `/salesforce/${localStorage.token}/home`; break;
                case 'manager': route = `/salesforce/${localStorage.token}/home`; break;
                case 'salesrep': route = `/salesforce/${localStorage.token}/home`; break;
                case 'agent': route = '/dashboard/agent'; break;
                case 'pipelineAdmin': route = '/dashboard/pipelineadmin'; break;
            }
  return route;
}
}

// export enum Role {
//   Manager = 'manager',
//   Admin = 'admin'
// }
