
 
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "environments/environment";
import { ToastrService } from "ngx-toastr";
@Injectable({
  providedIn: 'root'
})
export class ProductDeatailsService {
  
  
  baseUrl = environment.URL;

  constructor(private http: HttpClient, private toastrService: ToastrService, private router: Router) { }
  getAllProduct() {
    debugger;
    return this.http.get(`${this.baseUrl}/v0.1/getAllProduct`);
  }
  delete(_id){
    debugger;
    return this.http.delete(`${this.baseUrl}/v0.1/deleteProduct`);
    }
  
    edit(_id,data) {
    debugger;
    return this.http.put(`${this.baseUrl}/v0.1/editProduct??_id=${_id}`, data)
  }
  obtainProductById(id: any) {
    return this.http.get(`${this.baseUrl}/v0.1/getProductById?ProductId=${id}`); 
  }

  addProduct(addproduct: any) {
    debugger;
    return this.http.post(`${this.baseUrl}/v0.1//addProduct`, addproduct);
  }
  showToaster(message: string) {
    this.toastrService.success(message);
  }
  ErrorSuccess(message: string) {
    this.toastrService.error(message);
  }
  

  infoSuccess(message: string) {
    this.toastrService.info(message);
  }
  warningSuccess(message: string) {
    this.toastrService.warning(message);
  }
  deleteProduct(userId: any) {
    return this.http.delete(`${this.baseUrl}/v0.1/deleteProduct?_id=${userId}`);
  }
}
