import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class SocketService {
  agentList = this.socket.fromEvent<any>('agentList');
  connect = this.socket.fromEvent<any>('connected') ;
  getStatus = this.socket.fromEvent<any>('getStatus');

  constructor(private socket: Socket) { }
 

trackAgent(){
debugger;
  this.socket.emit( 'agentTracking')
}

disconnect(){
  debugger;
this.socket.emit('disconnect')

}

agentStatusChange(Status){
  this.socket.emit('statusChange' , Status)
}

createConnection(){
  debugger;
  this.socket.emit('createConnection')
}

}

