import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "environments/environment";
import { ToastrService } from "ngx-toastr";
@Injectable({
  providedIn: 'root'
})
export class CommissionService {

  baseUrl = environment.URL;
  constructor(private http: HttpClient, private toastrService: ToastrService, private router: Router) { }

getAllCommission() {
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/getAllCommission`);
}
delete(_id){
  debugger;
  return this.http.delete(`${this.baseUrl}/v0.1/deleteCommission?_id=${_id}`);
  }

editcomission(commissionid,data) {
  debugger;
  return this.http.put(`${this.baseUrl}/v0.1/editCommission?CommissionId=${commissionid}`, data)
}

addcommission(addcommission: any) {
  debugger;
  return this.http.post(`${this.baseUrl}/v0.1//addCommission`, addcommission);
}
showToaster(message: string) {
  this.toastrService.success(message);
}
ErrorSuccess(message: string) {
  this.toastrService.error(message);
}


infoSuccess(message: string) {
  this.toastrService.info(message);
}
warningSuccess(message: string) {
  this.toastrService.warning(message);
}
}