import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SalesforceService {
  
sessionActive:boolean=false;

  baseUrl = environment.URL;
  constructor(private http: HttpClient) { }


  getAllContactList() {
   //return this.http.get(`${this.baseUrl}/v0.1/salesforce/getAllContactsFromSalesforce`);
    return this.http.get(`${this.baseUrl}/v0.1/list/getAllListContacts`);
  }
  getContactStatus(salesRepId){
    debugger
    return this.http.get( `https://ee4aba28f6fd.ngrok.io/v0.1/list/getContactStatus?salesRepId=${salesRepId}`);
    }

  
  getAllLead() {
    return this.http.get(`${this.baseUrl}/v0.1/salesforce/getAllLeadsFromSalesforce`);
  }

  getAllReports() {
    return this.http.get(`${this.baseUrl}/v0.1/salesforce/getAllReportsFromSalesforce`);
  }

  uploadFile(formData) {
    debugger;
    return this.http.post(`${this.baseUrl}/v0.1/voiceMsg/SaveVoiceMsg`, formData);
  }
  
  editVoiceMessage(post){
    debugger;
    return this.http.put(`${this.baseUrl}/v0.1/voiceMsg/editVoiceMsg`, post);
  }

  deleteVoiceMessage(_id){
    return this.http.delete(`${this.baseUrl}/v0.1/voiceMsg/deleteVoiceMsg?_id=${_id}`)
  }
  

  getallIsdefaulttype1() {
    return this.http.get(`${this.baseUrl}/v0.1/voiceMsg/IsDefaultget?types=1`);
  }


  getallIsdefaulttype2() {
    return this.http.get(`${this.baseUrl}/v0.1/voiceMsg/IsDefaultget?types=2`);
  }

 
  getAllManagers(){
    return this.http.get(`${this.baseUrl}/v0.1/getCompanyManager`)
  }

  getAllSalesRep(){
    return this.http.get(`${this.baseUrl}/v0.1/getCompanySalesRep`)
  }

  getallVoiceMessage() {
    return this.http.get(`${this.baseUrl}/v0.1/voiceMsg/getVoiceMsg?types=1`);
  }
  getallGreetings() {
    debugger;
    return this.http.get(`${this.baseUrl}/v0.1/voiceMsg/getVoiceMsg?types=2`);
  }

  importExcel(post) {
    return this.http.post(`${this.baseUrl}/v0.1/salesforce/uploadExcel`, post);
  }

  getAllList(){
    return this.http.get(`${this.baseUrl}/v0.1/salesforce/getAllList`);
  }

  updateList(post){
    return this.http.put(`${this.baseUrl}/v0.1/list/editDialerList`, post);
  }

TableoneReportSetting(){
  
  return this.http.get(`${this.baseUrl}/v0.1/UserReport/getReport`);
}

TabletwoReportSetting(){
  
  return this.http.get(`${this.baseUrl}/v0.1/UserReport/getColumnReport`);
}

postReportSetting(post){
  
  return this.http.post(`${this.baseUrl}/v0.1/UserReport/saveColumnReport`, post);
}

getSetting(){
  debugger
  return this.http.get(`${this.baseUrl}/v0.1/setting/getSettings`);
}

postSetting(post){
  debugger;
  return this.http.post(`${this.baseUrl}/v0.1/setting/saveSettings`, post);
}

PostExportasExcelCSV(post){

debugger;
return this.http.post(`${this.baseUrl}/v0.1/list/excelList`, post);

}
transferOwnership(resData){

  debugger;
  return this.http.post(`${this.baseUrl}/v0.1/list/transferOwnership`, resData);
  
  }
  setDefaultVoiceGreetMsg(_id){
    debugger;
    return this.http.post(`${this.baseUrl}/v0.1/voiceMsg/IsDefaultChange`, _id);

  }

  importFromCRM(post){
    debugger;
    return this.http.post(`${this.baseUrl}/v0.1/salesforce/importExcelFromCRM`,post);

  }

  editList(post){
    debugger;
    return this.http.put(`${this.baseUrl}/v0.1/salesforce/editList`,post);
  }

  getSession(userId){
    debugger;
    return this.http.get(`http://6574709f.ngrok.io/v0.1/Session/getSessionByUser?Id=${userId}`);
  }

  delete(ListId){
    debugger;
    return this.http.delete(`${this.baseUrl}/v0.1/list/deleteDialerList`,ListId);
  }
  salesrepStatus(data){
    debugger
    return this.http.put(`${this.baseUrl}/v0.1/salesforce/updateSalesRepStatus`,data);
    
  }

  schedulerPost(data){
    debugger
    return this.http.post(`${this.baseUrl}/v0.1/scheduler/ScheduleTime`,data);
   
  }
  
  schedulerGet(){
    debugger
    return this.http.get( `${this.baseUrl}/v0.1/scheduler/getsalesRepTime`);

  
  }
}
