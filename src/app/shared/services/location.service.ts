import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LocationService {
  baseUrl = environment.URL;
  constructor(private http: HttpClient,private toastrService: ToastrService, private router: Router ) { }
  
  getCenterDetail (){
  debugger;
  return this.http.get(`${this.baseUrl}/v0.1/callCenter/getCenterDetail`);
  }
  
  saveCenterDetail(locationDetails){
  return this.http.post(`${this.baseUrl}/v0.1/callCenter/saveCenterDetail`, locationDetails);
  }
  
  editCenterDetail (post){
  debugger;
  return this.http.put(`${this.baseUrl}/v0.1/callCenter/editCenterDetail`, post);
  }
  
  deleteCenterDetail(_id){
  debugger;
  console.log(_id);
  return this.http.delete(`${this.baseUrl}/v0.1/callCenter/deleteCenterDetail?_id=${_id}`);
  }
  
  showToaster(message) {
  this.toastrService.success(message);
  }
  ErrorSuccess(message) {
  this.toastrService.error(message);
  }
  infoSuccess(message) {
  this.toastrService.info(message);
  }
  warningSuccess(message) {
  this.toastrService.warning(message);
  }
  }
