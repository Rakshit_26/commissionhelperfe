import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';


@Injectable({
  providedIn: 'root'
})
@Injectable({
  providedIn: 'root'
})
export class CompanycomponentService {

  invitationURl: any;
  baseurl = environment.URL;
  companyList: any;

  constructor(
    private http: HttpClient,
    private toastrService: ToastrService,
    private router: Router
  ) { }

  //tostr notifications

  showToaster(message) {
    this.toastrService.success(message);
  }
  ErrorSuccess(message) {
    this.toastrService.error(message);
  }
  infoSuccess(message) {
    this.toastrService.info(message);
  }
  warningSuccess(message) {
    this.toastrService.warning(message);
  }

  getAllCompany(pagenumber, filter, pageSize, property, order) {
      
     return this.http.get(`${this.baseurl}/v0.1/Company/getAllCompany?pageSize=${pageSize}&pageIndex=${pagenumber}&property=${property}${filter}&order=${order}`);
   }

   deleteCompany(CompanyId) {
     
     return this.http.delete(`${this.baseurl}/V0.1/Company/deleteCompany?CompanyId=${CompanyId}`);

   }




}
