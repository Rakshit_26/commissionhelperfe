import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class RoleService {
Role: string;
  constructor() { }

getRole() {
  debugger;
this.Role = localStorage.role;
return this.Role;
}

checkRoutes() {
  const userRole = this.getRole();
  debugger;
  let route = '';
  switch (userRole) {
                case 'Admin': route = `/layout/salesforce/${localStorage.token}/home`; break;
                case 'manager': route = `/layout/salesforce/${localStorage.token}/home`; break;
                case 'salesrep': route = `/layout/salesforce/${localStorage.token}/home`; break;
                case 'agent': route = '/layout/agent/home'; break;
                case 'null': route = '/layout/pipeline-admin/home'; break; //new path 
            }
  return route;
}
}

// export enum Role {
//   Manager = 'manager',
//   Admin = 'admin'
// }
