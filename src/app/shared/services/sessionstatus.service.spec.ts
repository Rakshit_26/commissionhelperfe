import { TestBed } from '@angular/core/testing';

import { SessionstatusService } from './sessionstatus.service';

describe('SessionstatusService', () => {
  let service: SessionstatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionstatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
