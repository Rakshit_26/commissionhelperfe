import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class SocketService {
  userid = localStorage.userId;
  agentList = this.socket.fromEvent<any>('agentList');
  connect = this.socket.fromEvent<any>('connected') ;
  getStatus = this.socket.fromEvent<any>('getStatus');
  getForwardedCall = this.socket.fromEvent<any>(`callForwarded`);
  connectAgain = this.socket.fromEvent<any>('connectAgain');
  getListOfContacts = this.socket.fromEvent<any>('getListOfContacts');
  constructor(private socket: Socket) { }

trackAgent(){
debugger;
  this.socket.emit('agentTracking')
}

getContactList(){
  debugger;
  this.socket.emit('getContactList')
}

disconnect(){
  debugger;
this.socket.emit('disconnect')

}

agentStatusChange(Status){
  this.socket.emit('statusChange' , Status);
}

createConnection(){
  debugger;
  this.socket.emit('createConnection')
}

callForward(Contact){
debugger;
this.socket.emit('callForward', Contact);
}

callDialing(Contact){
  this.socket.emit('dialing', Contact);
}
callPickedUp(Contact){
  this.socket.emit('callPickedUp', Contact);
}
callComplete(Contact){
  this.socket.emit('callComplete', Contact);
}
}

