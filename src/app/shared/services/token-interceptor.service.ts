import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { DialerService } from './dialer.service';

@Injectable({
   providedIn: 'root'
   })

export class TokenInterceptorService implements HttpInterceptor {

  debugger
  constructor(private injector: Injector){ }
  intercept(req, next) {

    const serviceFarm = this.injector.get(DialerService);
    debugger
    const tokenizedReq = req.clone({
      setHeaders: {
        Authorization : `${serviceFarm.getToken()}` // Read token
      }
    });
    return next.handle(tokenizedReq);
  }
}

// complete
